<br />
<div align="left">
 
  <h2 align="left">libaq</h2>
  <p align="left">
    Aquifer systems (saturated) with finite volume using a semi-implicit temporal scheme : 
    <br />
    - confined-unconfined aquifer units, 
    <br />
    - 2D diffusivity equation in each layer, 
    <br />
    - 1D vertical exchanges between layers with a linear drainance model, 
    <br />
    - 1D vertical exchanges at soil surface or for river with a conductance model, 
    <br />
    - subcell lithology.
    <br />
    <br />
    Library developed at the Centre for geosciences and geoengineering, Mines Paris/ARMINES, PSL University, Fontainebleau, France.
    <br />
    <br />
    <strong>Contributors</strong>
    <br />
    Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS, Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT, Shuaitao WANG
    <br />
    <br />
    <strong>Contact</strong>
    <br />
    Nicolas FLIPO <a href="mailto:nicolas.flipo@minesparis.psl.eu">nicolas.flipo@minesparis.psl.eu</a>
    <br />
    Nicolas GALLOIS <a href="mailto:nicolas.gallois@minesparis.psl.eu">nicolas.gallois@minesparis.psl.eu</a>
  </p>
</div>

## Copyright

[![License](https://img.shields.io/badge/License-EPL_2.0-blue.svg)](https://opensource.org/licenses/EPL-2.0)

&copy; 2022 Contributors to the libaq library.

*All rights reserved*. This software and the accompanying materials are made available under the terms of the Eclipse Public License (EPL) v2.0 
which accompanies this distribution, and is available at http://www.eclipse.org/legal/epl-v20.html.
