/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_eletype.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
#include "MSH.h"
#include "AQ.h"

void AQ_count_eletype(s_cmsh *pcmsh, FILE *fp) {

  s_id_io *eleactive;
  s_id_io **elebound;
  s_id_io **elesource;
  int *nele_type;
  int i;
  nele_type = pcmsh->pcount->pcount_hydro->nele_type;
  eleactive = pcmsh->pcount->pcount_hydro->eleactive;
  elebound = pcmsh->pcount->pcount_hydro->elebound;
  elesource = pcmsh->pcount->pcount_hydro->elesource;

  nele_type[ELE_ACTIVE_AQ] = IO_length_id(eleactive);
  for (i = 0; i < NBOUND; i++) {
    nele_type[i] = IO_length_id(elebound[i]);
  }
  for (i = 0; i < NSOURCE; i++) {
    nele_type[i + ELE_RECHARGE_AQ] = IO_length_id(elesource[i]);
  }
}

s_id_io ***AQ_create_eletype(int dim) {

  s_id_io ***eletype;
  int i;
  eletype = (s_id_io ***)malloc(dim * sizeof(s_id_io **));

  for (i = 0; i < dim; i++) {
    eletype[i] = NULL;
  }
  return eletype;
}

void AQ_tab_eletype_all(s_cmsh *pcmsh, FILE *fp) {
  s_id_io *eleactive;
  s_id_io **elebound;
  s_id_io **elesource;
  s_id_io ***p_eletype;
  int *nele_type;
  int i, j;
  pcmsh->pcount->pcount_hydro->nele_type = (int *)malloc(NELE_TYPE * sizeof(int));
  pcmsh->pcount->pcount_hydro->p_eletype = AQ_create_eletype(NELE_TYPE);
  AQ_count_eletype(pcmsh, fp);
  nele_type = pcmsh->pcount->pcount_hydro->nele_type;
  eleactive = pcmsh->pcount->pcount_hydro->eleactive;
  elebound = pcmsh->pcount->pcount_hydro->elebound;
  elesource = pcmsh->pcount->pcount_hydro->elesource;
  p_eletype = pcmsh->pcount->pcount_hydro->p_eletype;
  p_eletype[ELE_ACTIVE_AQ] = AQ_tab_eletype(eleactive, nele_type[ELE_ACTIVE_AQ], ELE_ACTIVE_AQ, fp);
  for (i = 0; i < NBOUND; i++) {
    p_eletype[i] = AQ_tab_eletype(elebound[i], nele_type[i], i, fp);
  }
  for (i = 0; i < NSOURCE; i++) {
    p_eletype[i + ELE_RECHARGE_AQ] = AQ_tab_eletype(elesource[i], nele_type[i + ELE_RECHARGE_AQ], i + ELE_RECHARGE_AQ, fp);
  }
}

s_id_io **AQ_tab_eletype(s_id_io *peletype, int neletype, int eletype, FILE *fp) {

  s_id_io *pid = NULL;
  s_id_io **p_eletype;
  int i;
  if (neletype > 0)
    p_eletype = (s_id_io **)malloc(neletype * sizeof(s_id_io *));
  else
    p_eletype = NULL;
  if (peletype != NULL)
    pid = IO_browse_id(peletype, BEGINNING_TS);

  for (i = 0; i < neletype; i++) {

    p_eletype[i] = pid;
    pid = pid->next;
  }

  if (pid != NULL)
    LP_error(fp, "Error inconsistent number of ele_type %s", AQ_name_eletype(eletype));
  return p_eletype;
}
