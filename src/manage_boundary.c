/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_boundary.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

/**\fn s_bound_aq *AQ_create_boundary(int kindof)
 *\brief create BC structur
 *\return pbound
 */
s_bound_aq *AQ_create_boundary(int kindof) {
  s_bound_aq *pbound;
  pbound = new_bound();
  bzero((char *)pbound, sizeof(s_bound_aq)); // qlim est bien mis a 0 ici
  pbound->type = kindof;
  // pbound->pft = (s_ft *) malloc(sizeof(s_ft));

  return pbound;
}

/**\fn s_ele_msh *AQ_tab_boundaries(s_ele_msh *pele,int icardinal,s_bound_aq *pbound,FILE *fpout)
 *\brief define the boundary condition of one element
 *\return pele
 */
// NF 30/9/2017 This function was not coded properly by BL. At ths stage of the CODE, note that the faces at the edge of the model are already flagged as pface->type==BOUND. But it seems that pface->phydro->pbound==NULL
s_ele_msh *AQ_tab_boundaries(s_ele_msh *pele, int icardinal, s_bound_aq *pbound, s_id_io **elebound, FILE *fpout) {

  s_face_msh *pface, *psubf;
  s_ele_msh *pneigh;
  s_id_io *id_list;
  int idir, itype, i, kindof, id;
  kindof = pbound->type;
  if (icardinal != CODE_AQ) // NF 6/12/2017 Only NEUMANN BC have a icardinal!
  {
    // NF6/12/2017 First we verify that the face is a border
    idir = MSH_dir_by_card(icardinal);
    itype = MSH_type_by_card(icardinal);
    pface = pele->face[idir][itype];
    pneigh = pface->p_ele[itype];
    if (pneigh != NULL)
      LP_error(fpout, "libaq%4.2f %s in %s l%d: the face %s of ele %i in layer %s is inside the MESH. Setting a NEUMANN condition on it is impossible but a hydraulic barrier, which has to be implemented in the code (In that case watch the subfaces). Please correct your BC file", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_name_neigh(idir, itype), MSH_get_ele_id(pele, GIS_MSH),
               pele->player->name);
    else {
      // NF 6/12/2017 If it is a border, we create a boundary at the center of the element which is needed to fill the matrix if this is the only BC on the element. Actually I am not 100% sure of that. It may be sufficient to create the boundary at the face only
      pele->phydro->pbound = AQ_create_boundary(kindof); // NF 26/9/2017 Je ne pense pas que ce soit bien de faire une copie. Je pense qu'il suffirait de gerer seulement la BC et de faire pointer pele->phydro->pbound directement vers pbound
      pele->phydro->pbound->freq = pbound->freq;
      pele->phydro->pbound->pft = pbound->pft;
      if (kindof == DIRICHLET || kindof == CAUCHY)
        LP_error(fpout, "libaq%4.2f %s in %s l%d: a %s boundary can't be applied on faces. Check in your input files that you did not introduce { W,E,N,S } in front of your cell numbers concerned by the BC.\n", VERSION_AQ, __func__, __FILE__, __LINE__, AQ_name_BC(kindof));

      if (pface->phydro->pbound == NULL) {
        pface->phydro->pbound = AQ_create_boundary(kindof); // NF 26/9/2017 Je ne pense pas que ce soit bien de faire une copie. Je pense qu'il suffirait de gerer seulement la BC et de faire pointer pele->phydro->pbound directement vers pbound. Pour cela il faudra mettre en place un compteur precis de boundary par type par element
        pface->phydro->pbound->freq = pbound->freq;
        pface->phydro->pbound->pft = pbound->pft;
      } else
        LP_error(fpout, "libaq%4.2f %s in %s l%d: the face %s of ele %i in layer %s has a double NEUMANN BC. Please, check your BC input file\n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_name_neigh(idir, itype), MSH_get_ele_id(pele, GIS_MSH), pele->player->name);
    }
  } // if cardinal != CODE_AQ

  else {
    if (kindof == NEUMANN) // NF 6/12/2017 Completement con car la condition idir!=CODE_TS implique que kindof==NEUMANN ! Supprimer cette ligne
      LP_error(fpout, "libaq%4.2f %s in %s l.%d : a %s boundary can't be applied on element center", VERSION_AQ, __func__, __FILE__, __LINE__, AQ_name_BC(kindof));

    else { // NF 1/10/2017 This is OK for both DIRICHLET and CAUCHY
      if (pele->phydro->pbound != NULL) {
        LP_warning(fpout, "libaq%4.2f  %s in %s l%d: element ID_ABS %d ID_GIS %d is already  %s BC overcome into a %s BC. If it was a NEUMANN condition, we try to preserve it, but possibly a couple of bugs to come\n.", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, GIS_MSH), AQ_name_BC(pele->phydro->pbound->type), AQ_name_BC(kindof));
        pele->phydro->pbound->type = kindof;
      } else
        pele->phydro->pbound = AQ_create_boundary(kindof); // NF 26/9/2017 Je ne pense pas que ce soit bien de faire une copie. Je pense qu'il suffirait de gerer seulement la BC et de faire pointer pele->phydro->pbound directement vers pbound
      pele->phydro->pbound->freq = pbound->freq;
      pele->phydro->pbound->pft = pbound->pft;
    }

    if (kindof == CAUCHY) { // NF 26/9/2017 Allocation de la condition de CAUCHY a la face superieure
      pface = pele->face[Z_MSH][TWO_MSH];

      if (pface->phydro->pbound == NULL) { // NF 25/9/2017 Important to create the boundary condition otherwise segmentation fault (//NF 6/12/2017 probably for the RIV_AQ type of cell)
        pface->phydro->pbound = pele->phydro->pbound;
      } else {
        LP_warning(fpout, "libaq%4.2f %s in %s l.%d : \n\tConflicting boundary type on element %d, face TOP CAUCHY required while already a %s condition on the face. CAUCHY ignored\n", VERSION_AQ, __func__, __FILE__, __LINE__, pele->id[GIS_MSH], AQ_name_BC(pface->phydro->pbound->type));
      }
    }

    if (pele->type != BOUND) {
#ifdef DEBUG
      LP_warning(fpout, "The  ele (GIS_ID %i ABS_ID %i) is not a boundary element\n", MSH_get_ele_id(pele, GIS_MSH), MSH_get_ele_id(pele, ABS_MSH));
#endif
      pele->type = BOUND;
    } else if (pele->phydro->pbound != NULL && pele->phydro->pbound->type != kindof) {
      LP_warning(fpout, "libaq%4.2f %s in %s l.%d : The element %d is a %s BC. The implementation of multiple boundary condition on a same cell is not implemented yet. BC %s IGNORED\n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, INTERN_MSH), AQ_name_BC(pele->phydro->pbound->type), AQ_name_BC(pbound->type));
    }
    // pele->phydro->pbound=pbound;//NF 26/9/2017
  }

  return pele;
}

/**\fn void AQ_define_bound(s_cmsh *pcmsh,s_ft pft, int nlayer, s_bound_aq pbound,double ltot,FILE *fpout)
 *\brief define the boundaries of a list of elements Warning is pbound->type = NEUMANN in pft the unitary flux is strored !! (one must multiply by dx to have the correct flux in m.s-1)
 *\return void
 */
void AQ_define_bound(s_cmsh *pcmsh, s_id_io *id_list, s_ft *values, int dir, int kindof, double ltot, double surf_riv, double ep_riv_bed, double TZ_riv_bed, FILE *fpout) {
  s_layer_msh *player;
  s_ele_msh *pele;
  int id, nlayer;
  s_id_io *pidtmp;
  s_count_hydro_aq *pcount;
  s_bound_aq *pbound;
  pidtmp = id_list;
  pcount = pcmsh->pcount->pcount_hydro;
  pbound = AQ_define_bound_val(kindof, values, ltot, fpout);
  // LP_printf(fpout,"In libaq%5.3f %s l.%d dimension of pft : %d,first value : %f\n",VERSION_AQ,__FILE__,__LINE__,TS_length_ts(pbound->pft),pbound->pft->ft); //BL to debug

  if (pidtmp != NULL) {
    AQ_define_elebound(pidtmp, pcount->elebound, pbound->type, fpout);

    while (pidtmp != NULL) {
      nlayer = pidtmp->id_lay;
      player = pcmsh->p_layer[nlayer];
      id = pidtmp->id;

      pele = player->p_ele[id];
      pele = AQ_tab_boundaries(pele, dir, pbound, pcount->elebound, fpout);
      if (kindof == CAUCHY)
        AQ_define_pdescr_cauchy(pele, surf_riv, ep_riv_bed, TZ_riv_bed, fpout);

      free(pbound); // DK Correction memory leak, pbound is not freed after each allocation 31 01 2022
      pidtmp = pidtmp->next;
    }
  } else
    LP_warning(fpout, "libaq%4.2f %s in %s l%d : No cells declared for BC %s -> ignored\n", VERSION_AQ, __func__, __FILE__, __LINE__, AQ_name_BC(kindof));
}

/**\fn void AQ_define_pdescr_cauchy(s_ele_msh *pele, double surf_riv,double ep_riv_bed,double TZ_riv_bed,FILE *fpout)
 *\brief define the Cauchy parameters accounting on the geometrie of the stream bed properties for RIV_MSH cells
 *\return void
 */
void AQ_define_pdescr_cauchy(s_ele_msh *pele, double surf_riv, double ep_riv_bed, double TZ_riv_bed, FILE *fpout) {
  s_face_msh *pface;

  pface = pele->face[Z_MSH][TWO];
  if (pface->p_subface != NULL)
    LP_error(fpout, "there is an error in creating vertical face check libmesh !!!\n");
  if ((pface->loc == TOP_MSH) || (pface->loc == RIV_MSH)) {
    if (fabs(surf_riv - CODE_TS) > EPS_TS)
      pface->p_descr[TWO]->surf = surf_riv;
    if (fabs(ep_riv_bed - CODE_TS) > EPS_TS)
      pface->p_descr[TWO]->l_side = ep_riv_bed;
    if (pface->phydro->pcond == NULL) {
      pface->phydro->pcond = AQ_create_cond();
      LP_error(fpout, "In libaq%4.2f %s in %s l%d : Conductance of element %d (ABS %d) should be affected already\nThis has to be checked by developpers\n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_get_ele_id(pele, GIS_MSH), MSH_get_ele_id(pele, ABS_MSH));
    }
    if (fabs(TZ_riv_bed - CODE_TS) > EPS_TS) { // NF 9/4/2018
      double conduc;
      conduc = TZ_riv_bed;
      conduc /= ep_riv_bed;
      conduc *= surf_riv;
      pface->phydro->pcond->conductance = conduc;
    }
  } else
    LP_error(fpout, "In libaq%4.2f %s in %s l%d : CAUCHY BC are applied only on cells on top of the model the cell ABS_MSH : %d INTERN_MSH : %d GIS_MSH : %d id not a top cell \n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, INTERN_MSH), MSH_get_ele_id(pele, GIS_MSH));
}
/**\fn void AQ_set_default_BC(s_cmsh *pcmsh,FILE *fpout)
 *\brief set the default value of BC ie NULL FLUX
 *\return void
 */
int AQ_set_default_BC(s_ele_msh *pele, FILE *fpout) {

  s_face_msh *pface, *psubf;
  int idir, itype, m;
  int count, count_subf, default_set;
  default_set = 0;

  count = 0;
  /*
  if(pele->id[INTERN_MSH]==19)
    {
      LP_printf(fpout,"DEBUG");

      }*/
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      pface = pele->face[idir][itype];
      if (pface->p_subface != NULL) {
        count_subf = 0;
        for (m = 0; m < Z_MSH; m++) {
          psubf = pface->p_subface[m];
          if (psubf->type == BOUND && psubf->phydro->pbound == NULL) {
            psubf->phydro->pbound = AQ_create_boundary(NEUMANN);
            psubf->phydro->pbound->pft = TS_create_function(0., 0.);
            psubf->phydro->pbound->freq = CST;
            count_subf++;
          }
        }
        if (count_subf == 1) {
          LP_warning(fpout, "Only one subface is a boundary condition in ele %i face %s", MSH_get_ele_id(pele, GIS_MSH), MSH_name_neigh(idir, itype));
          count++;
        }
        if (count_subf == 2) {
          count++;
        }
      } else {
        if (pface->type == BOUND && pface->phydro->pbound == NULL) {
          pface->phydro->pbound = AQ_create_boundary(NEUMANN);
          pface->phydro->pbound->pft = TS_create_function(0., 0.);
          pface->phydro->pbound->freq = CST;
          count++;
        }
      }
    }
  }

  if (count > 0) {
    if (pele->phydro->pbound == NULL) {
      pele->phydro->pbound = AQ_create_boundary(NEUMANN);
      default_set = count;
    }
  }

  return default_set;
}

/**\fn void AQ_print_elebound(s_cmsh *pcmsh,FILE *fpout)
 *\brief print the elements which are boundary
 *\return void
 */
void AQ_print_elebound(s_cmsh *pcmsh, FILE *fpout) {
  int i;
  double Q_tot;
  s_id_io *ptmp;
  s_id_io **elebound;
  s_ele_msh *pele;
  s_face_msh *pface;
  s_ft *pft;
  double conductance;
  elebound = pcmsh->pcount->pcount_hydro->elebound;
  for (i = 0; i < NBOUND; i++) {

    LP_printf(fpout, "Boundary : %s\n", AQ_name_BC(i));
    if (elebound[i] != NULL) {
      ptmp = IO_browse_id(elebound[i], BEGINNING_TS);
      while (ptmp != NULL) {
        LP_printf(fpout, "Layer : %d cell : %d ABS_MSH : %d \n", ptmp->id_lay, ptmp->id, MSH_get_ele_id(pcmsh->p_layer[ptmp->id_lay]->p_ele[ptmp->id], ABS_MSH));
        pele = MSH_get_ele(pcmsh, ptmp->id_lay, ptmp->id, fpout);
        if (i == CAUCHY) {
          pface = pele->face[Z_MSH][TWO];
          LP_printf(fpout, "face type : %s\n", MSH_name_cell(pface->type, fpout));
          if (pface->phydro->pcond == NULL) {
            LP_warning(fpout, "no conductance is defined ! \n"); // NF 1/3/2017ici il faut indiquer ce que l'on fait dans ce cas, sinon Pb en perspective
          } else {
            conductance = pface->phydro->pcond->conductance;
            LP_printf(fpout, " conductance :  %e [m2/s]", conductance);
            conductance /= pele->pdescr->surf;
            LP_printf(fpout, ", specific conductance %e [s-1]\n", conductance);
          }
        }
        Q_tot += AQ_calc_bound_val_neuman(pele, fpout);
        ptmp = ptmp->next;
      }
    }
  }
  // LP_error(fpout,"débit total de neumann %f m3/s\n",Q_tot);
}

/**\fn s_bound_aq * AQ_define_bound_val(int kindof,s_ft *pft,double ltot)
 *\brief define boundaries values
 *\return pbound;
 */
s_bound_aq *AQ_define_bound_val(int kindof, s_ft *pft, double ltot, FILE *fpout) {
  s_bound_aq *pbound;
  s_ft *ptmp;
  pbound = AQ_create_boundary(kindof);
  if (kindof == NEUMANN) {
    //  LP_printf(fpout,"ltot = %f\n",ltot); //BL to debug
    // ptmp=TS_division_ft_double(pft,ltot);
    ptmp = TS_division_ft_double(pft, -ltot, fpout); // BL negatif pour les source !!!
    // ptmp=TS_division_ft_double(pft,-1,fpout);
    pft = TS_free_ft(pft);
  } else {
    ptmp = pft;
  }
  pbound->pft = ptmp;
  if (pbound->pft->next != NULL)
    pbound->freq = VAR;
  return pbound;
}

/**\fn void AQ_define_elebound(s_id_io *id_list,s_id_io **elebound,int kindof,FILE *fpout)
 *\brief add list of element in elebound
 *\return void
 */
void AQ_define_elebound(s_id_io *id_list, s_id_io **elebound, int kindof, FILE *fpout) {
  s_id_io *pcpy, *ptmp, *ptmp_id;
  int count = 0;

  if (elebound[kindof] != NULL) {
    // LP_printf(fpout,"adding id_list at elebound[%d]\n",kindof); //BL to debug
    ptmp_id = id_list;
    if (id_list == NULL)
      LP_error(fpout, "id_list null in define_elebound \n");
    while (ptmp_id != NULL) {
      ptmp = IO_browse_id(elebound[kindof], BEGINNING_TS);
      if (IO_is_element_id(ptmp_id, ptmp) == NO_MSH) {
        // LP_printf(fpout,"in AQ_define_ele_bound type %s\n",AQ_name_BC(kindof)); //BL ti debug
        // IO_print_id(ptmp_id,fpout); //BL to debug
        pcpy = IO_copy_id(ptmp_id);
        elebound[kindof] = IO_browse_id(elebound[kindof], END_TS);
        elebound[kindof] = IO_secured_chain_fwd_id(elebound[kindof], pcpy);
      }
      ptmp_id = ptmp_id->next;
    }
  } else {
    // LP_printf(fpout,"elebound[%d] not assigned yet\n",kindof);
    pcpy = IO_copy_id_serie(id_list, BEGINNING_TS);
    elebound[kindof] = pcpy;
  }
  // BL attention à voir s'il ne faut pas free pcpy !!!
}

double AQ_calc_bound_val_neuman(s_ele_msh *pele, FILE *fpout) {
  s_ft *pft;
  double Q_tot = 0;
  int bound_type, idir, itype, m;
  s_bound_aq *pbound;
  s_face_msh *pface, *psubf;

  if (pele->phydro->pbound != NULL) {
    bound_type = pele->phydro->pbound->type;

    if (bound_type == NEUMANN) {
      for (idir = 0; idir < ND_MSH; idir++) {
        for (itype = 0; itype < NFACE_MSH; itype++) {
          pface = pele->face[idir][itype];
          if (pface->p_subface != NULL) // MM 13/02/2019 cas de NEUMANN applique au subface non traite, l'initialisation de psubf->pbound doit d'abord etre traitee par la fonction AQ_set_default_BC ce qui ne semble pas etre le cas
          {

            for (m = 0; m < Z_MSH; m++) {
              psubf = pface->p_subface[m];
              if (psubf->type == BOUND && psubf->phydro->pbound != NULL) // MM 13/02/19 pbound doit etre initialise pour pouvoir y attribuer pft)
              {
                pft = psubf->phydro->pbound->pft;
                while (pft != NULL) {
                  Q_tot += pft->ft * psubf->p_ele[MSH_switch_direction(itype, fpout)]->pdescr->l_side;
                  pft = pft->next;
                }
              } else if (psubf->type == BOUND && psubf->phydro->pbound == NULL)
                LP_warning(fpout, "undeclared subface boundary of ele %d at face [%s][%s], subf %d\n", pele->id[ABS_MSH], MSH_name_direction(idir, fpout), MSH_name_type(itype, fpout), m);
            }
          } else if (pface->type == BOUND) {
            if (pface->phydro->pbound != NULL) { // NF 30/9/2017 otherwise NULL FLUX face
              pft = pface->phydro->pbound->pft;
              while (pft != NULL) {
                Q_tot += pft->ft * pface->p_ele[MSH_switch_direction(itype, fpout)]->pdescr->l_side;
                pft = pft->next;
              }
            }
          }
        }
      }
    }
  }
  return (Q_tot);
}

void AQ_print_bound_val(s_ele_msh *pele, FILE *fpout) {
  s_ft *pft, *plim;
  int bound_type, idir, itype, m;
  s_bound_aq *pbound;
  s_face_msh *pface, *psubf;

  // if (pele->phydro->pbound!=NULL) {
  bound_type = pele->phydro->pbound->type;
  if (bound_type == DIRICHLET || bound_type == CAUCHY) {
    pft = pele->phydro->pbound->pft;
    if (pft == NULL)
      LP_error(fpout, "libaq%4.2f %s in %s l%d : No head value defined for boundary %s at ele INTERN_MSH %d ABS_MSH %d GIS_MSH %d \n", VERSION_AQ, __func__, __FILE__, __LINE__, AQ_name_BC(bound_type), pele->id[INTERN_MSH], pele->id[ABS_MSH], pele->id[GIS_MSH]);
    while (pft != NULL) {
      LP_printf(fpout, " value of imposed_head : %f [m] at time %f\n", pft->ft, pft->t); // NF 1/3/2017
      pft = pft->next;
    }
  }

  if (bound_type == NEUMANN) {
    for (idir = 0; idir < ND_MSH; idir++) {
      for (itype = 0; itype < NFACE_MSH; itype++) {
        pface = pele->face[idir][itype];
        if (pface->p_subface != NULL) // MM cas de NEUMANN applique au subface non traite
        {
          for (m = 0; m < Z_MSH; m++) {
            psubf = pface->p_subface[m];
            if (psubf->type == BOUND && psubf->phydro->pbound != NULL) // MM 13/02/19 pbound doit etre initialise pour pouvoir y attribuer pft
            {
              pft = psubf->phydro->pbound->pft;
              while (pft != NULL) {
                // LP_printf(fpout," value of boundary at face [%s][%s] subf %d : %f at time %f\n",MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout),m,pft->ft,pft->t);
                pft = pft->next;
              }
            }
          }
        } else if (pface->type == BOUND) {
          if (pface->phydro->pbound != NULL) {
            pft = pface->phydro->pbound->pft;
            while (pft != NULL) {
              // LP_printf(fpout," value of boundary at face [%s][%s] : %f at time %f\n",MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout),pft->ft,pft->t);
              pft = pft->next;
            }
          } else {
            // LP_printf(fpout," face [%s][%s] is a NULL flux boundary\n",MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout));
          }
        }
      }
    }
  }
  //  }
  // else{
  //	  LP_printf(fpout,"Not a BC. May be a surface cell without CAUCHY condition\n");//NF 1/10/2017
  // }
}

void AQ_set_qlim(s_cmsh *pcmsh, s_id_io *id_list, double qlim, int kindof, FILE *fpout) {
  s_layer_msh *player;
  int id;
  int nlayer;
  s_ele_msh *pele;
  s_id_io *ptmp;
  s_bound_aq *pbound;
  ptmp = id_list;
  if (ptmp != NULL) {
    nlayer = ptmp->id_lay;
    player = pcmsh->p_layer[nlayer];
  }

  while (ptmp != NULL) {
    id = ptmp->id;
    pele = player->p_ele[id];
    // pbound=pele->phydro->psource[kindof];
    pbound = pele->phydro->pbound;
    pbound->qlim = qlim; // NF 4/3/2017 Ici nous pourrons mettre un *pft en son temps sans gros pb;
    ptmp = ptmp->next;
  }
}

double AQ_calculate_cauchy_q(double conduc, double haq, double hcauchy, double qlim) {
  double qpot = 0;

  // qpot=hcauchy-haq;
  qpot = haq - hcauchy;
  qpot *= conduc;
  qpot = TS_max(qpot, qlim); // NF 30/9/2017 Si j'ai bonne memoire, la convention de signe dans libaq est qu'un apport a l'aquifer est negatif. Qpot est calcule avec Hnap-h_sur. Une infiltration est donc negative. Il faut donc prendre le max et non pas le min ici

  return qpot;
}

/*Launches the connexion status check for all TOP_MSH cells with a CAUCHY BC*/
void AQ_check_cauchy_top(double dt, double t, s_carac_aq *pchar_aq, FILE *fp) {
  s_id_io *cauchy;
  s_ele_msh *pele_aq;
  s_face_msh *pface_aq;
  s_gc *pgc;
  double theta;
  theta = pchar_aq->settings->general_param[THETA_AQ];
  pgc = pchar_aq->calc_aq;
  cauchy = pchar_aq->pcmsh->pcount->pcount_hydro->elebound[CAUCHY];
  cauchy = IO_browse_id(cauchy, BEGINNING_TS);
  while (cauchy != NULL) {
    pele_aq = pchar_aq->pcmsh->p_layer[cauchy->id_lay]->p_ele[cauchy->id];
    pface_aq = pele_aq->face[Z_MSH][TWO];
    if (pface_aq->loc == TOP_MSH)
      AQ_check_qlim_top(pele_aq, pgc, dt, theta, t, fp);
    cauchy = cauchy->next;
  }
}

/*Checks connexion status between the aquifer and the surface and corrects the LHS if needed. Not used for RIV_MSH cells. Here it is essentially the case of the soil surface. More random usage for estuaries or wetlands*/
void AQ_check_qlim_top(s_ele_msh *pele_aq, s_gc *pgc, double dt, double theta, double t, FILE *flog) {
  s_id_io *id_riv;
  s_face_msh *pface_aq;
  int id_abs, id_mat6, nb_inf, modif_old;
  double q, qlim;
  double h_riv0, h_riv1;
  s_ft *pft, *ptmp, *pft_old;

  pface_aq = pele_aq->face[Z_MSH][TWO];
  if (pface_aq->loc == TOP_MSH) { // NF 23/9/2017 Attention les cas de faces horizontales en CAUCHY ne sont pas prévus ici (typiquement les berges des gravieres
    // NF 18/10/2017 Il faut d'abord faire une extraction exacte puis prendre la valeur des bornes
    pft = TS_browse_ft(pele_aq->phydro->pbound->pft, BEGINNING_TS);
    // printf("1\n");
    h_riv0 = TS_interpolate_ts(t - dt, pft);
    pft = TS_browse_ft(pele_aq->phydro->pbound->pft, BEGINNING_TS);
    // printf("2\n");
    h_riv1 = TS_interpolate_ts(t, pft);
    //    LP_printf(flog,"Check cauchy top (apres resol syst): idabs=%d h_riv1=%f\n",pele_aq->id[ABS_MSH],h_riv1);
    q = pface_aq->phydro->pcond->conductance * (pele_aq->phydro->h[PIC] - h_riv1); // FB 08/02/18
    qlim = pface_aq->phydro->pbound->qlim;

    modif_old = pele_aq->modif[MODIF_CALC_AQ];
    pele_aq->modif[MODIF_MB_AQ] = modif_old;

    if (qlim > q) {
      pele_aq->modif[MODIF_CALC_AQ] = QLIM_AQ;
      // pele_aq->phydro->Qlim=qlim;//NF 18/10/2017 Normalement inutile car dejé initialise

    } else
      pele_aq->modif[MODIF_CALC_AQ] = NO_LIM_AQ;

    if (pele_aq->modif[MODIF_CALC_AQ] == QLIM_AQ) {
      if (modif_old == NO_LIM_AQ) // il faut désactiver CAUCHY aq
      {
        id_abs = pele_aq->id[ABS_MSH];
        id_mat6 = pgc->mat4[id_abs];
        AQ_correct_LHS(pgc, id_mat6, theta, dt, pface_aq, COR_NEG_AQ);
      }
    } else if (pele_aq->modif[MODIF_CALC_AQ] == NO_LIM_AQ && modif_old == QLIM_AQ) { // il faut reactiver CAUCHY aq
      id_abs = pele_aq->id[ABS_MSH];
      id_mat6 = pgc->mat4[id_abs];
      AQ_correct_LHS(pgc, id_mat6, theta, dt, pface_aq, COR_POS_AQ);
    }
  }
}

/**\fn void AQ_check_compatibility_DIRICHLET_initial_state(s_carac_aq *pcarac_aq,FILE *fpout)
 *\brief function which ensures the compatibility between initial state and DIRICHLET boundary condition. If not done, this creates a bug for the calculation of a steady state, and mass balance errors for the first time step of a transient calculation
 *\return void
 *
 */
void AQ_check_compatibility_DIRICHLET_initial_state(s_carac_aq *pcarac_aq, FILE *fpout) {
  s_cmsh *pcmsh;
  s_id_io ***p_eletype;
  int *n_eletype;
  int neletype;
  int e;
  s_id_io **elebound;
  s_id_io *bound;
  s_ele_msh *pele;

  pcmsh = pcarac_aq->pcmsh;
  n_eletype = pcmsh->pcount->pcount_hydro->nele_type;
  p_eletype = pcmsh->pcount->pcount_hydro->p_eletype;
  neletype = n_eletype[DIRICHLET];
  elebound = p_eletype[DIRICHLET];

  for (e = 0; e < neletype; e++) {
    bound = elebound[e];
    pele = MSH_get_ele(pcmsh, bound->id_lay, bound->id, fpout);

    if (fabs(pele->phydro->h[T] - pele->phydro->pbound->pft->ft) > EPS_TS) {
      LP_warning(fpout, "libaq%4.2f %s in %s l%d: incompatible initial state with DIRICHLET BC in ele %d (ABS_ID), inGIS_ID %d in layer %s. BC value %f at t=%f instead of initial value %f. Must be corrected!!!\n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, GIS_MSH), pele->player->name, pele->phydro->pbound->pft->ft, pele->phydro->pbound->pft->t,
                 pele->phydro->h[T]);
      pele->phydro->h[T] = pele->phydro->pbound->pft->ft;
    }
  }
}

/**\fn void AQ_init_cauchy_qlim(s_carac_aq *pcarac_aq,FILE *fpout)
 *\brief function which initializes the value of pele->phydro->Qlim for Cauchy boundary condition. This is this value that is used to estimate the RHS and not the pbound->qlim, see in manage_mat.c double AQ_calc_b_i_cauchy(s_ele_msh *pele,double deltat,double theta,double t,int idir,int itype,FILE *fpout). This value is needed for the SW_GW configuration (in casenot enough water is in the river).
 *\return void
 *
 */
void AQ_init_cauchy_qlim(s_carac_aq *pcarac_aq, FILE *fpout) {
  s_cmsh *pcmsh;
  s_id_io ***p_eletype;
  int *n_eletype;
  int neletype;
  int e;
  s_id_io **elebound;
  s_id_io *bound;
  s_ele_msh *pele;

  pcmsh = pcarac_aq->pcmsh;
  n_eletype = pcmsh->pcount->pcount_hydro->nele_type;
  p_eletype = pcmsh->pcount->pcount_hydro->p_eletype;
  neletype = n_eletype[CAUCHY];
  elebound = p_eletype[CAUCHY];

  for (e = 0; e < neletype; e++) {
    bound = elebound[e];
    pele = MSH_get_ele(pcmsh, bound->id_lay, bound->id, fpout);

    pele->phydro->Qlim = pele->phydro->pbound->qlim;
  }
}
