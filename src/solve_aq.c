/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: solve_aq.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"
#include "spa.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"
#ifdef OMP
#include "omp.h"
#endif

double AQ_do_one_pic_it(int *iit, double t, double dt, s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_clock_CHR *clock, int idebug, int *it_count, FILE *fpout) {
  double pic_max;

  iit[0] += 1;
  LP_printf(fpout, "\nPicard iteration : %d\n", *iit);
  pic_max = AQ_solve_diffusiv_pic(pchar_aq, chronos, clock, idebug, it_count, fpout);
  AQ_refresh_mat6_unconfined(pchar_aq, chronos, fpout); // NF 26/11/2018 For unconfined problem, it is needed to call AQ_refresh_mat6_unconfined() first that refreshes the LHS
  AQ_check_cauchy_top(dt, t, pchar_aq, fpout);

  return pic_max;
}

// NG : 09/06/2021 : Counter of total number of Steady iteration added
double AQ_solve_diffusiv_steady_one_it(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_clock_CHR *clock, s_out_io **pout, int idebug, int *it_count, FILE *fpout) {

  int err, i;
  double drawd;
  double t, dt;
  s_gc *pgc;
  s_cmsh *pcmsh;
  s_param_aq *param;
  int iit, nitmax, nitmin;
  double pic_max, eps_pic, eps_Z;
  i = 0;
  drawd = 0;
  pic_max = (double)CODE_TS;

  pgc = pchar_aq->calc_aq;
  pcmsh = pchar_aq->pcmsh;
  param = pchar_aq->settings;

  nitmax = pchar_aq->ppic->nitmax;
  nitmin = pchar_aq->ppic->nitmin;
  eps_pic = pchar_aq->ppic->eps_pic;
  iit = 0;
  t = DEFAULT_T_AQ;
  dt = DEFAULT_DT_AQ;

  if (nitmin > 1) { // Case transient with CAUCHY Boundaries or case STEADY
    while (iit < nitmin) {
      pic_max = AQ_do_one_pic_it(&iit, t, dt, pchar_aq, chronos, clock, idebug, it_count, fpout); // NG : 09/06/2021 : It_count added
    }
    while ((iit < nitmax) && (fabs(pic_max) > eps_pic)) {
      pic_max = AQ_do_one_pic_it(&iit, t, dt, pchar_aq, chronos, clock, idebug, it_count, fpout); // NG : 09/06/2021 : It_count added
    }
    if ((iit == nitmax) && (fabs(pic_max) > eps_pic)) {
      LP_warning(fpout, "Picard loop failed to converge after %d iterations\n", iit);
    }
  } else // No CAUCHY Boundaries in transient case
  {
    AQ_refresh_mat6_unconfined(pchar_aq, chronos, fpout);                               // NF 26/11/2018 For unconfined problem, it is needed to call AQ_refresh_mat6_unconfined() first that refreshs the LHS
    pic_max = AQ_solve_diffusiv_pic(pchar_aq, chronos, clock, idebug, it_count, fpout); // NG : 09/06/2021 : it_count added
  }
  CHR_begin_timer();
  drawd = AQ_calc_drawd(pchar_aq, fpout);
  AQ_refresh_H_interpol(pchar_aq, NULL, NULL, fpout);
  // AQ_fill_mb_ini_end(pchar_aq,chronos,pout,fpout);//FB To debug, if we want to print mass balace at each steady state iteration
  return drawd;
}

void AQ_solve_diffusiv_steady(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_clock_CHR *clock, s_out_io **pout, int idebug, FILE *fpout) {

  int err, i, j, k, nele;
  double drawd;
  double t, dt;
  int nlayer;
  s_gc *pgc;
  s_cmsh *pcmsh;
  s_param_aq *param;
  s_ele_msh *pele;
  s_layer_msh *play;
  int iit, nitmax, nitmin;
  double pic_max, eps_pic, eps_Z;
  i = 0;
  drawd = 0;
  pic_max = (double)CODE_TS;

  pgc = pchar_aq->calc_aq;
  pcmsh = pchar_aq->pcmsh;
  param = pchar_aq->settings;

  // nitmax=pchar_aq->ppic->nitmax;//
  nitmax = pchar_aq->steadynitmax; // FB 01/02/2018 Nombre iterations max pour le steady state != nombre max it Picard
  eps_pic = pchar_aq->ppic->eps_pic;
  iit = 0; // NG : 09/06/2021 : Counter of total number of steady iterations

  /*LP_printf(fpout,"Taking an implicit scheme for steady state\n");
  pchar_aq->settings->general_param[THETA_AQ]=1.;
  *///NF 5/1/2018 Too late here, must be done in the calling program
  pic_max = BIG_TS;

  AQ_fill_state(pchar_aq, chronos, pout, INI_AQ, fpout);

  while ((iit < nitmax) && (fabs(pic_max) > eps_pic)) {
    LP_printf(fpout, "\n\n STEADY STATE iteration : %d\n", iit);
    pic_max = AQ_solve_diffusiv_steady_one_it(pchar_aq, chronos, clock, pout, idebug, &iit, fpout); // NG : 09/06/2021 Global counter of Steady iterations added

    // print mb a chaque iteration du steady (for debug)
    // if(pout[MASS_IO]!=NULL){
    //   AQ_print_output(chronos,(double)iit,pchar_aq,pout[MASS_IO],MASS_IO,fpout);
    //  }

    iit++;
  }

  if (iit < nitmax)
    LP_printf(fpout, "Steady state converged\n");
  else
    LP_printf(fpout, "No convergence of STEADY STATE, error still = %f after %d it\n", pic_max, iit);

  /* drawd=AQ_calc_drawd(pchar_aq,fpout);
  AQ_refresh_H_interpol(pchar_aq,NULL,NULL,fpout);
  */ //FB 19/02/18 Je ne comprends pas a quoi ca sert. Ca repete les operations faites apres la derniere iteration de Picard ?
  AQ_fill_state(pchar_aq, chronos, pout, END_AQ, fpout);
  AQ_calc_mb_end(pchar_aq, chronos, pout, fpout);
}

double AQ_solve_diffusiv_pic(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_clock_CHR *clock, int idebug, int *it_count, FILE *fpout) {

  int err, i, k, j;
  int nlayer;
  int nele, id_ele, neletot;
  double pic_max;
  double t;

  s_gc *pgc;
  s_cmsh *pcmsh;
  s_layer_msh *play;
  s_ele_msh *pele;
  s_param_aq *param;

  k = 0;
  pic_max = 0;
  pgc = pchar_aq->calc_aq;
  pcmsh = pchar_aq->pcmsh;
  param = pchar_aq->settings;
  nlayer = pcmsh->pcount->nlayer;

  CHR_begin_timer();

  // MM AR 8/10/2018 Add recalc LHS for unconfined layer
  // NF 26/11/2018 Create a function AQ_refresh_mat6_unconfined()
  // NF 9/10/2018 bouger AQ_calc_vector_b ici
  AQ_calc_vector_b(pchar_aq, chronos, chronos->t[CUR_CHR], fpout);

  if (idebug == YES_TS) {
    AQ_print_H(pchar_aq->pcmsh, T, fpout);
    AQ_print_LHS_RHS(pchar_aq->pcmsh, pchar_aq->calc_aq, fpout);
  }

  clock->time_spent[MAT_FILL_AQ] += CHR_end_timer();
  CHR_begin_timer();

  // LP_printf(fpout,"In libaq%4.2f : In file %s, function %s : Global counter value = %d\n",VERSION_AQ,__FILE__,__func__,*it_count);  // NG check
  GC_configure_gc(pgc, it_count, fpout); // NG : 09/06/2021
  GC_solve_gc(pgc, AQ_GC, fpout);        // NG : 09/06/2021 prototype modified

  clock->time_spent[MAT_SOLVE_AQ] += CHR_end_timer();
  CHR_begin_timer();
  pic_max = AQ_calc_pic(pchar_aq, fpout);
  if (idebug == YES_TS)
    AQ_print_H(pchar_aq->pcmsh, PIC, fpout);
  clock->time_spent[MAT_GET_AQ] += CHR_end_timer();
  return pic_max;
}

void AQ_refresh_state_variable(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_clock_CHR *clock, s_out_io **pout, FILE *fpout) {

  s_gc *pgc;
  s_cmsh *pcmsh;
  double drawd;
  drawd = 0;

  pgc = pchar_aq->calc_aq;
  pcmsh = pchar_aq->pcmsh;
  CHR_begin_timer(); // LV 3/09/2012
  drawd = AQ_calc_drawd(pchar_aq, fpout);
  // LP_printf(fpout,"Drawdown max during time step : %e\n",drawd);
  AQ_refresh_H_interpol(pchar_aq, chronos, pout[MASS_IO], fpout); // FB 27/02/18 Keyword PIEZ_IO does not exist anymore
  // AQ_calc_mb_face_All(pchar_aq,chronos,pout,fpout);//FB 16/02/18
  AQ_update_aq_system_hydraulic_state(pcmsh, fpout);     // NG : 17/04/2024
  AQ_fill_state(pchar_aq, chronos, pout, END_AQ, fpout); // NF 29/11/2018
  AQ_calc_mb_end(pchar_aq, chronos, pout, fpout);        // FB 16/02/18//NF 29/11/2018
  clock->time_spent[MAT_GET_AQ] += CHR_end_timer();
}

double AQ_calc_eps(s_gc *pgc, s_cmsh *pcmsh, FILE *fpout) {

  double eps = -9999.0;
  double diff;
  int nlayer, nele, k, j;
  s_layer_msh *player;
  s_ele_msh **p_ele, *pele;
  nlayer = pcmsh->pcount->nlayer;
  for (k = 0; k < nlayer; k++) {
    player = pcmsh->p_layer[k];
    nele = player->nele;
    p_ele = player->p_ele;
    for (j = 0; j < nele; j++) {
      pele = p_ele[j];
      // LP_printf(fpout,"ele %d H[T] = %f H_gc = %f\n",MSH_get_ele_id(pele,ABS_MSH),pele->phydro->h[T],pgc->x[MSH_get_ele_id(pele,ABS_MSH)-1]);
      diff = pele->phydro->h[T] - pgc->x[MSH_get_ele_id(pele, ABS_MSH) - 1];

      // LP_printf(fpout,"diff : %f\n",diff);
      eps = TS_max(eps, fabs(diff));
      // LP_printf(fpout,"eps : %f \n",eps);
    }
  }
  return eps;
}

double AQ_calc_drawd(s_carac_aq *pchar_aq, FILE *fpout) {

  double drawd2;
  int nlayer, nele, k, j, id_draw;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele;
  s_gc *pgc;
  s_cmsh *pcmsh;
#ifdef OMP
  s_smp *psmp;
  psmp = pchar_aq->settings->psmp;
#endif
  drawd2 = -9999.0;
  pcmsh = pchar_aq->pcmsh;
  pgc = pchar_aq->calc_aq;
  nlayer = pcmsh->pcount->nlayer;
  for (k = 0; k < nlayer; k++) {
    player = pcmsh->p_layer[k];
    nele = player->nele;
    p_ele = player->p_ele;
#ifdef OMP
    int taille;
    int nthreads;
    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);

    psmp->chunk = PC_set_chunk_size_silent(fpout, player->nele, nthreads);
    taille = psmp->chunk;
#pragma omp parallel shared(p_ele, drawd2, nthreads, taille) private(j, pele)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (j = 0; j < nele; j++) {
        pele = p_ele[j];
        // pele->phydro->h[DRAWD]=pele->phydro->h[ITER]-pgc->x[MSH_get_ele_id(pele,ABS_MSH)-1];
        // printf("\n Dans calc drawd idabs=%d\n",pele->id[ABS_MSH]);
        // printf("pele->phydro->h[T]=%lf\n",pele->phydro->h[T]);
        // printf("pgc->x[MSH_get_ele_id(pele,ABS_MSH)-1]=%lf\n",pgc->x[MSH_get_ele_id(pele,ABS_MSH)-1]);
        pele->phydro->h[DRAWD] = pele->phydro->h[T] - pgc->x[MSH_get_ele_id(pele, ABS_MSH) - 1]; // FB 08/02/18 h[T] est la charge a l'iteration precedente, h[ITER] est a deux iterations precedentes!
#ifdef OMP
#pragma omp critical
#endif
        // drawd2=TS_max(drawd2,fabs(pele->phydro->h[DRAWD]));
        if (fabs(pele->phydro->h[DRAWD]) > drawd2) { // FB 08/02/18 Pour imprimer l'element ou drawd2 est max
          drawd2 = fabs(pele->phydro->h[DRAWD]);     // FB
          id_draw = pele->id[ABS_MSH];               // FB
        }                                            // FB
      }
#ifdef OMP

    } // end of parallel section
#endif
  }
  // LP_printf(fpout,"Maximum drawdown for time step: %e [m]\n",drawd2);
  LP_printf(fpout, "Maximum drawdown for time step: %e [m] in element ABS_MSH=%d\n", drawd2, id_draw); // FB 08/02/18
  return drawd2;
}

void AQ_refresh_H_interpol(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_out_io *pout, FILE *fpout) {
  int nlayer, nele, k, j;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele;
  double t, dt, t_out;
  s_cmsh *pcmsh;
  s_gc *pgc;
#ifdef OMP
  s_smp *psmp;
  psmp = pchar_aq->settings->psmp;
#endif
  pcmsh = pchar_aq->pcmsh;
  pgc = pchar_aq->calc_aq;
  nlayer = pcmsh->pcount->nlayer;
  if (chronos != NULL) {
    dt = chronos->dt;
    t = chronos->t[CUR_CHR];
    t_out = pout->t_out[CUR_IO];
  } else {
    t = t_out = (double)CODE_TS;
    dt = DT_CHR;
  }

  for (k = 0; k < nlayer; k++) {
    player = pcmsh->p_layer[k];
    nele = player->nele;
    p_ele = player->p_ele;
#ifdef OMP
    int taille;
    int nthreads;
    // LP_printf(fpout,"OMP source\n"); //BL to debug
    nthreads = psmp->nthreads;
    // LP_printf(fpout,"Nthreads = %d\n",nthreads); //BL to debug
    omp_set_num_threads(psmp->nthreads);

    psmp->chunk = PC_set_chunk_size_silent(fpout, player->nele, nthreads);
    taille = psmp->chunk;
    // LP_printf(fpout,"Taille = %d \n",taille); //BL to debug
#pragma omp parallel shared(p_ele, t_out, t, dt, nthreads, taille) private(j, pele)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (j = 0; j < nele; j++) {
        pele = player->p_ele[j];
        pele->phydro->h[ITER] = pele->phydro->h[T];
        pele->phydro->h[ITER_INTERPOL] = pele->phydro->h[INTERPOL];
        pele->phydro->h[T] = pgc->x[MSH_get_ele_id(pele, ABS_MSH) - 1];
        if (t_out < t && t_out > t - dt) {
          pele->phydro->h[INTERPOL] = TS_interpol(pele->phydro->h[ITER], t - dt, pele->phydro->h[T], t, t_out);
        } else
          pele->phydro->h[INTERPOL] = pele->phydro->h[T];
      }
#ifdef OMP

    } // end of parallel section
      // LP_printf(fpout,"done\n"); //BL to debug
#endif
  }
}

double AQ_calc_pic(s_carac_aq *pchar_aq, FILE *fpout) {

  double drawd2;
  int nlayer, nele, k, j, id_drawd;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele;
  s_cmsh *pcmsh;
  s_gc *pgc;

#ifdef OMP
  s_smp *psmp;
  psmp = pchar_aq->settings->psmp;
#endif

  // drawd2=(double)CODE_TS;
  drawd2 = 0;
  pcmsh = pchar_aq->pcmsh;
  pgc = pchar_aq->calc_aq;
  nlayer = pcmsh->pcount->nlayer;
  for (k = 0; k < nlayer; k++) {
    player = pcmsh->p_layer[k];
    nele = player->nele;
    p_ele = player->p_ele;

#ifdef OMP
    int taille;
    int nthreads;
    // LP_printf(fpout,"OMP source\n"); //BL to debug
    nthreads = psmp->nthreads;
    // LP_printf(fpout,"Nthreads = %d\n",nthreads); //BL to debug
    omp_set_num_threads(psmp->nthreads);

    psmp->chunk = PC_set_chunk_size_silent(fpout, player->nele, nthreads);
    taille = psmp->chunk;
    // LP_printf(fpout,"Taille = %d \n",taille); //BL to debug
#pragma omp parallel shared(p_ele, drawd2, nthreads, taille) private(j, pele)
    {
#pragma omp for schedule(dynamic, taille)
#endif

      for (j = 0; j < nele; j++) {
        pele = p_ele[j];
        pele->phydro->h[DRAWD] = pele->phydro->h[PIC] - pgc->x[MSH_get_ele_id(pele, ABS_MSH) - 1]; // BL enregistrement temporaire dans DRAWD !!!

        pele->phydro->h[PIC] = pgc->x[MSH_get_ele_id(pele, ABS_MSH) - 1]; // BL enregistremant temporaire dans INTERPOL !!
#ifdef OMP
#pragma omp critical
#endif
        if (fabs(pele->phydro->h[DRAWD]) > fabs(drawd2)) {
          drawd2 = pele->phydro->h[DRAWD];
          id_drawd = pele->id[ABS_MSH];
        }
      }

#ifdef OMP
    } // end of parallel section
#endif
  }
  LP_printf(fpout, "Maximum head discrepancy with respect to previous Picard iteration : %e in element ABS_MSH %d\n\n", drawd2, id_drawd);
  return drawd2;
}
