/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_link.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

void AQ_init_link_ele(s_ele_msh *pele, int id, double prct, int link_type, FILE *fp) {
  s_id_spa *link_tmp;

  link_tmp = SPA_create_ids(id, prct);
  pele->link[link_type] = SPA_secured_chain_id_fwd(pele->link[link_type], link_tmp);
}

// NG : 05/03/2020 : Print all connexions with aquifer cells for a given link_type
void AQ_print_links_ele(s_carac_aq *pcarac_aq, int link_type, FILE *fp) {

  s_cmsh *pcmsh = pcarac_aq->pcmsh;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele;
  int i, j, nlayer, nele;
  s_id_spa *link;

  nlayer = pcmsh->pcount->nlayer;

  LP_printf(fp, "Links between aquifer cells and objets from %s :\n", AQ_name_link_aq(link_type));
  for (i = 0; i < nlayer; i++) {
    player = pcmsh->p_layer[i];
    nele = player->nele;
    p_ele = player->p_ele;
    for (j = 0; j < nele; j++) {
      pele = p_ele[j];
      link = pele->link[link_type];
      if (link != NULL) {
        LP_printf(fp, "Aquifer cell INTERN ID %d layer ID %d linked with :\n", pele->id[INTERN_MSH], player->id);
        SPA_print_ids(link, fp);
      }
    }
  }
}
