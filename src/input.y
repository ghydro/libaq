/*-------------------------------------------------------------------------------
* 
* LIBRARY NAME: libaq
* FILE NAME: input.y
* 
* CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS, 
*               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT, 
*               Shuaitao WANG
* 
* LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
* using a semi-implicit temporal scheme: confined-unconfined aquifer units, 
* 2D diffusivity equation in each layer, 1D vertical exchanges between layers 
* with a linear drainance model, 1D vertical exchanges at the soil surface 
* or for river with a conductance model, subcell lithology.
*
* Library developed at the Geosciences Center, joint research center 
* of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
*
* COPYRIGHT: (c) 2022 Contributors to the libaq Library.
* CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
*          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
*
* All rights reserved. This Library and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v20.html
* 
*------------------------------------------------------------------------------*/


%{
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <libprint.h>
#include <time_series.h>
#include "IO.h"
#include "CHR.h"
#include "AQ.h"
#include "global_AQ.h"//only for the main program
#include "ext_AQ.h"//extern variables



  char name_layer[STRING_LENGTH_LP]; 
  s_layer_msh *player;
  s_ele_msh *pele;
  s_bound_aq *pbound;
  s_ft *pft;
  s_id_io *id_list;
  int ncoord=0;
  int coordo=0;
  int nele=0;
  int verif_layer=0;
  int verif_ele=0;
  int nlayer=0;
  int i=0;
  int id_new=0;
  int nm=0;
  int intern_id = 0;
  int nele_tot=1;
  int  kindof_attribut,kindof;
  int idir=1;
  int kindof_transm,kindof_BC,kindof_SOURCE;
  int nelebound;
  double ltot;
  double stot;
  double surf_riv=0;
  double ep_riv_bed = 0;
  double TZ_riv_bed = 0;
  double simul_length=0; /*!< define the length of the simulation the dimention is deltat[OUTPUT] */
  double t_init=0;
  double deltat=0;
  //id_list=NULL;
  /*#if defined GCC481 ||  defined GCC473 || defined GCC472 ||  defined GCC471
void yyerror(char const *);
#else
  void yyerror(char *);
#endif //test on GCC
  int yylex();*/
%}


%union {
  /* initial pour ProSe:*/
  double real;
  int integer;
  char *string;
  s_layer_msh *gw_layer;
  s_ele_msh *gw_ele;
  s_ft *struct_ft;
  s_id_io *struct_id;
}

%token <real> LEX_DOUBLE LEX_UNIT LEX_A_UNIT LEX_VAR_UNIT LEX_UNIT_MOL 
%token <integer> LEX_INT LEX_MESH_ATT LEX_DIR LEX_TYPE_BC LEX_TYPE_MEAN LEX_TYPE_SOURCE LEX_SIM LEX_ANSWER LEX_TYPE_COND LEX_FORMAT
%token  LEX_EQUAL LEX_OPENING_BRACE LEX_CLOSING_BRACE LEX_COLON LEX_SEMI_COLON LEX_VIRGULE LEX_INV  LEX_OPENING_BRACKET LEX_CLOSING_BRACKET LEX_POW LEX_INPUT_FOLDER LEX_OUTPUT_FOLDER LEX_AVIEW LEX_NEWSAM  LEX_LAYER LEX_SETUP_LAYER LEX_SET_UP LEX_MEAN LEX_BOUND LEX_SOURCE LEX_TIME_S LEX_PARAM LEX_PARAM_RIV
%token LEX_SIM_SET LEX_SIGMA LEX_SIM_LENGTH LEX_SIM_TYPE LEX_DELTA LEX_SURF LEX_EPSILON LEX_NIT LEX_COND LEX_T_INIT LEX_FORMAT_TYPE
%token <string> LEX_NAME 


%type <real> flottant
%type <gw_layer> layer layers
%type <gw_ele>  lire_eles lire_ele lecture_eles
%type <struct_ft> series serie
%type <struct_id> ele_ids ele_id
%type <string> le_name

%start beginning
%%

  


beginning : paths
            simul_setup
            create_mesh
            create_faces
            specify_simul {
  s_cmsh *pcmsh;
  pcmsh=Simul->pcmsh;
  LP_printf(Simul->poutputs,"Model %s\n",Simul->name);
  MSH_carac_mesh(pcmsh,Simul->poutputs);
};


/* This part enables to define specific input and output folders */
paths : path paths
| path
;

path : input_folders
| output_folder
;

input_folders : LEX_INPUT_FOLDER folders
;

/* List of input folders' names */
folders : folder folders
| folder 
;

folder : LEX_EQUAL LEX_NAME
{
  if (folder_nb >= NPILE) 
    LP_printf(Simul->poutputs,"Only %d folder names are available as input folders\n",NPILE);
  
  name_out_folder[folder_nb++] = strdup($2);
  LP_printf(Simul->poutputs,"path : %s\n",name_out_folder[folder_nb-1]);
  free($2);
} 
;

/* Definition of the folder where outputs are stored */
output_folder : LEX_OUTPUT_FOLDER LEX_EQUAL LEX_NAME
{
  int noname;
  char *new_name;
  char cmd[ STRING_LENGTH_LP];
  FILE *fp;

  fp=Simul->poutputs;

  Simul->name_outputs = $3;
  new_name = (char *)calloc(strlen($3) + 10,sizeof(char));
  sprintf(new_name,"RESULT=%s",$3);
  LP_printf(fp,"%s\n",new_name);
  noname = putenv(new_name); 
  if (noname == -1)
    LP_error(fp,"File %s, line %d : undefined variable RESULT\n",
		current_read_files[pile].name,line_nb); 
  sprintf(cmd,"mkdir %s",getenv("RESULT")); 
  system(cmd);
} 
;

simul_setup : LEX_SIM_SET LEX_EQUAL brace sim_params brace 
{
  if(Simul->param->sim_type==STEADY)
    {
      simul_length=1; //BL to test
      deltat=1;
      Simul->param->a0=0;
      Simul->param->theta=1;
    }
  else
    {
      Simul->param->a0=1;
      
    }

  AQ_init_time(Simul->param,t_init,simul_length,deltat);
  IO_init_time(Simul->pout,t_init,simul_length,deltat);

}
;

sim_params : sim_param sim_params
| sim_param
;

sim_param : LEX_SIGMA LEX_EQUAL flottant
{
  Simul->param->theta=$3;
}
| LEX_SIM_LENGTH LEX_EQUAL flottant
{
  simul_length=$3;
}
| LEX_T_INIT LEX_EQUAL flottant
{
  t_init=$3;
}
| LEX_SIM_TYPE LEX_EQUAL LEX_SIM
{
  Simul->param->sim_type=$3;
}
| LEX_DELTA LEX_EQUAL flottant
{
  deltat=$3;
  
}
|LEX_SURF LEX_EQUAL LEX_ANSWER
{
  Simul->param->Surf=$3;
}
|LEX_FORMAT_TYPE LEX_EQUAL LEX_FORMAT
{
  Simul->pout->format=$3;
} 
;

create_mesh : LEX_NEWSAM LEX_EQUAL brace le_name tab_layer brace
{
  sprintf(Simul->name,"%s",$4);
}
|  LEX_NEWSAM LEX_EQUAL brace tab_layer brace
;


tab_layer : layers
{
  int i;
  double ratio_tmp;
  double precision;
  s_cmsh *pcmsh;
  double dimension;

  pcmsh=Simul->pcmsh;

  pcmsh->pcount->nlayer=nlayer;
  pcmsh->p_layer=MSH_tab_layer(player,nlayer,Simul->poutputs);

  ratio_tmp=pcmsh->l_max/pcmsh->l_min;
  pcmsh->dimension=(int)ratio_tmp;
  precision=fabs(ratio_tmp-pcmsh->dimension);
  if (precision>EPS){
    LP_error(Simul->poutputs,"Pb de Dimension calculee %f/%f = %f estime a %d\n",pcmsh->l_max,pcmsh->l_min,ratio_tmp,pcmsh->dimension);
  }

  for (i=nlayer-1;i>=0;i--)
    {
      pele=pcmsh->p_layer[i]->pele;
      MSH_calcul_sur_ele(pele);
    }

}
;

layers : layer layers
{
$$=MSH_chain_layer($1,$2);
}
|layer {$$=$1;}
;

layer : lire_layer carac_layer  brace
{
  
  // player->id=++nlayer;
  $$ = player;
  printf("layer %s, id %d read\n",player->name,player->id);
}
;

lire_layer : LEX_LAYER LEX_EQUAL brace le_name
{
  int i;
  player = MSH_create_layer($4,++nlayer);
  nele=0;
  if(Simul->param->Surf==YES && nlayer==2)
    nele_tot=1;
}
;
 
carac_layer : lecture_eles LEX_AVIEW
{
  int i;
  player->nele=nele;
  Simul->pcmsh->pcount->nele+=nele;
  //MSH_attribute_ele2layer(player,$1,nele);
  player->p_ele=MSH_tab_ele($1,nele,Simul->poutputs);
  nele=0;
  intern_id=Simul->pcmsh->pcount->nele;
}
;

le_name :LEX_NAME
{
  $$=$1;
  //sprintf(player->name,"%s",$1);
}
;


lecture_eles : lire_eles
{
  player->pele=$1;
  $$=$1;
}
;

lire_eles : lire_ele lire_eles
{
  $$=MSH_chain_ele($1,$2);
}
| lire_ele {
  $$=$1;}
; 


lire_ele : lire_id_ele lire_coordonne lire_coordonne lire_coordonne lire_coordonne lire_coordonne LEX_AVIEW
{

  MSH_ordinate_mesh(pele);
  pele->player=player;
  $$=pele;
}
;

lire_id_ele : LEX_INT LEX_AVIEW
{
  pele=MSH_create_element($1,ACTIVE);
  pele->id[INTERN_MSH] = nele++; // pour que l'id interne corresponde à la position de l'ele dans le tableau par layer : p_layer->pele[i]
  pele->id[ABS_MSH] = nele_tot++;
  pele->nele=1;
  coordo=0;
}
;

lire_coordonne :  flottant LEX_VIRGULE flottant
{
  if (coordo<4)
    {
      pele->pdescr->coord[coordo][0] = $1;
      pele->pdescr->coord[coordo++][1] = $3;
    }
}
;

create_faces : 
{
  MSH_check_redundancy(Simul->pcmsh,Simul->poutputs);
  MSH_referencing_mesh(Simul->pcmsh,Simul->poutputs);
  MSH_order_element_in_ref(Simul->pcmsh,Simul->poutputs);
  MSH_check_element_ordering(Simul->pcmsh,Simul->poutputs);
  MSH_create_ele_faces(Simul->pcmsh,Simul->poutputs);
  MSH_create_vertical_faces(Simul->pcmsh,Simul->poutputs);
  MSH_tab_all_neigh(Simul->pcmsh,Simul->poutputs);
  MSH_calcul_on_all_faces(Simul->pcmsh,Simul->poutputs);
}
;

specify_simul : intro_set setup_layers LEX_CLOSING_BRACE
;

intro_set : LEX_SET_UP LEX_EQUAL LEX_OPENING_BRACE
;
setup_layers : setup_layer setup_layers
|setup_layer
{
  int ele_layer=Simul->pcmsh->pcount->nlayer;
  if(verif_layer>ele_layer ||  verif_layer<ele_layer)
    LP_error(Simul->poutputs,"problem in layer definition look at command file \n");
  
}
;
//BL ATTENTION les param d=sont toujours definis avant les BC ou les sources
setup_layer : read_setup_layer set_param set_bound set_source LEX_CLOSING_BRACE
|read_setup_layer set_param set_source LEX_CLOSING_BRACE
|read_setup_layer set_param set_bound LEX_CLOSING_BRACE
|read_setup_layer set_param LEX_CLOSING_BRACE
;

read_setup_layer : LEX_SETUP_LAYER LEX_EQUAL LEX_OPENING_BRACE le_name
{
  int i;
  
  s_layer_msh *player;
  nlayer=MSH_get_layer_rank_by_name(Simul->pcmsh,$4,Simul->poutputs);
  verif_layer++;
   player=Simul->pcmsh->p_layer[nlayer];
  if(player!=NULL)
    {
      AQ_create_hydro_tab(player,Simul->poutputs);
      
    }
   else
    {
      LP_error(Simul->poutputs,"There is no layer %d in mesh",nlayer);
    }
}
;
//BL ATTENTION les param d=sont toujours definis avant les BC ou les sources
set_param : intro_param att_series type_mean LEX_CLOSING_BRACE 
|intro_param att_series type_mean type_cond LEX_CLOSING_BRACE
|intro_param att_series type_cond type_mean  LEX_CLOSING_BRACE

intro_param : LEX_PARAM LEX_EQUAL LEX_OPENING_BRACE;
att_series : att_serie att_series
| att_serie
;

att_serie : intro_descr setup_list  LEX_CLOSING_BRACE
{
  int ele_num=Simul->pcmsh->p_layer[nlayer]->nele;
      if(verif_ele>ele_num || verif_ele<ele_num)
	LP_error(Simul->poutputs,"Problem in %s parameters  check the input file\n",AQ_name_attributs(kindof_attribut));
     
    
}
;
 
intro_descr : LEX_MESH_ATT LEX_EQUAL LEX_OPENING_BRACE
{
  
  kindof_attribut = $1;
  verif_ele=0;
  LP_printf(Simul->poutputs,"Attributing %s\n",AQ_name_attributs(kindof_attribut));
}
;


//setup_list : params
//;

setup_list : values
|T_anisos //BL define_transm to define face transm as input to finish
;

values : value values
|value
;

value : LEX_INT flottant
{ 
  FILE *fp;
  s_descr_msh *pdescr;
  int j,k;
  fp=Simul->poutputs;
  verif_ele++;
  //Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro=AQ_create_ele_hydro($1);
  
  switch(kindof_attribut){
  case H_INI:{
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->H_ini=$2;
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->h[ITER]=$2;
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->h[T]=$2;
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->h[INTERPOL]=$2;
    break;
  }
  case THICK:{
      Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->Thick=$2;
    break;
  }
  case TRANSM_HOMO:{
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm=AQ_create_transm();
    if($2>0)
      {
	Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->transm[T_HOMO]=$2;//BL
	Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->type=HOMO;
      }
    else
      LP_error(fp,"The transmissivity value of ele %d is Null or Negative (T= %f) !!!\n",$1,$2);
    break;
  }
    

  case CONDUCT:{//BL 
    //la conducatance est traitée de la manière suivante : si thick n'est pas renseignée (thick ==0)
    //  la conductance entrée est considérée comme une conductance à travers une éponte elle est entrée en m2.s-1 (une valeur par element) à la face la conductance est recalculée pour gérée les différences de géométrie (division par surface element puis multiplication par la surface de la face) elle est en m2.s-1. 

    //si thick est renseignée (Thick!=0) et (pele->phydro->conductance !=0) et (pneigh_up->phydro->conductance !=0 ou pneigh_down->phydro->conductance !=0)

  //la conductance est alors définie comme une transmissivité verticale elle doit etre définie pour toutes les mailles du modèle. La conductance est alors calculée par définition de la transimissivité de passage (meme moyenne que pour la transmissivité) divisée par la longueur entre les centres puis multipliée par la surface de la face (elle sera fixée nulle aux faces de surface et de bases du modèle).

    //C=K_eponte/e_eponte Attention le Criv est une conductance les définitions précédentes s'appliquent donc elle pourra etre calculée en rentrant directement l'épaisseur de la zone hyporéhique ainsi que la transmissivité verticale!!!.
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->pcond=AQ_create_cond();
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->pcond->conductance=$2*Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->surf; //BL doit multiplier par surface car divise par surface dans calcul matrice. conductance = K_eponte/ep_eponte !!
    //LP_printf(Simul->poutputs," lay %d ele %d cond %e \n",nlayer,$1,$2);
    break;
  }

  case STORAGE:{//NF 2/9/2010
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->S[Ss]=$2;
    break;
  } 
  case ZBOT:{
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->zbot=$2;
    break;
  }  
  case ZTOP:{
    Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ztop=$2;
    break;
  } 
  }
  
}
;
// BL To define face transmissivity as an input to finish
T_anisos : T_aniso T_anisos
| T_aniso
;

T_aniso : LEX_INT LEX_DOUBLE LEX_DOUBLE
{
  verif_ele++;
  Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm=AQ_create_transm();
  Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->transm[T_X]=$2;//BL
  Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->transm[T_Y]=$3;
  Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->phydro->ptransm->type=ANI;
   
}
;

set_bound : intro_bound boundaries LEX_CLOSING_BRACE
{
  //BL Attention à vérifier si les source ne sont pas mélangées avec les BC !! + voir ou stocker ce tableau !!
     
 

}
;

set_source : intro_source boundaries LEX_CLOSING_BRACE
{
  //BL Attention à vérifier si les source ne sont pas mélangées avec les BC !! + voir ou stocker ce tableau !!

}
;

intro_bound : LEX_BOUND LEX_EQUAL LEX_OPENING_BRACE
{
  int i;
  LP_printf(Simul->poutputs,"Attibuting Boundary condition of layer : %d\n",nlayer);

 
  //BL j'efface tout à chaque bound >> très con on a juste besoin d'un ft et de la boundarie traitée!!

}
;

intro_source : LEX_SOURCE LEX_EQUAL LEX_OPENING_BRACE
{
  int i;
  LP_printf(Simul->poutputs,"Attributing inputs of layer %d :\n ",nlayer);
 
}
;

boundaries : a_boundary_type boundaries
| a_boundary_type
;

a_boundary_type : typeof_boundary boundary_series LEX_CLOSING_BRACE
{
 
  kindof_SOURCE=0;
  kindof_BC=0;
}
;

typeof_boundary : LEX_TYPE_BC LEX_EQUAL LEX_OPENING_BRACE
{
  kindof = $1;
  LP_printf(Simul->poutputs,"type : %s\n",AQ_name_BC(kindof));
  kindof_SOURCE=CODE_AQ;
  idir =CODE_AQ;//Needs to be initialized
  
}
| LEX_TYPE_SOURCE LEX_EQUAL LEX_OPENING_BRACE
{
 kindof_BC=CODE_AQ;
 kindof=$1;
 LP_printf(Simul->poutputs,"type : %s\n",AQ_name_source(kindof));
 
}
;

boundary_series : a_boundary boundary_series 
| a_boundary 
{
  FILE *fp;
  fp=Simul->poutputs;
      //TS_print_ts(id_list,Simul->poutputs);
  
      idir = CODE_AQ;
}
;

a_boundary : intro_dir defs_bound
| param_riv defs_bound
| defs_bound
;

param_riv : LEX_PARAM_RIV LEX_EQUAL LEX_OPENING_BRACE param_cond param_TZ LEX_CLOSING_BRACE
| LEX_PARAM_RIV LEX_EQUAL LEX_OPENING_BRACE param_cond LEX_CLOSING_BRACE
;

param_cond : LEX_DOUBLE
{
  surf_riv = $1;
}
;
param_TZ : LEX_DOUBLE LEX_DOUBLE
{
  ep_riv_bed = $1;
  TZ_riv_bed = $2;
}
;

intro_dir : LEX_DIR LEX_EQUAL
{
FILE *fp;
  fp=Simul->poutputs;
  idir = $1; 
};

defs_bound : LEX_OPENING_BRACE ele_ids LEX_CLOSING_BRACE time_serie
{
  //s_ft *ptmp;
  FILE *fp;
  fp=Simul->poutputs;
  
  id_list = IO_browse_id($2,BEGINNING_TS);

  if(kindof_SOURCE!=CODE_AQ)
    {

      /*------------------- WARNING : --------------------*/
      /* il faudra multiplier par la surface de l'élément*/
      /*pour avoir les bon flux !!!                      */
      LP_printf(Simul->poutputs,"\t-->stot = %f \n",stot);
      AQ_define_source(Simul->pcmsh,id_list,pft,kindof,stot,Simul->poutputs);
      //BL PLUS BESOIN DU NLAYER SI ID_LIST EST INTERN_ID
      
     
    }

  else
    {
      
      /*------------------- WARNING : --------------------*/
      /* il faudra multiplier par la longueur de l'élément*/
      /*pour avoir les bon flux !!!                       */
      LP_printf(Simul->poutputs,"\t-->ltot = %f \n",ltot);
      AQ_define_bound(Simul->pcmsh,id_list,pft,idir,kindof,ltot,surf_riv,ep_riv_bed,TZ_riv_bed,Simul->poutputs); 
      //BL PLUS BESOIN DU NLAYER IL EST DANS ID_LIST!!
      
      Simul->pcmsh->pcount->pcount_hydro->nelebound[kindof] += nelebound;
      
    }
  nelebound=0;
  ltot=0;
  stot=0;
}
;

ele_ids : ele_id ele_ids
{
  //init $$=TS_secured_chain_fwd_ts($2,$1);
$$=IO_chain_bck_id($1,$2);
}
| ele_id
{
  $$=$1;
}
;
ele_id : LEX_INT
{
  
  $$=IO_create_id(nlayer,$1);
  nelebound ++;
  ltot+=Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->l_side;
  stot+=Simul->pcmsh->p_layer[nlayer]->p_ele[$1]->pdescr->surf;
}
;
time_serie : intro_time series LEX_CLOSING_BRACE
{
pft=TS_browse_ft($2,BEGINNING_TS);
}
;
intro_time : LEX_TIME_S LEX_EQUAL LEX_OPENING_BRACE;

series : serie series
{
  //init $$=TS_secured_chain_fwd_ts($2,$1);
$$=TS_chain_bck_ts($1,$2);
}
| serie
{
  $$=$1;
}
;

serie : flottant flottant
{

$$=TS_create_function((double)$1,(double)$2);

}
;

type_mean : LEX_MEAN LEX_EQUAL LEX_TYPE_MEAN
{
  int type_mean;
  s_layer_msh *player;
  
  player=Simul->pcmsh->p_layer[nlayer];
  player->mean_type = $3;
  
}
;
type_cond : LEX_COND LEX_EQUAL LEX_TYPE_COND
{
 
  s_layer_msh *player;
  
  player=Simul->pcmsh->p_layer[nlayer];
  player->cond_type = $3;
  
}
;

brace :  LEX_OPENING_BRACE
|  LEX_CLOSING_BRACE
;

flottant : LEX_DOUBLE
{
  $$ = $1;
}
| LEX_INT
{
  $$ = (double)$1;
}
;

%%

/* Procedure used to display errors
 * automatically called in the case of synthax errors
 * Specifies the file name and the line where the error was found
 */
#if defined GCC481 || defined GCC473 || defined GCC472 ||  defined GCC471
void yyerror(char const *s)
#else 
void yyerror(char *s)
#endif
{
  if (pile >= 0) {
    //fprintf(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
    //exit -1;
    LP_error(Simul->poutputs,"File %s, line %d : %s\n",current_read_files[pile].name,line_nb+1,s);
  }
}

void lecture(char *name,FILE *fp) 
{


  pile = 0;
  current_read_files[pile].name = (char *)strdup(name);
  if ((yyin = fopen(name,"r")) == NULL)
    LP_error(Simul->poutputs,"File %s doesn't exist\n",name);
  current_read_files[pile].address = yyin;
  current_read_files[pile].line_nb = line_nb;

  LP_printf(fp,"\n****Reading input data****\n");
  yyparse();
  LP_printf(fp,"\n****Input data read****\n");
 
  
}
