/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: print_mat.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdarg.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
#include "MSH.h"
#include "AQ.h"

void AQ_print_LHS_RHS(s_cmsh *pcmsh, s_gc *pgc, FILE *fpout) {
  double *LHS, *RHS;
  double **LHSprint;
  int *id_diag;
  s_layer_msh *play;
  int nele, id_ele, neletot;
  int i, iref, icol, pos_diag, j, k;
  double aij;
  int nlayer;
  s_ele_msh *pele;
  char *name;

  neletot = pcmsh->pcount->nele;
  nlayer = pcmsh->pcount->nlayer;
  LHSprint = (double **)calloc(neletot, sizeof(double *));
  for (i = 0; i < neletot; i++)
    LHSprint[i] = (double *)calloc(neletot, sizeof(double));

  id_diag = pgc->mat4;
  LHS = pgc->mat6;
  RHS = pgc->b;

  for (k = 0; k < nlayer; k++) {
    play = pcmsh->p_layer[k];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      id_ele = MSH_get_ele_id(play->p_ele[j], ABS_MSH); // Starts at 1 for coupling with libgc
      pos_diag = id_diag[id_ele] - 1;                   // numering of mat4 and mat5 starts at 0
      iref = id_diag[id_ele - 1] - 1;
      if (pgc->mat5[pos_diag] != MSH_get_ele_id(play->p_ele[j], ABS_MSH))
        LP_error(fpout, "libaq%4.2f %s in %s l%d : The LHS is not defined properly. The diagonal number is not the last one of mat5.\nIn the LHS line %d, the last defined coefficient is the %d one, which is not the diagonal one %d\n The matriciel library is not built properly with pgc. Check the way mat5 is filled\n", VERSION_AQ, __func__, __FILE__, __LINE__, id_ele, pgc->mat5[pos_diag], id_ele);
      for (i = iref + 1; i <= pos_diag; i++) { // NF 7/10/2017 Attention, il faut ordonner les elements par numeros de column....
        icol = pgc->mat5[i];
        aij = LHS[i];
        LHSprint[id_ele - 1][icol - 1] = aij;
      }
    }
  }

  // NF 7/10/2017 Impression des indices de colonne
  fprintf(fpout, "LHS \t");
  for (i = 0; i < neletot; i++)
    fprintf(fpout, " %d", i + 1); // NF 7/10/2017 Impression de l'indice de ligne (raw), ie indice ABS_MSH si tout est bien ordonne -> pas sur
  fprintf(fpout, "\n");

  for (i = 0; i < neletot; i++) {
    fprintf(fpout, "%d", i + 1); // NF 7/10/2017 Impression de l'indice de ligne (raw), ie indice ABS_MSH si tout est bien ordonne -> pas sur
    for (j = 0; j < neletot; j++) {
      if (fabs(LHSprint[i][j]) > EPS_TS)
        fprintf(fpout, " %.6e", LHSprint[i][j]);
      else
        fprintf(fpout, " x");
    }
    fprintf(fpout, "\n");
  }

  fprintf(fpout, "RHS \n");
  for (i = 0; i < neletot; i++) {
    pele = MSH_get_ele_from_abs(pcmsh, i + 1, fpout); // NF 11/10/2017 id ABS_MSH starts at 1 and not 0
    if (pele->phydro->pbound != NULL) {
      if (pele->phydro->pbound->type == CAUCHY)
        fprintf(fpout, "%d\t%.6e\t%s\n", i + 1, RHS[i], AQ_name_modif(pele->modif[MODIF_CALC_AQ])); // NF 7/10/2017 WARNING Impression de l'indice de ligne (raw), ie indice ABS_MSH si tout est bien ordonne -> pas sur
      else
        fprintf(fpout, "%d\t%.6e\t%s\n", i + 1, RHS[i], AQ_name_BC(pele->phydro->pbound->type));
    } else
      fprintf(fpout, "%d\t%.6e\t%s\n", i + 1, RHS[i], MSH_name_cell(pele->type, fpout));
  }
  for (i = 0; i < neletot; i++)
    free(LHSprint[i]);
  free(LHSprint);
}

/**\fn void AQ_print_mat_int(s_gc *pgc,FILE *fpout)
 *\brief function to print the integer matrix of pgc
 *\return void
 *
 */
void AQ_print_mat_int(s_gc *pgc, FILE *fpout) {
  int i, j, k;
  LP_printf(fpout, "mat4 : ");
  for (i = 0; i < pgc->ndl + 1; i++) {
    LP_printf(fpout, "%d ", pgc->mat4[i]);
    if (i % 10 == 0 && i > 0)
      LP_printf(fpout, "\n");
  }
  k = 1;
  LP_printf(fpout, "\n mat5 : ");
  for (j = 0; j < pgc->lmat5; j++) {
    LP_printf(fpout, "%d ", pgc->mat5[j]);
    if (j == (pgc->mat4[k] - 1)) {
      LP_printf(fpout, "| ");
      k++;
    }
    if (j % 10 == 0 && j > 0)
      LP_printf(fpout, "\n");
  }
}

/**\fn void AQ_print_RHS(s_gc *pgc,FILE *fpout)
 *\brief function to print the Right Hand Side vector
 *\return void
 *
 */
void AQ_print_RHS(s_gc *pgc, FILE *fpout) {
  int i;
  LP_printf(fpout, "RHS : \n");
  LP_printf(fpout, "ele b_i\n");
  for (i = 0; i < pgc->ndl; i++) {
    LP_printf(fpout, "%d %e\n ", i + 1, pgc->b[i]);
  }
}

/**\fn void AQ_print_LHS(s_gc *pgc,FILE *fpout)
 *\brief function to print the Left Hand Side matrix
 *\return void
 *
 */
void AQ_print_LHS(s_gc *pgc, FILE *fpout) {
  int j, k;
  LP_printf(fpout, "LHS : \n");
  k = 1;
  for (j = 0; j < pgc->lmat5; j++) {
    LP_printf(fpout, "%e ", pgc->mat6[j]);
    if (j == (pgc->mat4[k] - 1)) {
      LP_printf(fpout, "| ");
      k++;
    }
    if (j % 10 == 0 && j > 0)
      LP_printf(fpout, "\n");
  }
}
