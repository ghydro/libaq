/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: ext_AQ.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* Variables used when reading input data */
extern FILE *yyin;
extern int yydebug;
/* Structure containing the simulation caracteristics */
extern s_simul_aq *Simul;
/* Table of the addresess of the read files */
extern s_file current_read_files[NPILE];
/* Number of folders defined by the ugloser */
extern int folder_nb;
/* File number */
extern int pile;
/* Position within the file being processed */
extern int line_nb;
/* Name of file folders */
extern char *name_out_folder[NPILE];

/* Functions of input.y */
void lecture(char *, FILE *);
int yylex();
#if defined GCC481 || defined GCC473 || defined GCC472 || defined GCC471
void yyerror(char const *);
#else
void yyerror(char *);
#endif /*test on GCC*/
