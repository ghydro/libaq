/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: param_AQ.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#define CODE_AQ CODE_TS
#define STRING_LENGTH STRING_LENGTH_LP
#define VERSION_AQ 0.69
// #define Z Z_MSH
#define Y Y_MSH
#define X X_MSH
#define YES_AQ YES_IO
#define NO_AQ NO_IO
#define CURRENT NEXTREMA_TS
#define CUR CUR_AQ
#define EPS_MB_AQ 0.1
#define EPS_T_AQ 1E-20            // NG : 31/05/2021 : New criteria to check potential very low values of transmissivity.
#define ALPHA_Z_DEFAULT_AQ 0.1    // Default value of vertical anisotropy factor
#define SY_DEFAULT_FACTOR_AQ 0.01 // NG 11/08/2021 : Default multiplication factor value of specific yield when switching from UNCONFINED to CONFINED configuration.
#define AQ_DEFAULT_THICK 10
#define AQ_DEFAULT_WET_THICKNESS 0.1
#define EPS_AQ EPS_TS

// NF 23/5/2017 Il y a un pb de definition des CL dans cawaqs. Il faut arriver a dechiffrer la signification de cette liste
enum qlim_type { NO_LIM_AQ, QLIM_AQ, DRY_AQ, NLIM_RIV_AQ };

enum kindof_mesh_att { H_INI, THICK, TRANSM_HOMO, TRANSM_ANI, CONDUCT, STORAGE, SPECIFIC_YIELD, ZBOT, ZTOP, NMESH_ATT };

enum mean_type { HARMONIC, ARITHMETIC, GEOMETRIC, NMEAN_AQ };

enum sim_type_aq { STEADY_AQ, TRANSIENT_AQ, IMPOSED_AQ, NSIM_TYPE_AQ };

enum mat { A, B, NAB };

enum output_type { PIEZ, MASS, FLUX, NOUT };

enum eletype { ELE_DIRICHLET_AQ, ELE_NEUMANN_AQ, ELE_CAUCHY_AQ, ELE_RECHARGE_AQ, ELE_UPTAKE_AQ, ELE_ACTIVE_AQ, NELE_TYPE };

enum bc_mb { DIRICHLET_MB, NEUMANN_MB, OVERFLOW_MB, RIV_MB, N_BC_MB };

/* General parameters of the simulation
 * THETA = degree of impliciteness of the equations' discretization
 * EPS_Q = convergence precision of Q
 * EPS_Z = convergence precision of Z
 */
enum general_param_aq { THETA_AQ, EPS_Q_AQ, EPS_Z_AQ, NPAR_AQ };
// enum deltat {CALC,OUTPUT,NDELTAT};
#define NB_H_VAL_AQ 2
#define NB_MB_VAL_AQ NMASS + N_BC_MB + 1
#define NB_FLUX_VAL_AQ NMASS *ND_MSH *NFACE_MSH

/* FB 05/04/2018 Each component of out_mb_aq corresponds to a column in AQ_MB file. H_END [m] is the piezometric head at the end of the time step, DV_DT [m3/s] is the variation of the water
 volume in the cell per unit time, FLUX_DD_NN [m3/s] is the discharge at the face direction DD type NN, REC [m3/s] is the aquifer recharge, UPT [m3/s] is the uptake from an aquifer cell,
 FLUX_DIRICHLET [m3/s] is the discharge associated to the Dirichlet boundary condition, FLUX_NEUMANN [m3/s] is the discharge associated to the Neumann boundary condition,SOL [m3/s] is the
 discharge at the soil (overflow), ENR [m3/s] is stream-aquifer discharge, ERR [m3/s] is the mass balance error, ERR_REL [-] is the mass balance relative error. See user guide for more explanation. */
enum out_aq_mb { H_END, DV_DT, FLUX_X_ONE, FLUX_X_TWO, FLUX_Y_ONE, FLUX_Y_TWO, FLUX_Z_ONE, FLUX_Z_TWO, REC, UPT, FLUX_DIRICHLET, FLUX_NEUMANN, SOL, ENR, ERR, ERR_REL, NB_OUT_AQ };

enum out_aq_thresh { H_END_AQ, H_THR_AQ, UPT_AQ, UPT_THR_AQ, DRYWET_AQ, NB_OUT_THRESH_AQ };

// To debug, add H_IN and conductance at all faces
// enum out_aq_mb {H_IN,H_END,DV_DT,FLUX_X_ONE,FLUX_X_TWO,FLUX_Y_ONE,FLUX_Y_TWO,FLUX_Z_ONE,FLUX_Z_TWO,COND_X_ONE,COND_X_TWO,COND_Y_ONE,COND_Y_TWO,COND_Z_ONE,COND_Z_TWO,REC,UPT,FLUX_DIRICHLET,FLUX_NEUMANN,SOL,ENR,ERR,ERR_REL,NB_OUT_AQ};

#define NOUT_AQ NSAT_IO
#define NB_VAL_OUTPUT_HYDHEAD_AQ 1
#define YIELD_CONFINED_AQ 0.001
#define YIELD_UNCONFINED_AQ 0.01
#define PICNITMIN_AQ 3 // NF 23/9/2017 Nb d'iterations minimales de picart si BC CAUCHY
#define PICNITMAX_AQ 10
#define STEADYNITMAX_AQ 1000 // Max number of steady state iterations (by default; can be modified in the COMM file)
#define DEFAULT_T_AQ 0
#define DEFAULT_DT_AQ 86400        // Simple leurre utilise pour le regime permanent avec iteration de picart
#define NB_MB_FLUX_AQ_TRANSPORT 26 // NG 09/10/2021 : Number of MB values printed out in output files in case of transport in aquifer
#define COR_POS_AQ 1
#define COR_NEG_AQ -1
#define NOCONDUCT_AQ -7777 // NF 14/9/2022 indicates that the conductance is not calculate on the face but on the subfaces. We could implement a calculation of an average conductance but would be unused by CaWaQS so not done
#define NOCONDUCT_AQ -7777 // NF 14/9/2022 indicates that the conductance is not calculate on the face but on the subfaces. We could implement a calculation of an average conductance but would be unused by CaWaQS so not done
