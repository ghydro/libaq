/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_mat.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdarg.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"
#ifdef OMP
#include "omp.h"
#endif
// #include "GC.h"

/**\fn int AQ_calc_lmat5(s_cmsh *pcmsh)
 // AR: lire notice libgc pour comprendre mat6 mat4 mat5
*\brief calculate the length of mat5 for pgc
 *\return lmat5
 *
 */
int AQ_calc_lmat5(s_cmsh *pcmsh) {
  int lmat5, nlayer, nele;
  s_ele_msh *pele;
  s_layer_msh *play;
  int i, j, idir, itype;
  int nele_tot;
  s_id_io *pneigh;
  lmat5 = 0;
  nele_tot = 0;
  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;
    nele_tot += nele;
    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];

      for (idir = 0; idir < ND_MSH; idir++) {

        for (itype = 0; itype < NFACE_MSH; itype++) {
          pneigh = pele->p_neigh[idir][itype];
          if (pneigh != NULL) {
            pneigh = IO_browse_id(pneigh, BEGINNING_TS);
            lmat5 += IO_length_id(pneigh);
          }
        }
      }
    }
  }
  lmat5 += nele_tot;

  return lmat5;
}

/**\fn void void AQ_init_mat_int(s_cmsh *pcmsh,s_gc *pgc,FILE *fp)
 *\brief function to initialize and fill the integer matrix and vector needed for pgc
 *\return void
 *
 */
void AQ_init_mat_int(s_cmsh *pcmsh, s_gc *pgc, FILE *fp) {
  int nlayer, nele, neletot;
  int i, j, loc_mat5;

  s_ele_msh *pele;
  s_layer_msh *play;
  int *mat4, *mat5;
  // s_gc *pgc;
  s_id_io *mat_tmp;
  nlayer = pcmsh->pcount->nlayer;
  pgc->ndl = pcmsh->pcount->nele;
  pgc->lmat5 = AQ_calc_lmat5(pcmsh);
  pgc->mat5 = GC_create_mat_int(pgc->lmat5);
  pgc->mat4 = GC_create_mat_int(pgc->ndl + 1);
  loc_mat5 = 0;
  mat4 = pgc->mat4;
  mat5 = pgc->mat5;
  neletot = 0;
  for (i = 0; i < nlayer; i++) {
    play = pcmsh->p_layer[i];
    nele = play->nele;

    for (j = 0; j < nele; j++) {
      pele = play->p_ele[j];
      mat_tmp = AQ_mat_neigh(pele);
      // on fait la boucle sur mat_tmp
      while (mat_tmp->next != NULL) {
        AQ_fill_mat5_neigh(pcmsh, pgc, mat_tmp, loc_mat5, fp);
        loc_mat5++;
        mat_tmp = mat_tmp->next;
      }
      // on fait le dernier id car mat_tmp ne doit pas etre null pour le free!!!
      AQ_fill_mat5_neigh(pcmsh, pgc, mat_tmp, loc_mat5, fp);
      loc_mat5++;

      mat_tmp = IO_free_id_serie(mat_tmp, fp);

      if (loc_mat5 < pgc->lmat5) {
        mat5[loc_mat5] = MSH_get_ele_id(pele, ABS_MSH);
        loc_mat5++;
      } else {
        LP_printf(fp, "loc_mat5 = %d dim mat5 = %d\n", loc_mat5, pgc->lmat5);
        LP_error(fp, "depassement de tableau dans mat5\n");
      }

      if (neletot < pgc->ndl) {
        mat4[++neletot] = loc_mat5;
      } else
        LP_error(fp, "depassement de tableau dans mat4\n");
    }
  }
}

void AQ_fill_mat5_neigh(s_cmsh *pcmsh, s_gc *pgc, s_id_io *mat_tmp, int loc_mat5, FILE *fp) {
  s_ele_msh *pneigh;
  int id_neigh;

  pneigh = MSH_get_ele(pcmsh, mat_tmp->id_lay, mat_tmp->id, fp);
  id_neigh = MSH_get_ele_id(pneigh, ABS_MSH);
  if (loc_mat5 < pgc->lmat5) {
    pgc->mat5[loc_mat5] = id_neigh;
    // LP_printf(fp,"mat5[%d]=%d\n",loc_mat5,id_neigh); // BL to debug
  } else {
    //    LP_printf(fp,"loc_mat5 = %d dim mat5 = %d\n",loc_mat5,pgc->lmat5); // BL to debug
    LP_error(fp, "depassement de tableau dans mat5\n");
  }
}

/**\fn s_id_io* AQ_mat_neigh(s_ele_msh *pele)
 *\brief function to put the neighboor ABS_ID in a pointor string the last pointor of the string is the ABS_ID of pele
 *\return *s_id_io
 *
 */
s_id_io *AQ_mat_neigh(s_ele_msh *pele) {
  int idir, itype;

  s_id_io *ptmp, *ptmp2;
  s_id_io *mat_tmp;

  mat_tmp = NULL;
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      ptmp = pele->p_neigh[idir][itype];

      if (ptmp != NULL) {
        ptmp2 = IO_copy_id_serie(ptmp, BEGINNING_TS);
        if (mat_tmp != NULL) {
          mat_tmp = IO_browse_id(mat_tmp, END_TS);
          mat_tmp = IO_secured_chain_fwd_id(mat_tmp, ptmp2);
          // mat_tmp=MSH_secured_chain_fwd_id(mat_tmp,ptmp);
        } else {
          mat_tmp = ptmp2;
          // mat_tmp=IO_browse_id(ptmp2,BEGINNING_TS);
        }
      }
    }
  }

  if (mat_tmp != NULL)
    mat_tmp = IO_browse_id(mat_tmp, BEGINNING_TS);

  return mat_tmp;
}
/**\fn void AQ_reinit_b_source(s_cmsh *pcmsh,FILE *fpout)
 *\brief function to reinitialize the b_source double set in pele->phydro
 *\return void
 *
 */
void AQ_reinit_b_source(s_carac_aq *pcarac_aq, FILE *fpout) {
  s_cmsh *pcmsh;
  s_ele_msh *pele;
  s_id_io **elesource;
  s_id_io ***p_eletype;
  s_id_io *source;
  int nsource, e, neletype;
  int *n_eletype;
#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_aq->settings->psmp;
#endif
  pcmsh = pcarac_aq->pcmsh;
  p_eletype = pcmsh->pcount->pcount_hydro->p_eletype;
  n_eletype = pcmsh->pcount->pcount_hydro->nele_type;
  for (nsource = 0; nsource < NSOURCE; nsource++) {
    elesource = p_eletype[nsource + ELE_RECHARGE_AQ];
    neletype = n_eletype[nsource + ELE_RECHARGE_AQ];
#ifdef OMP

    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);

    psmp->chunk = PC_set_chunk_size_silent(fpout, n_eletype[nsource + ELE_RECHARGE_AQ], nthreads);
    taille = psmp->chunk;

#pragma omp parallel shared(elesource, pcmsh, nthreads, taille) private(e, source, pele)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (e = 0; e < neletype; e++) {
        source = elesource[e];
        pele = MSH_get_ele(pcmsh, source->id_lay, source->id, fpout);
        pele->phydro->b_source = 0;
      }

#ifdef OMP
    } // end of parallel section
#endif
  }
}

/**\fn void AQ_calc_vector_b(s_carac_aq *pcarac_aq,s_chronos_CHR *chronos,double t,FILE *fpout)
 *\brief function to calculate the RHS vector
 *\return void
 *
 */
void AQ_calc_vector_b(s_carac_aq *pcarac_aq, s_chronos_CHR *chronos, double t, FILE *fpout) {
  s_cmsh *pcmsh;
  s_param_aq *param;
  s_gc *pgc;
  s_ele_msh *pele;
  s_layer_msh *play;
  s_id_io *active, *source, *bound;
  s_id_io **elebound, **elesource, **eleactive;
  s_id_io ***p_eletype;
  double deltat, theta, b_i;
  double *B;
  int nsource, nbound;
  int a0, neletype, e;
  int *n_eletype;
#ifdef OMP
  int taille;
  int nthreads;
  s_smp *psmp;
  psmp = pcarac_aq->settings->psmp;
#endif
  pcmsh = pcarac_aq->pcmsh;
  param = pcarac_aq->settings;
  pgc = pcarac_aq->calc_aq;

  theta = param->general_param[THETA_AQ];
  deltat = chronos->dt;
  a0 = param->a0;
  // pgc->b=GC_create_mat_double(pgc->ndl);
  // B=pgc->b;
  // LP_printf(fpout," deltat : %f \n",deltat);

  n_eletype = pcmsh->pcount->pcount_hydro->nele_type;
  p_eletype = pcmsh->pcount->pcount_hydro->p_eletype;

  AQ_reinit_b_source(pcarac_aq, fpout);

  /*calcul au prealable du terme b_source pour l'ensemble des ele sources bound et active*/
  for (nsource = 0; nsource < NSOURCE; nsource++) {
    neletype = n_eletype[nsource + ELE_RECHARGE_AQ];
    elesource = p_eletype[nsource + ELE_RECHARGE_AQ];

#ifdef OMP
    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);

    psmp->chunk = PC_set_chunk_size_silent(fpout, n_eletype[nsource + ELE_RECHARGE_AQ], nthreads);
    taille = psmp->chunk;

#pragma omp parallel shared(elesource, pcmsh, deltat, nsource, theta, nthreads, taille) private(e, source, pele)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (e = 0; e < neletype; e++) {
        source = elesource[e];
        pele = MSH_get_ele(pcmsh, source->id_lay, source->id, fpout);
        AQ_calc_b_i_source(pele, theta, t, deltat, nsource, fpout);
      }

#ifdef OMP

    } // end of parallel section
#endif
  }

  neletype = n_eletype[ELE_ACTIVE_AQ];
  eleactive = p_eletype[ELE_ACTIVE_AQ];
#ifdef OMP
  nthreads = psmp->nthreads;
  omp_set_num_threads(psmp->nthreads);
  psmp->chunk = PC_set_chunk_size_silent(fpout, n_eletype[ELE_ACTIVE_AQ], nthreads);
  taille = psmp->chunk;
#pragma omp parallel shared(eleactive, pcmsh, a0, deltat, theta, nthreads, taille) private(e, active, pele, b_i)
  {
#pragma omp for schedule(dynamic, taille)
#endif
    for (e = 0; e < neletype; e++) {
      b_i = 0;
      active = eleactive[e];
      pele = MSH_get_ele(pcmsh, active->id_lay, active->id, fpout);
      b_i += pele->phydro->b_source;
      b_i += AQ_calc_b_active(pele, pcmsh, a0, deltat, theta, t, fpout);
      AQ_def_RHS(b_i, MSH_get_ele_id(pele, ABS_MSH), pgc, fpout);
    }

#ifdef OMP

  } // end of parallel section
  // LP_printf(fpout,"done\n"); //BL to debug
#endif

  for (nbound = 0; nbound < NBOUND; nbound++) {
    neletype = n_eletype[nbound];
    elebound = p_eletype[nbound];
#ifdef OMP
    nthreads = psmp->nthreads;
    omp_set_num_threads(psmp->nthreads);
    psmp->chunk = PC_set_chunk_size_silent(fpout, n_eletype[nbound], nthreads);
    taille = psmp->chunk;
#pragma omp parallel shared(elebound, pcmsh, a0, deltat, theta, nbound, nthreads, taille) private(e, bound, pele, b_i)
    {
#pragma omp for schedule(dynamic, taille)
#endif
      for (e = 0; e < neletype; e++) {
        bound = elebound[e];
        b_i = 0;
        pele = MSH_get_ele(pcmsh, bound->id_lay, bound->id, fpout);
        if (nbound != DIRICHLET)
          b_i += pele->phydro->b_source;
        b_i += AQ_calc_b_bound(pele, pcmsh, a0, deltat, theta, t, nbound, fpout);
        AQ_def_RHS(b_i, MSH_get_ele_id(pele, ABS_MSH), pgc, fpout);
      }
#ifdef OMP

    } // end of parallel section
#endif
  }
}

double AQ_calc_sizeweight_at_face(int idir, s_ele_msh *pele, s_ele_msh *pneigh, int nneigh, FILE *fpout) {
  double zweight = 1.;
  double v1, v2;

  if (idir != Z_MSH) {           // NF 18/12/2017 voir equation discretisee. Il faut calculer surf maille divisee par (largeur maille*distance entre centres des elements) pour ensuite ponderer les termes ecrits pour un maillage regulier.
    v1 = pele->pdescr->l_side;   // NF 18/12/2017 en supposant que itype est bien l'appelant;
    v2 = pneigh->pdescr->l_side; // NF 18/12/2017 en supposant que itype est bien l'appelant;
    zweight = v1;
    zweight /= 0.5 * (v1 + v2);
    // zweight/=2.;

    if ((pele->pdescr->l_side - pneigh->pdescr->l_side) > EPS_TS)
      zweight /= 2.; // NF 4/1/2018 In case of one or two smaller neighbours, the effective section is half the size of the face_width. AS the term of element i is weighted by its surface, it is then required to divide l2 by a factor 2.
  }
  // NF 5/1/2018 This is removed because the face that is selected is already the smaller one (function MSH_return_face) and therefore has the adequate parameter values.
  // else   //NF 18/12/2017 Le else est le Cas d'une drainance. Il faut ponderer la conductance de la face de l'element dans lequel on se trouve par son nombre de voisins
  //  zweight/=(double)nneigh;
  return zweight;
}

/**\fn double double AQ_calc_b_oneface(s_ele_msh *pele,s_cmsh *pcmsh,int a0,double deltat,double theta,double t,FILE *fpout)
 *\brief function that calculates the contribution of one face to the RHS vector
 *\return double
 *
 */
double AQ_calc_b_oneface(s_ele_msh *pele, s_cmsh *pcmsh, double deltat, double theta, double t, int idir, int itype, FILE *fpout) {

  int i;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface;
  s_bound_aq *pbound;
  int nneigh;

  double b_i, l2;
  i = 0;
  nneigh = 0;
  b_i = 0.;

  neigh = pele->p_neigh[idir][itype];
  if (neigh != NULL) {
    neigh = IO_browse_id(neigh, BEGINNING_TS);
    nneigh = IO_length_id(neigh);

    while (neigh != NULL) {
      i++;
      pneigh = MSH_get_ele(pcmsh, neigh->id_lay, neigh->id, fpout);
      pface = MSH_return_face(pele, pneigh, idir, itype, fpout);
      l2 = AQ_calc_sizeweight_at_face(idir, pele, pneigh, nneigh, fpout); // NF18/12/2017
      if (pface->phydro->pbound == NULL) {
        b_i += AQ_calc_b_i_active(pface, pneigh, deltat, theta, l2, idir, itype, fpout);
        b_i -= AQ_calc_b_i_active(pface, pele, deltat, theta, l2, idir, itype, fpout);
      } else {
        LP_warning(fpout, "libaq%4.2f %s in %s l%d : \n\t ACTIVE Element %d has a boundary that may be a cauchy for libwet through a face. It is however taken into account in RHS but not LHS. This comment can be removed from the source code if everything works properly\n", VERSION_AQ, __func__, __FILE__, __LINE__, pele->id[ABS_MSH]);
        b_i += AQ_calc_b_i_cauchy(pele, deltat, theta, t, idir, itype, fpout);
      }
      neigh = neigh->next;
    } // end while(neigh!=NULL)
  }   // end if(neigh!=NULL)
  else {
    pface = pele->face[idir][itype];
    pbound = pface->phydro->pbound;
    if ((pbound != NULL) && (pbound->type == NEUMANN)) { // NF 6/12/2017 Cas ou on a un flux NEUMANN a la frontiere qui aurait ete ecrase par une condition de cauchy
      b_i += AQ_calc_b_i_neumann(pface, deltat, theta, t, itype, fpout);
    }
  } // end else(neigh!=NULL)

  return b_i;
}

/**\fn double double AQ_calc_b_active(s_ele_msh *pele,s_cmsh *pcmsh,int a0,double deltat,double theta,double t,FILE *fpout)
 *\brief functionto calculate the active cells terms of the RHS vector
 *\return double
 *
 */
double AQ_calc_b_active(s_ele_msh *pele, s_cmsh *pcmsh, int a0, double deltat, double theta, double t, FILE *fpout) {
  int idir, itype, i;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface;
  s_bound_aq *pbound;
  int nneigh;

  double b_i, l2, dx;
  i = 0;

  b_i = a0 * pele->phydro->S[SCUR] * pele->phydro->h[T] * pele->pdescr->surf; // BL modif init

  // LP_printf(fpout,"calculating b for element %d\n",MSH_get_ele_id(pele,ABS_MSH)); // BL to debug
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      b_i += AQ_calc_b_oneface(pele, pcmsh, deltat, theta, t, idir, itype, fpout);
    }
  }

  return b_i;
}

/**\fn double AQ_calc_b_bound(s_ele_msh *pele,s_cmsh *pcmsh,int a0,double deltat,double theta,double t,int bound_type,FILE *fpout)
 *\brief function to calculate the boundaries terms of the RHS vector
 *\return double
 *
 */
double AQ_calc_b_bound(s_ele_msh *pele, s_cmsh *pcmsh, int a0, double deltat, double theta, double t, int bound_type, FILE *fpout) {
  int idir, itype, i, j, nneigh, m;
  int verif = 0;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface, *psubf;
  int nbound = 0;
  double b_i, l2, l_ele, dx;
  nneigh = 0;
  i = 0;

  if (bound_type == NEUMANN) {
    // LP_printf(fpout,"calculating b for element %d\n",MSH_get_ele_id(pele,ABS_MSH)); // BL to debug

    b_i = a0 * pele->phydro->S[SCUR] * pele->phydro->h[T] * pele->pdescr->surf; // BL modif - added a voir si pas de soucis pour permanent !!!

    for (idir = 0; idir < Z_MSH; idir++) // NF 6/12/2017 Simplification de NEUMANN qui ne marche que pour les frontieres du modèle en X et Y, donc une seule dimension de face
    {
      for (itype = 0; itype < NFACE_MSH; itype++) {
        nneigh = 0;
        neigh = pele->p_neigh[idir][itype];
        if (neigh != NULL) // BL pour gerer les conditions à l'interieur du modèle
        {
          neigh = IO_browse_id(neigh, BEGINNING_TS);
          nneigh = IO_length_id(neigh);

          while (neigh != NULL) {
            i++;
            pneigh = MSH_get_ele(pcmsh, neigh->id_lay, neigh->id, fpout);
            pface = MSH_return_face(pele, pneigh, idir, itype, fpout);

            if (pface->phydro->pbound == NULL) {

              l2 = AQ_calc_sizeweight_at_face(idir, pele, pneigh, nneigh, fpout); // NF18/12/2017
              b_i += AQ_calc_b_i_active(pface, pneigh, deltat, theta, l2, idir, itype, fpout);
              b_i -= AQ_calc_b_i_active(pface, pele, deltat, theta, l2, idir, itype, fpout);
            } // ifpface->pbound==NULL
            else {
              LP_error(fpout, "In libaq%4.2f %s in %s l%d : Hydraulic barrier not implemented yet direction %s face %s of ele %i (ABS_ID)\n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_name_direction(idir, fpout), MSH_name_type(itype, fpout), MSH_get_ele_id(pele, ABS_MSH));
              nbound++;
              b_i += AQ_calc_b_i_neumann(pface, deltat, theta, t, itype, fpout);
            } // else pface->pbound==NULL
            neigh = neigh->next;
          } // while neigh!=NULL
          if (nneigh > 0) {
            pface = pele->face[idir][itype];
            if (pface->p_subface != NULL) {
              for (m = 0; m < NFACE_MSH; m++) {
                if (pface->p_subface[m]->phydro->pbound != NULL) {
                  nbound++;
                  b_i += AQ_calc_b_i_neumann(pface->p_subface[m], deltat, theta, t, itype, fpout);
                }
              }
            }
          }

        }    // if neigh!=NULL
        else // BL pour gerer les conditions aux frontières du modèles
        {
          pface = pele->face[idir][itype];
          if (pface->phydro->pbound != NULL) {
            nbound++;
            l_ele = pele->pdescr->surf;
            b_i += AQ_calc_b_i_neumann(pface, deltat, theta, t, itype, fpout);
          } else
            LP_error(fpout, "In libaq%4.2f %s in %s l%d : no neumann boundary in direction %s face %s of ele %i\n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_name_direction(idir, fpout), MSH_name_type(itype, fpout), MSH_get_ele_id(pele, ABS_MSH));
        } // else neigh!=NULL
      }   // for itype
    }     // for idir

    idir = Z_MSH;
    for (itype = 0; itype < NFACE_MSH; itype++) // NF 9/3/2018 add the flux through vertical faces that were missing
    {
      b_i += AQ_calc_b_oneface(pele, pcmsh, deltat, theta, t, idir, itype, fpout);
    }

    if (nbound == 0)
      LP_error(fpout, "In libaq%4.2f %s in %s l%d : no boundary condition at cell %d (ABS_ID), which should be a border layer with only one neighbor of smaller size at its border face\n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_get_ele_id(pele, ABS_MSH));
  } // if NEUMANN

  if (bound_type == CAUCHY) {
    if (pele->phydro->pbound != NULL) {
      b_i = AQ_calc_b_active(pele, pcmsh, a0, deltat, theta, t, fpout);
      b_i += AQ_calc_b_i_cauchy(pele, deltat, theta, t, CODE_AQ, CODE_AQ, fpout);
    } // if pele->pbound!=NULL
    else {
      LP_error(fpout, "no boundary condition at cell ABS_ID %d INTERN_ID %d\n", MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, INTERN_MSH));
    } // else pele->pbound!=NULL
  }   // if CAUCHY
  if (bound_type == DIRICHLET)
    b_i = AQ_calc_b_i_dirichlet(pele, t, fpout);
  return b_i;
}

/**\fn double AQ_calc_b_i_active(s_face_msh *pface,s_ele_msh *pneigh,double deltat,double theta,double l2,int idir,FILE *fpout)
 *\brief function called in AQ_calc_b_active to calculate the active cells terms of the RHS vector
 *\return void
 *
 */
double AQ_calc_b_i_active(s_face_msh *pface, s_ele_msh *pneigh, double deltat, double theta, double l2, int idir, int itype, FILE *fpout) {
  double b_i;
  b_i = 0;

  if (idir != Z_MSH) {

    b_i = pface->phydro->ptransm->transm[T_HOMO];
  }

  else {
    b_i = pface->phydro->pcond->conductance;
    // BL init b_i=(1-theta)*deltat*(pface->phydro->pcond->conductance/pface->p_descr[MSH_switch_direction(itype,fpout)]->surf)*pneigh->phydro->h[T];
  }
  b_i *= (1 - theta) * deltat * l2 * pneigh->phydro->h[T];
  return b_i;
}

/**\fn double AQ_calc_b_i_cauchy(s_ele_msh *pele,double deltat,double theta,double t,int idir,int itype,FILE *fpout)
 *\brief function called in AQ_calc_b_bound. to calculate the CAUCHY boundaries terms of the RHS vector with a semi-implicit cauchy formulation
 *\return double
 *
 */
double AQ_calc_b_i_cauchy(s_ele_msh *pele, double deltat, double theta, double t, int idir, int itype, FILE *fpout) {

  double b_i = 0., h_riv0, h_riv1;
  s_face_msh *pface;
  s_ft *pft, *pbt1, *pbt2;
  s_bound_aq *pbound;
  double qpot; // debit echange potentiellement limite
  FILE *fb;

  // NF 5/3/2017 simplification du code en evitant repetitions de lignes de commande similaires

  if (idir == CODE_AQ) // CODE_AQ pour dire que l'on est sur un element
  {
    idir = Z_MSH;
    itype = TWO;
    pface = pele->face[idir][itype];
    pbound = pele->phydro->pbound;
  } else { // cauchy a une face, pour libwet?
    pface = pele->face[idir][itype];
    pbound = pface->phydro->pbound;
  }

  // NF 5/3/2017 Il me semble que les if sur MODIF_CALC_AQ sont inutiles si on inclut bien le qlim dans la fonction de cauchy et notamment le seuil qlim
  /*  printf("idabs=%d\n",pele->id[ABS_MSH]);
          fb=fopen("test_fb.txt","w");
          pft=TS_browse_ft(pbound->pft,BEGINNING_TS);
          TS_print_ts(pft,fb);
          pft=TS_browse_ft(pbound->pft,BEGINNING_TS);
          fclose(fb);
          getchar();*/

  if (pele->modif[MODIF_CALC_AQ] == NO_LIM_AQ) {
    if (pbound->freq == CST) {
      h_riv0 = pbound->pft->ft;
      h_riv1 = pbound->pft->ft;
    } else {
      pft = TS_browse_ft(pbound->pft, BEGINNING_TS); // NF 5/3/2017 pour optimiser le temps de calcul, il faudrait mettre le pointeur au plus prêt du temps. Il existe une fonction de parcours faisant cela dans libts

      // printf("3\n");
      h_riv0 = TS_interpolate_ts(t - deltat, pft);
      pft = TS_browse_ft(pbound->pft, BEGINNING_TS);
      // printf("4\n");
      h_riv1 = TS_interpolate_ts(t, pft);
      // printf("t-deltat=%f hriv0=%f t=%f  hriv1=%f\n",t-deltat,h_riv0,t,h_riv1);
      // getchar();
    }

    if (pface != NULL) {
      double conduc;
      conduc = pface->phydro->pcond->conductance;
      // qpot=AQ_calculate_cauchy_q(conduc,pele->phydro->h[T],h_riv0,pbound->qlim);
      // b_i=-(1-theta)*qpot;//NF 1/10/2017 partie explicite//NF 1/10/2017 D'apres l'ecriture de l'equation de diffusivite, un apport q est negatif, ce qui implique -q dans le RHS
      // b_i+=theta*conduc*h_riv1;//NF 1/10/2017 partie implicite. Le signe est du au signe - devant q dans RHS, car par convention une recharge est negative
      b_i = conduc * h_riv1; // FB 08/02/18
      // LP_printf(fpout,"Calc b_i_cauchy (avant resol syst): idabs=%d h_riv1=%f\n",pele->id[ABS_MSH],h_riv1);
      b_i *= deltat;

      // b_i=(1-theta)*deltat*(pface->phydro->pcond->conductance)*(h_riv0-pele->phydro->h[T])+deltat*theta*(pface->phydro->pcond->conductance)*h_riv1;//NF 5/3/2017 Il s'agit d'une formulation semi-implicite, le qlim ne peut pas être pris en compte sur le second terme relatif au temps T+1. On doit donc retrouver une partie de ce poids sur le terme diagonal de A. Cela est fait plus bas dans mat6
      // LP_printf(fpout,"Idabs=%d h_riv0=%f h_riv1=%f Conduc=%f\n",pele->id[ABS_MSH],h_riv0,h_riv1,conduc);//FB 11/04/2018 debug
    } else
      LP_error(fpout, "the face[%s][%s] of ele %d doesn't exist check the conductance input file!!\n", MSH_name_direction(idir, fpout), MSH_name_type(itype, fpout), MSH_get_ele_id(pele, ABS_MSH));
  } // endif(pele->modif[MODIF_CALC_AQ] == NO_LIM_AQ)
  // NF 5/3/2017 Il me semble que les if sur MODIF_CALC_AQ sont inutiles si on inclut bien le qlim dans la fonction de cauchy et notamment le seuil qlim
  else if (pele->modif[MODIF_CALC_AQ] == QLIM_AQ) // NF 3/10/2017 pour le moment DRY_AQ n'est pas utilise. J'enleve la condition (pele->modif[MODIF_CALC_AQ]==DRY_AQ)
  {
    if (pface != NULL) {
      b_i = -pele->phydro->Qlim * deltat; // NF 1/10/2017 D'apres l'ecriture de l'equation de diffusivite, un apport q est negatif, ce qui implique -q dans le RHS. L'utilisation de pele->phydro->Qlim plutot que de pbound->qlim*deltat, est necessaire pour eviter les assèchements de riviere. Bien verifier ce calcul car pele->phydro->Qlim est recalcule a chaque iteration en fonction de la capacite
                                          // d'infiltration de la riviere. Je pense qu'il serait sage dans le cas d'une maille TOP_MESH de mettre Qlim=0.ien verifier que Qlim reprend la valeur de  pbound->qlim dans CAW_cAQ_refresh_cauchy de cawaqs AQ_manage_boundary_coupled.c
      // LP_printf(fpout,"Idabs=%d Qlim=%f\n",pele->id[ABS_MSH],pele->phydro->Qlim);//FB 11/04/2018 debug
    } else
      LP_error(fpout, "the face[%s][%s] of ele %d doesn't exist check the conductance input file!!\n", MSH_name_direction(idir, fpout), MSH_name_type(itype, fpout), MSH_get_ele_id(pele, ABS_MSH));
  }
  return b_i;
}
/**\fn double double AQ_calc_b_i_neumann(s_face_msh *pface,double deltat,double theta,double t,FILE *fpout)
 *\brief function called in AQ_calc_b_bound. to calculate the NEUMANN boundaries terms of the RHS vector
 *\return double
 *
 */
double AQ_calc_b_i_neumann(s_face_msh *pface, double deltat, double theta, double t, int itype, FILE *fpout) {
  double b_i, Q0, Q1;
  double l_side;
  s_ft *pft;
  b_i = 0;

  l_side = pface->p_ele[MSH_switch_direction(itype, fpout)]->pdescr->l_side;
  if (pface->phydro->pbound->freq == CST) {
    // LP_printf(fpout,"l_side in calc b_i neumann %f ",l_side);
    Q0 = pface->phydro->pbound->pft->ft * l_side;
    // BL (lorsque un flux est definie pour plusieurs elements dans le fichier d'entree, celui ci doit contenir le debit global à appliquer sur la totalité des elements et non le debit unitaire à appliquer sur chacun des elements. Le debit total à la frontière des elements concernés est ensuite divisé par l_eletot (la somme des surfaces des elements aux quels sont appliqués les flux) pour obtenir
    // le flux unitaire -par unité de longueur- m.s-1 ) !!!
    Q1 = Q0;
  } else {
    pft = TS_browse_ft(pface->phydro->pbound->pft, BEGINNING_TS);
    // printf("5\n");
    Q1 = TS_interpolate_ts(t, pft) * l_side; // BL Attention l'entree des BC neumann est un debit et non un flux !!!
    pft = TS_browse_ft(pface->phydro->pbound->pft, BEGINNING_TS);
    // printf("6\n");
    Q0 = TS_interpolate_ts(t - deltat, pft) * l_side; // BL Attention l'entree des BC neumann est un debit et non un flux !!!
    // if(Q0!=0)
    //	LP_error(fpout,"Q0 in calc_b_neumann %f t %f\n",Q0,pface->phydro->pbound->pft->t);
  }
  // BL init
  // b_i=(1-theta)*deltat*Q0*pface->p_descr[MSH_switch_direction(itype,fpout)]->surf-theta*deltat*Q1*pface->p_descr[MSH_switch_direction(itype,fpout)]->surf;
  b_i = (1 - theta) * deltat * Q0 + theta * deltat * Q1; // b_i in m3

  return b_i;
}
/**\fn double AQ_calc_b_i_diriclet(s_ele_msh *pele,double t,FILE *fpout)
 *\brief function called in AQ_calc_b_bound. to calculate the DIRICLET boundaries terms of the RHS vector
 *\return double
 *
 */
double AQ_calc_b_i_dirichlet(s_ele_msh *pele, double t, FILE *fpout) {
  double b_i;

  if (pele->phydro->pbound->freq == CST) {
    b_i = pele->phydro->pbound->pft->ft;
    // LP_printf(fpout," ele lay %d ele id %d source term : %f , pbounb value : %f \n",pele->player->id,pele->id[INTERN_MSH],b_i,pele->phydro->pbound->pft->ft); //BL to debug
  } else
    b_i = TS_interpolate_ts(t, pele->phydro->pbound->pft);

  return b_i;
}

/**\fn void AQ_allocate_mat6(s_gc *pgc,FILE *fpout)
 *\brief function to allocate the LHS matrix. needs lmat5 to be already known
 *
 *\return void
 *
 */

/**\fn void AQ_calc_mat6(s_cmsh *pcmsh,s_chronos_CHR *chronos,s_param_aq *param,s_gc *pgc,double t,FILE *fpoutfpout])
 *\brief function to calculate the LHS matrix
 *\return void
 *
 */
void AQ_calc_mat6(s_cmsh *pcmsh, s_chronos_CHR *chronos, s_param_aq *param, s_gc *pgc, double t, FILE *fpout) {
  s_ele_msh *pele;
  s_layer_msh *play;
  s_id_io *eleactive, *source, *bound;
  s_id_io **elebound, **elesource;
  double deltat, theta;
  s_ft *mat6_A;
  int nsource, nbound, nlayer;
  int a0, i;
  nlayer = pcmsh->pcount->nlayer;
  // pgc->mat6=GC_create_mat_double(pgc->lmat5);//NF 9/10/2018 attention si plusieurs appels a cette fonction, cet appel cree une fuite memoire. Il faut initialiser en premier. Cela est fait dans le programme principal

  theta = param->general_param[THETA_AQ];
  deltat = chronos->dt;
  a0 = param->a0;

  eleactive = pcmsh->pcount->pcount_hydro->eleactive;

  elebound = pcmsh->pcount->pcount_hydro->elebound;
  if (eleactive != NULL)
    eleactive = IO_browse_id(eleactive, BEGINNING_TS);
  // else
  // LP_error(fpout,"no active element, eleactive is NULL in AQ_calc_mat6\n");

  /*calcul d'un "bloc mat6" et d'un "bloc b" de l'ele ele_ij*/
  while (eleactive != NULL) {
    pele = MSH_get_ele(pcmsh, eleactive->id_lay, eleactive->id, fpout);
    mat6_A = AQ_calc_mat6_ele_active(pele, pcmsh, a0, deltat, theta, t, fpout);
    AQ_def_LHS(mat6_A, MSH_get_ele_id(pele, ABS_MSH), pgc, fpout);
    mat6_A = TS_free_ts(mat6_A, fpout);
    eleactive = eleactive->next;
  }

  for (nbound = 0; nbound < NBOUND; nbound++) {

    bound = elebound[nbound];
    if (bound != NULL)
      bound = IO_browse_id(bound, BEGINNING_TS);
    while (bound != NULL) {
      pele = MSH_get_ele(pcmsh, bound->id_lay, bound->id, fpout);
      mat6_A = AQ_calc_mat6_ele_bound(pele, pcmsh, a0, deltat, theta, t, nbound, fpout);
      AQ_def_LHS(mat6_A, MSH_get_ele_id(pele, ABS_MSH), pgc, fpout);
      mat6_A = TS_free_ts(mat6_A, fpout);
      bound = bound->next;
    }
  }
}

/**
 * \fn double AQ_calc_b_i_source(s_ele_msh *pele, double theta,double t, double deltat,int nsource)
 * \brief function called in AQ_calc_b_vector to calculate the sources (in [RECHARGE_AQ,UPTAKE_AQ] terms of the RHS vector
 * \return void
 */
// NG : 04/03/2020 : Updated function to integrate new ***psource with potential multiple sources time series per kindof for each element
// NG : 02/06/2021 : Point chain reading procedure modified to speed up calculations.
// NG : 29/02/2024 : Modified and restructured to account for minimum critical thresholds, either on hydraulic head or uptakes values.
void AQ_calc_b_i_source(s_ele_msh *pele, double theta, double t, double deltat, int nsource, FILE *fpout) {

  int j, nb_pbound;
  double b_ij, hcrit, pcrit;
  double *u_values;
  s_ft *pft;
  s_bound_aq **psource;
  s_threshold_aq *pthresh, *ptmp;

  psource = pele->phydro->psource[nsource];        // psource in m/s
  nb_pbound = pele->phydro->nsource_type[nsource]; // Number of pbounds for the associated kindof

  hcrit = 0.;
  pcrit = 0.;

  // If thresholds are defined --> modifying uptakes :
  if (nsource == UPTAKE_AQ && pele->phydro->p_thresh != NULL) { // Reminder : By that point, threshold uptakes time series have been set as positive values, in m/s.

    pthresh = pele->phydro->p_thresh[H_THRESH_AQ];

    if (pthresh == NULL)
      LP_error(fpout,
               "In libaq%4.2f : In file %s, function %s at line %d : Missing threshold input : No water table critical value set for aquifer cell "
               "ABS %d (layer %d, INT_ID %d).\n",
               VERSION_AQ, __FILE__, __func__, __LINE__, pele->id[ABS_MSH], pele->player->id - 1, pele->id[INTERN_MSH]);
    else {
      if (pthresh->freq == CST) { // Fetching water table critical value.
        hcrit = pthresh->pft->ft;
      } else {
        pthresh->pft = TS_function_t_pointed(t - deltat, pthresh->pft, fpout);
        hcrit = TS_interpolate_ts(t, pthresh->pft);
      }

      pele->phydro->thresh_val[H_THRESH_AQ][DMASS_AQ] = hcrit; // Shortcut for output files.

      if ((pele->phydro->hyd_cellstate == DRYCELL_AQ) || (pele->phydro->h[ITER] <= hcrit)) {

        if (pele->phydro->p_thresh[UPTAKE_THRESH_AQ] == NULL) { // No critical uptake ts is defined. Then, total uptake momentarily cancelled.
          pele->phydro->thresh_val[UPTAKE_THRESH_AQ][ITER] = pele->phydro->thresh_val[UPTAKE_THRESH_AQ][T];
          pele->phydro->thresh_val[UPTAKE_THRESH_AQ][T] = 0.;
        } else { // Replacing from critical value fetched from threshold input ts.
          ptmp = pele->phydro->p_thresh[UPTAKE_THRESH_AQ];
          if (ptmp->freq == CST) {
            pcrit = ptmp->pft->ft;
          } else {
            ptmp->pft = TS_function_t_pointed(t - deltat, ptmp->pft, fpout);
            pcrit = TS_interpolate_ts(t, ptmp->pft);
          }
          pele->phydro->thresh_val[UPTAKE_THRESH_AQ][ITER] = pele->phydro->thresh_val[UPTAKE_THRESH_AQ][T];
          pele->phydro->thresh_val[UPTAKE_THRESH_AQ][T] = pcrit;
        }
      } else { // Usual case : he input uptake ts is considered (as usual).
        u_values = AQ_calc_source_value(nb_pbound, psource, t, deltat, fpout);

        pele->phydro->thresh_val[UPTAKE_THRESH_AQ][ITER] = u_values[0];
        pele->phydro->thresh_val[UPTAKE_THRESH_AQ][T] = u_values[1];
        free(u_values);
      }
    }
  } else { // No threshold input defined whatsoever ==> Regular case

    u_values = AQ_calc_source_value(nb_pbound, psource, t, deltat, fpout);

    if (nsource == UPTAKE_AQ) {
      pele->phydro->thresh_val[UPTAKE_THRESH_AQ][ITER] = u_values[0];
      pele->phydro->thresh_val[UPTAKE_THRESH_AQ][T] = u_values[1];
      free(u_values);
    }
  }

  if (nsource == UPTAKE_AQ)
    b_ij = theta * deltat * pele->phydro->thresh_val[UPTAKE_THRESH_AQ][T] + (1 - theta) * deltat * pele->phydro->thresh_val[UPTAKE_THRESH_AQ][ITER];
  else {
    b_ij = theta * deltat * u_values[1] + (1 - theta) * deltat * u_values[0];
    free(u_values);
  }

  b_ij *= -1; // NF 6/12/2017 Not the proper sign convention neg for recharge and pos for pumping

  pele->phydro->b_source += b_ij * pele->pdescr->surf;
}

s_ft *AQ_calc_mat6_ele_active(s_ele_msh *pele, s_cmsh *pcmsh, int a0, double deltat, double theta, double t, FILE *fpout) {

  int idir, itype, i;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface;

  double a_ii, l2, dx;
  double a_neigh, b_coi_coj;
  s_ft *matele_A, *ptmp, *ptmp2;
  int nneigh;

  a_ii = a0 * pele->phydro->S[SCUR] * pele->pdescr->surf;
  a_neigh = 0;
  i = 0;

  // LP_printf(fpout," deltat MAT6 ele active: %f \n",deltat);//debug

  // matele_A=(s_ft*)malloc(sizeof(s_ft));
  matele_A = NULL;
  // LP_printf(fpout,"ele ACTIVE ABS %d GIS %d\n",MSH_get_ele_id(pele,ABS_MSH),MSH_get_ele_id(pele,GIS_MSH));
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      nneigh = 0;
      neigh = pele->p_neigh[idir][itype];
      if (neigh != NULL) {
        neigh = IO_browse_id(neigh, BEGINNING_TS);
        nneigh = IO_length_id(neigh);

        while (neigh != NULL) {
          i++;

          pneigh = MSH_get_ele(pcmsh, neigh->id_lay, neigh->id, fpout);
          //	id_subf=MSH_get_subfaceloc_in_face(pele,pneigh,idir,itype,fpout);
          pface = MSH_return_face(pele, pneigh, idir, itype, fpout);
          if (pface->phydro->pbound == NULL) {
            l2 = AQ_calc_sizeweight_at_face(idir, pele, pneigh, nneigh, fpout); // NF18/12/2017
            ptmp = AQ_calc_a_neigh_active(pface, deltat, theta, l2, t, idir, fpout);
            matele_A = TS_secured_chain_fwd_ts(matele_A, ptmp);
            a_ii += AQ_calc_aii_active(pface, deltat, theta, l2, idir, fpout);
          } else {
            LP_warning(fpout, "In libaq%4.2f %s in %s l%d : Check if the ACTIVE element %d has a CAUCHY condition on its face for libwet\nIf the case it may not be taken into account properly (revision 128->129) in LHS. Because no value is calculated for a_ii.  This comment can be removed from the source code if everything works properly\n", VERSION_AQ, __func__, __FILE__, __LINE__,
                       pele->id[ABS_MSH]);
            ptmp2 = AQ_calc_a_cauchy(pface, deltat, theta, t, itype, fpout);
            a_ii += ptmp2->ft;
            free(ptmp2);
          }

          neigh = neigh->next;
        }
      }
      /*  else{
        ptmp2=AQ_calc_a_cauchy(pface,deltat,theta,t,itype,fpout);
        a_ii+=ptmp2->ft;
        free(ptmp2);
        }*/
    }
  }

  if (a_ii != 0) {

    ptmp = TS_create_function(t, a_ii);
    matele_A = TS_secured_chain_fwd_ts(matele_A, ptmp);
  }

  else
    LP_error(fpout, "libaq %4.2f %s in %s l%d a coefficient of ele GIS_MSH %d ABS_MSH %d INTERN_MSH %d is NULL\n", VERSION_AQ, __func__, __FILE__, __LINE__, MSH_get_ele_id(pele, GIS_MSH), MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, INTERN_MSH));

  return matele_A;
}

/**\fn s_ft *AQ_calc_a_cauchy(s_face_msh *pface,double deltat,double theta,double t,int idir,FILE *fpout)
 *\brief function called in AQ_calc_mat6_ele_active to calculate the cauchy term in A.
 *\return s_ft *
 *
 */
s_ft *AQ_calc_a_cauchy(s_face_msh *pface, double deltat, double theta, double t, int itype, FILE *fpout) {

  s_ft *matele_A;
  double a_neigh;
  a_neigh = 0;

  // LP_printf(fpout,"Conductance is %e\n",pface->phydro->pcond->conductance); // BL to debug
  // a_neigh=+(theta)*deltat*(pface->phydro->pcond->conductance);//NF 1/10/2017 Le + vient du fait que la convention de signe recharge negative implique un q negatif et ainsi dans l'equation discretisee un -q dans le memnbre de droite RHS. De plus q= criv*(hnappe-hsurf).
  a_neigh = deltat * (pface->phydro->pcond->conductance); // FB 08/02/18
  // BL a verifier pour MSH_switch_direction
  // a_neigh=(theta)*deltat*(pface->phydro->pcond->conductance/pface->p_descr[MSH_switch_direction(itype,fpout)]->surf);;
  matele_A = TS_create_function(t, a_neigh);

  return matele_A;
}

/**\fn s_ft *AQ_calc_a_neigh_active(s_face_msh *pface,double deltat,double theta,double l2,double t,int idir,FILE *fpout)
 *\brief function called in AQ_calc_mat6_ele_active to calculate the non diagonal terms of an active cell.
 *\return s_ft *
 *
 */
s_ft *AQ_calc_a_neigh_active(s_face_msh *pface, double deltat, double theta, double l2, double t, int idir, FILE *fpout) {

  s_ft *matele_A;
  double a_neigh;
  a_neigh = AQ_calc_aii_active(pface, deltat, theta, l2, idir, fpout);
  a_neigh *= -1;
  matele_A = TS_create_function(t, a_neigh);

  return matele_A;
}

/**\fn double AQ_calc_aii_active(s_face_msh *pface,double deltat,double theta,double l2,int idir,FILE *fpout)
 *\brief function called in AQ_calc_mat6_ele_active to calculate the diagonal terms of an active cell.
 *\return s_ft *
 *
 */
double AQ_calc_aii_active(s_face_msh *pface, double deltat, double theta, double l2, int idir, FILE *fpout) {
  double a_ii;
  int transm;
  a_ii = 0;

  // LP_printf(fpout,"theta %f, deltat %f, transm %f, l2 %f\n",theta,deltat,pface->phydro->ptransm->transm[T_HOMO],l2); //BL to debug
  if (idir != Z_MSH) {
    a_ii = pface->phydro->ptransm->transm[T_HOMO];

  } else { // NF 18/12/2017 Attention OK pour conductance en m2/s pour une riviere ou a la surface du sol, mais pas pour la drainance entre une grosse maille et une petite maille. Dans ce cas, il faut diviser a_ii par le nb de voisins car ce qui compte est la conductance de la petite face entre voisin
    // LP_printf(fpout,"in calc a active Conductance is %e surf %f \n",pface->phydro->pcond->conductance,pface->p_descr[ONE]->surf); // BL to debug
    a_ii = pface->phydro->pcond->conductance;
    // BL init  a_ii=theta*deltat*(pface->phydro->pcond->conductance/pface->p_descr[MSH_switch_direction(itype,fpout)]->surf);
  }

  a_ii *= theta * deltat * l2;
  return a_ii;
}

/**\fn void AQ_def_RHS(double b_i,int i,s_gc *pgc,FILE *fpout)
 *\brief function called in AQ_calc_b_vector to fill the vector in pgc structur
 *\return s_ft *
 *
 */
void AQ_def_RHS(double b_i, int i, s_gc *pgc, FILE *fpout) {

  double *RHS;
  RHS = pgc->b;
  RHS[i - 1] = b_i;
}
/**\fn void void AQ_def_LHS(s_ft *mat6_A,int id_ele,s_gc *pgc,FILE *fpout)
 *\brief function called in AQ_calc_mat6 to fill a matrix line in pgc structur
 *\return s_ft *
 *
 */
void AQ_def_LHS(s_ft *mat6_A, int id_ele, s_gc *pgc, FILE *fpout) {
  double *LHS;
  int *id_diag;
  int i, pos_diag, i_init;
  s_ft *ptmp;
  LHS = pgc->mat6;
  id_diag = pgc->mat4;

  // LP_printf(fpout,"element : %d\n",id_ele); // BL to debug
  // LP_printf(fpout,"dim matele_A %d\n",TS_length_ts(mat6_A));// BL to debug
  pos_diag = id_diag[id_ele];
  // LP_printf(fpout,"position de l'ele diagonal : %d\n",pos_diag); // BL to debug
  i = id_diag[id_ele - 1];
  // LP_printf(fpout,"nombre de voisins : %d \n",pos_diag-(i+1)); // BL to debug
  i_init = i;
  if (mat6_A != NULL)
    ptmp = TS_browse_ft(mat6_A, BEGINNING_TS);
  else
    LP_error(fpout, "PB de remplissage pour l'element %d", id_ele);
  while (ptmp != NULL) {
    LHS[i] = ptmp->ft;
    // LP_printf(fpout," traitement du voisin : %d \n",i); // BL to debug
    i++;
    ptmp = ptmp->next;
  }
  if (i != pos_diag) {
    LP_error(fpout, "dimensional inconsistency between mat5 and mat6 for ele %d\n", id_ele);
  }
}

s_ft *AQ_calc_mat6_dirichlet(s_ele_msh *pele, double t, FILE *fpout) {
  int idir, itype;
  s_id_io *neigh;
  s_ft *matele_A, *p_tmp, *ptmp;
  double a_ii;

  matele_A = NULL;
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      neigh = pele->p_neigh[idir][itype];
      if (neigh != NULL)
        neigh = IO_browse_id(neigh, BEGINNING_TS);

      while (neigh != NULL) {
        p_tmp = TS_create_function(t, 0.0);
        matele_A = TS_secured_chain_fwd_ts(matele_A, p_tmp);
        neigh = neigh->next;
      }
    }
  }
  a_ii = 1;
  ptmp = TS_create_function(t, a_ii);
  matele_A = TS_secured_chain_fwd_ts(matele_A, ptmp);

  return matele_A;
}

s_ft *AQ_calc_mat6_neumann(s_ele_msh *pele, s_cmsh *pcmsh, double deltat, double theta, double t, int a0, FILE *fpout) {
  int idir, itype, i, nbound = 0, nneigh = 0, id_subf;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface, *psubf;
  int sf;
  double a_ii, l2, dx;
  double a_neigh;

  s_ft *matele_A, *p_tmp, *ptmp, *ab_tmp;
  a_ii = a0 * pele->phydro->S[SCUR] * pele->pdescr->surf;
  a_neigh = 0;

  matele_A = NULL;
  for (idir = 0; idir < ND_MSH; idir++) {
    for (itype = 0; itype < NFACE_MSH; itype++) {
      nneigh = 0;
      neigh = pele->p_neigh[idir][itype];
      if (neigh != NULL) {
        neigh = IO_browse_id(neigh, BEGINNING_TS);
        nneigh = IO_length_id(neigh);

        while (neigh != NULL) {
          pneigh = MSH_get_ele(pcmsh, neigh->id_lay, neigh->id, fpout);
          // id_subf=MSH_get_subfaceloc_in_face(pele,pneigh,idir,itype,fpout);
          pface = MSH_return_face(pele, pneigh, idir, itype, fpout);
          if (pface->phydro->pbound == NULL) {

            l2 = AQ_calc_sizeweight_at_face(idir, pele, pneigh, nneigh, fpout); // NF18/12/2017
            p_tmp = AQ_calc_a_neigh_active(pface, deltat, theta, l2, t, idir, fpout);
            matele_A = TS_secured_chain_fwd_ts(matele_A, p_tmp);
            a_ii += AQ_calc_aii_active(pface, deltat, theta, l2, idir, fpout);
          } // ifpface->pbound==NULL
          else {
            nbound++; // BL le flux à la face condition limite n'est pas calculé en fonction de la différence de charge. il est dans le terme b!!
            p_tmp = TS_create_function(t, 0.0);
            matele_A = TS_secured_chain_fwd_ts(matele_A, p_tmp);
          }
          neigh = neigh->next;
        } // while neigh!=NULL
      }   // if neigh!=NULL

    } // for itype
  }   // for idir

  if (a_ii != 0) {

    ptmp = TS_create_function(t, a_ii);
    matele_A = TS_secured_chain_fwd_ts(matele_A, ptmp);
  } else
    LP_error(fpout, "a coefficient of ele ABS_ID %d INTERN_ID %d is NULL\n", MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, INTERN_MSH));

  return matele_A;
}

s_ft *AQ_calc_mat6_cauchy(s_ele_msh *pele, s_cmsh *pcmsh, double deltat, double theta, double t, int a0, FILE *fpout) {

  int idir, itype, i, nbound = 0;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface, *psubf;

  double a_ii, dx;

  s_ft *matele_A, *ptmp;
  matele_A = AQ_calc_mat6_ele_active(pele, pcmsh, a0, deltat, theta, t, fpout); // NF 22/9/17 contient les poids vers les voisins d'un element courant. Il s'agit d'une chaine de pointeur ft contenant les poids jusqu'au terme diagonal, le dernier

  pface = pele->face[Z_MSH][TWO];
  if (pele->modif[MODIF_CALC_AQ] == NO_LIM_AQ) // NF 26/11/2018 Needs to check the connexion status to have a refresh mat6 compatible with a potential connexion status between two iterations of computation. The initial connexion status is alway NO_LIM_AQ
  {
    ptmp = AQ_calc_a_cauchy(pface, deltat, theta, t, TWO, fpout);
    matele_A->ft += ptmp->ft; // NF 22/9/17 Il suffit d'ajouter la ponderation de Cauchy, ie le CRIV au terme diagonal. Par contre il faudra absolument l'enlever en cas de non linearite pour tenir compte du QLIM Unesolution plus efficace serait une formulation explicite de la conductance
    free(ptmp);
  }

  return matele_A;
}
/**\fn s_ft *AQ_calc_mat6_ele_bound(s_ele_msh *pele,s_cmsh *pcmsh,int a0,double deltat,double theta,double t,int bound_type,FILE *fpout)
 *\brief function called in AQ_calc_mat6 to calculate a line of LHS for BC element
 *\return s_ft *
 *
 */
// refaire cette fonction en découplant les parties
s_ft *AQ_calc_mat6_ele_bound(s_ele_msh *pele, s_cmsh *pcmsh, int a0, double deltat, double theta, double t, int bound_type, FILE *fpout) {
  s_ft *matele_A;

  matele_A = NULL;

  if (bound_type == DIRICHLET) {

    matele_A = AQ_calc_mat6_dirichlet(pele, t, fpout);

  } // if DIRICLET

  else if (bound_type == NEUMANN) {
    matele_A = AQ_calc_mat6_neumann(pele, pcmsh, deltat, theta, t, a0, fpout);
  } // if NEUMANN

  else if (bound_type == CAUCHY) {
    matele_A = AQ_calc_mat6_cauchy(pele, pcmsh, deltat, theta, t, a0, fpout);

  } // if CAUCHY

  return matele_A;
}

/**\fn void AQ_init_sol_vec(s_gc *pgc)
 *\brief function called in solve_aq to initialize the solution vector
 *\return s_ft *
 *
 */
void AQ_init_sol_vec(s_gc *pgc) {

  if (pgc->x != NULL) {
    free(pgc->x);
    pgc->x = NULL;
  }

  pgc->x = GC_create_mat_double(pgc->ndl);
}

/**\fn void AQ_init_sol_vec(s_gc *pgc)
 *\brief function called in solve_aq to initialize the solution vector
 *\return s_ft *
 *
 */
void AQ_init_RHS(s_gc *pgc) {

  if (pgc->b != NULL) {
    free(pgc->b);
    pgc->b = NULL;
  }

  pgc->b = GC_create_mat_double(pgc->ndl);
}

/**\fn void AQ_correct_LHS(s_gc *pgc,int id_mat6,double theta,double dt,s_face_msh *pface_aq,int sign)
 *\brief Correction of the diagonal term of the LHS for a Cauchy cell that switches from one state to another (QLIM_AQ, NO_LIM_AQ). In that case the diagonal term needs to be corrected by +/- (theta)*dt*(pface_aq->phydro->pcond->conductance). Keep in mind that the sign convention is negative for recharge.
 *\return void
 *
 */
void AQ_correct_LHS(s_gc *pgc, int id_mat6, double theta, double dt, s_face_msh *pface_aq, int sign) {
  double cor;

  // cor=(theta)*dt*(pface_aq->phydro->pcond->conductance);
  cor = dt * (pface_aq->phydro->pcond->conductance); // FB 08/02/18
  cor *= sign;
  pgc->mat6[id_mat6 - 1] += cor;
}

// NF,BL 22/10/2015
void AQ_free_mat6(s_gc *pgc) {

  if (pgc->mat6 != NULL) {
    free(pgc->mat6);
    pgc->mat6 = NULL;
  }
}

// NF 26/11/2018 The calculation of the new transmissivity value and the filling up of the mat6 matrice needs to be done before the status of the cauchy condition
void AQ_refresh_mat6_unconfined(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, FILE *fpout) {
  s_layer_msh *play;
  int i, nlayer;
  s_param_aq *param_aq = pchar_aq->settings;

  nlayer = pchar_aq->pcmsh->pcount->nlayer;
  for (i = 0; i < nlayer; i++) {
    play = pchar_aq->pcmsh->p_layer[i];
    if (play->type == UNCONFINED_AQ) {
      AQ_calc_param_unconfined(param_aq, play, fpout);                                                           // MM 24/10/2018 calculates T[HOMO] at each element center
      AQ_calc_face_transm_layer(play, fpout);                                                                    // MM 24/10/2018 calculates T[HOMO] at faces based on T[HOMO] at element centers
                                                                                                                 // Test matthias conductance
      AQ_calc_face_cond_layer(pchar_aq->settings, play, chronos, fpout);                                         // NF 6/12/2018 recalculates the conductance value at faces
      AQ_calc_mat6(pchar_aq->pcmsh, chronos, pchar_aq->settings, pchar_aq->calc_aq, chronos->t[CUR_CHR], fpout); // MM 24/10/2018 reactualisation de mat6
    }
  }
}
