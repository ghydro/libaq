/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: struct_AQ.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <time.h>
typedef struct simul_aq s_simul_aq;
typedef struct param_aq s_param_aq;
typedef struct carac_aq s_carac_aq;
typedef struct picart_aq s_picart_aq;

struct carac_aq {

  s_cmsh *pcmsh;           /*!< mesh caracterisation structur*/
  s_chronos_CHR *pchronos; /*!< Temporal characteristics of the simulation required with this mesh in libaq tini, tend, dt */
  int iappli_gcc;          /*!< Number of the process in pgc for hydrodynamic problem, ie AQ_GC*/
  s_gc *calc_aq;           /*!< gradient conjugue strucutur*/
  s_param_aq *settings;    /*!< simulation parameters*/
  s_picart_aq *ppic;       /*!<Pointer towards Picart structur*/
  int steadynitmax;        /*!<Maximum number of steady state iterations*/
};

struct picart_aq {
  int iit;        /*!<Picart iteration being proceeded*/
  int nitmax;     /*!<Maximum number of Picart iterations*/
  int nitmin;     /*!<Minimum number of Picart iterations*/
  double eps_pic; /*!Convergence criterion for Picart loop (for groundwater head)*/
};

struct simul_aq {
  char name[STRING_LENGTH_LP];
  char *name_outputs;      /*!< Name of the output director */
  FILE *poutputs;          /*!< Debug File where warnings and indications about the sinulation are written */
  s_chronos_CHR *pchronos; /*!< Temporal characteristics of the simulation tini, tend, dt */
  s_clock_CHR *pclock;     /*!< time structure */
  s_carac_aq *pcarac_aq;
  s_out_io **pout;
};

struct param_aq {
  double general_param[NPAR_AQ];
  int calc_state;           /*!< define the simultation type in [steady,transient] */
  int aq_type;              /*!< define if there is unconfined layers in simulation in [CONFINED,UNCONFINED]*/
  int a0;                   /*!< multiplicator of Storativity 0 if STEADY 1 if TRANSIENT*/
  int Surf;                 /*!< define if exist surface layer */
  int cascade;              /*!< aquifer cascades activation status. Values in YES,NO. Set to NO by default. */
  int bypass;               /*!< bypass general activation status. Values in YES,NO. Set to NO by default. */
  double Sy_factor;         /*!< Multiplication factor of specific yield when switching from
                                 UNCONFINED to CONFINED configuration. Set to SY_DEFAULT_FACTOR_AQ by default */
  double anisotropy_factor; /*!< Vertical anisotropy factor. Set to ALPHA_Z_AQ by default */
  int log_balance;          /* Values in [YES,NO]. Prints the AQ balance at the layer scale in the log file. Set to NO by default. */
  int print_lithos;         /* Values in [YES,NO]. Prints out an output file containing the ids of
                               the subsurface lithology. Set to NO by default. */
#ifdef OMP
  s_smp *psmp;
#endif
};

#define new_simul_aq() ((s_simul_aq *)malloc(sizeof(s_simul_aq)))
#define new_param_aq() ((s_param_aq *)malloc(sizeof(s_param_aq)))
#define new_carac_aq() ((s_carac_aq *)malloc(sizeof(s_carac_aq)))
#define new_picart_aq() ((s_picart_aq *)malloc(sizeof(s_picart_aq)))
