/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"
#include "spa.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

char *AQ_name_BC(int iname) {
  char *name;

  switch (iname) {
  case DIRICHLET: {
    name = strdup("DIRICHLET");
    break;
  } // NF 8/9/06
  case NEUMANN: {
    name = strdup("NEUMANN");
    break;
  }
  case CAUCHY: {
    name = strdup("CAUCHY");
    break;
  }
  default: {
    name = strdup("Not a boundary");
    break;
  } // NF 7/19/06
  }
  return name;
}

char *AQ_name_freq(int iname) {
  char *name;

  switch (iname) {
  case CST: {
    name = strdup("Constant value");
    break;
  } // NF 8/9/06
  case VAR: {
    name = strdup("Variable value");
    break;
  }
  }
  return name;
}

char *AQ_name_source(int iname) {
  char *name;
  switch (iname) {
  case RECHARGE_AQ:
    name = strdup("Recharge");
    break;
  case UPTAKE_AQ:
    name = strdup("Uptake");
    break;
  default: {
    name = strdup("Not a source");
    break;
  } // NG 03/01/2020
  }
  return name;
}

char *AQ_name_bc_mb(int iname) {
  char *name;

  switch (iname) {
  case DIRICHLET_MB: {
    name = strdup("DIRICHLET");
    break;
  } // NF 8/9/06
  case NEUMANN_MB: {
    name = strdup("NEUMANN");
    break;
  }
  case OVERFLOW_MB: {
    name = strdup("OVERFLOW");
    break;
  }
  case RIV_MB: {
    name = strdup("iNR");
    break;
  }
  default: {
    name = strdup("Not a boundary");
    break;
  } // NF 7/19/06
  }
  return name;
}

char *AQ_name_modif(int iname) {
  char *name;

  switch (iname) {
  case NO_LIM_AQ: {
    name = strdup("CAUCHY");
    break;
  } // NF 8/9/06
  case QLIM_AQ: {
    name = strdup("QLIM");
    break;
  }
    // case TOP_LIM_AQ : {name=strdup("TOP LIM");break;}//FB 29/05/2017
  case DRY_AQ: {
    name = strdup("DRY");
    break;
  }
  default: {
    name = strdup("Not a Conductance status");
    break;
  } // NF 7/19/06
  }
  return name;
}

char *AQ_name_attributs(int iname) {
  char *name;

  switch (iname) {
  case H_INI:
    name = strdup("Initial Piezometric Head");
    break;
  case THICK:
    name = strdup("Thickness");
    break;
  case TRANSM_HOMO:
    name = strdup("Homogeneous Transmissivity");
    break;
  case TRANSM_ANI:
    name = strdup("Anisotropic Transmissivity");
    break;
  case CONDUCT:
    name = strdup("Conductance parameters");
    break;
  case STORAGE:
    name = strdup("Specific Storage");
    break;
  case SPECIFIC_YIELD:
    name = strdup("Specific yield of unconfined aquifer");
    break;
  case ZTOP:
    name = strdup("Top layer altitude");
    break;
  case ZBOT:
    name = strdup("Bottom Layer Altitude");
    break;
  }
  return name;
}

char *AQ_shortname_param(int iname) {
  char *name;

  switch (iname) {
  case H_INI: {
    name = strdup("H_INI");
    break;
  }
  case THICK: {
    name = strdup("THICK");
    break;
  }
  case TRANSM_HOMO: {
    name = strdup("TRANSM_HOMO");
    break;
  }
  case TRANSM_ANI: {
    name = strdup("TRANSM_ANI");
    break;
  }
  case CONDUCT: {
    name = strdup("CONDUCT");
    break;
  }
  case STORAGE: {
    name = strdup("STORAGE");
    break;
  }
  case SPECIFIC_YIELD: {
    name = strdup("SPECIFIC_YIELD");
    break;
  }
  case ZTOP: {
    name = strdup("ZTOP");
    break;
  }
  case ZBOT: {
    name = strdup("ZBOT");
    break;
  }
  }
  return name;
}

char *AQ_name_regime(int iname) {
  char *name;

  switch (iname) {
  case STEADY_AQ: {
    name = strdup("STEADY");
    break;
  } // NF 8/9/06
  case TRANSIENT_AQ: {
    name = strdup("TRANSIENT");
    break;
  }
  case IMPOSED_AQ: {
    name = strdup("IMPOSED");
    break;
  }
  }
  return name;
}

char *AQ_name_T(int iname) {
  char *name;

  switch (iname) {
  case T_HOMO:
    name = strdup("Homogeneous");
    break;
  case T_X:
    name = strdup("T_X");
    break;
  case T_Y:
    name = strdup("T_Y");
    break;
  }
  return name;
}

char *AQ_name_out(int iname) {
  char *name;
  switch (iname) {
  case PIEZ:
    name = strdup("H");
    break;
  case MASS:
    name = strdup("MASS_BALANCE");
    break;
  case FLUX:
    name = strdup("FLUX");
    break;
  }
  return name;
}

char *AQ_name_mass(int iname) {
  char *name;
  switch (iname) {
  case BC:
    name = strdup("BOUNDS");
    break;
  case UPTAKE:
    name = strdup("UPTAKE");
    break;
  case RECH:
    name = strdup("RECHARGE");
    break;
  case TOT:
    name = strdup("TOTAL");
    break;
  }
  return name;
}

char *AQ_name_eletype(int iname) {
  char *name;
  switch (iname) {
  case ELE_DIRICHLET_AQ:
    name = strdup("BOUND DIRICHLET");
    break;
  case ELE_NEUMANN_AQ:
    name = strdup("BOUND NEUMANN");
    break;
  case ELE_CAUCHY_AQ:
    name = strdup("BOUND CAUCHY");
    break;
  case ELE_RECHARGE_AQ:
    name = strdup("RECHARGE");
    break;
  case ELE_UPTAKE_AQ:
    name = strdup("UPTAKE");
    break;
  case ELE_ACTIVE_AQ:
    name = strdup("ACTIVE");
    break;
  }
  return name;
}

char *AQ_name_link_aq(int iname) {
  char *name;
  switch (iname) {
  case SURF_AQ:
    name = strdup("SURFACE");
    break;
  case NSAT_AQ:
    name = strdup("UNSATURATED ZONE");
    break;
  case HDERM_AQ:
    name = strdup("HYPERDERMIC ZONE");
    break;
  }
  return name;
}

// NG : 20/04/2021
char *AQ_derivation_type_name(int iname) {
  char *name;
  switch (iname) {
  case CASCADE_AQ:
    name = strdup("AQ-CASCADE");
    break;
  case BYPASS_AQ:
    name = strdup("AQ-BYPASS");
    break;
  default:
    name = strdup("Unknown AQ-derivation type");
    break;
  }
  return name;
}

// NG : 20/04/2021
char *AQ_name_source_origin(int iname) {
  char *name;
  switch (iname) {
  case LOCAL_SOURCE_AQ:
    name = strdup("ISOLATED SOURCE");
    break;
  case ZNS_SOURCE_AQ:
    name = strdup("COUPLING WITH ZNS");
    break;
  case HDERM_SOURCE_AQ:
    name = strdup("COUPLING WITH HDERM");
    break;
  case CASCADE_DISCHARGE_AQ:
    name = strdup("CASCADE-TYPE SOURCE");
    break;
  case BYPASS_DISCHARGE_AQ:
    name = strdup("BYPASS-TYPE SOURCE");
    break;
  }
  return name;
}

// NG : 05/02/2021
char *AQ_name_outvar(int iname) {
  char *name;
  switch (iname) {
  case H_END:
    name = strdup("H_END");
    break;
  case DV_DT:
    name = strdup("STORAGE");
    break;
  case FLUX_X_ONE:
    name = strdup("FLOWX_ONE");
    break;
  case FLUX_X_TWO:
    name = strdup("FLOWX_TWO");
    break;
  case FLUX_Y_ONE:
    name = strdup("FLOWY_ONE");
    break;
  case FLUX_Y_TWO:
    name = strdup("FLOWY_TWO");
    break;
  case FLUX_Z_ONE:
    name = strdup("FLOWZ_ONE");
    break;
  case FLUX_Z_TWO:
    name = strdup("FLOWZ_TWO");
    break;
  case REC:
    name = strdup("RECHARGE");
    break;
  case UPT:
    name = strdup("UPTAKES");
    break;
  case FLUX_DIRICHLET:
    name = strdup("DIRICHLET");
    break;
  case FLUX_NEUMANN:
    name = strdup("NEUMANN");
    break;
  case SOL:
    name = strdup("OVERFLOWS");
    break;
  case ENR:
    name = strdup("RIV_TO_AQ");
    break;
  case ERR:
    name = strdup("ERR");
    break;
  case ERR_REL:
    name = strdup("ERR_REL");
    break;
  }
  return name;
}

// NG : 10/08/2021
char *AQ_name_layer_type(int iname) {
  char *name;
  switch (iname) {
  case CONFINED_AQ:
    name = strdup("CONFINED AQUIFER");
    break;
  case UNCONFINED_AQ:
    name = strdup("UNCONFINED AQUIFER");
    break;
  default:
    name = strdup("Unknown AQ-layer type");
    break;
  }
  return name;
}

// NG : 29/02/2024
char *AQ_threshold_type(int itype) {
  char *name;

  switch (itype) {
  case H_THRESH_AQ:
    name = strdup("Critical minimal hydraulic head");
    break;
  case UPTAKE_THRESH_AQ:
    name = strdup("Critical minimal uptake");
    break;
  default:
    name = strdup("Unknown threshold value type");
    break;
  }
  return name;
}

char *AQ_cell_hydraulic_state(int iname) {
  char *name;

  switch (iname) {
  case WETCELL_AQ: {
    name = strdup("WET");
    break;
  }
  case DRYCELL_AQ: {
    name = strdup("DRY");
    break;
  }
  }
  return name;
}