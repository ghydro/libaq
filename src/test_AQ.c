/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: test_AQ.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "CHR.h"
#include "GC.h"
#include "MSH.h"
#include "AQ.h"
#include "global_AQ.h"
#include "global_var_AQ.h"

int main(int argc, char **argv) {
  /* Date of the beginning of the simulation */
  char *date_of_simulation;
  /* Structure from time.h containing date, time, etc */
  static struct timeval current_time;
  /* Structure containing computer clock time */
  struct tm *clock_time;
  /* Current time in seconds when programm starts */
  time_t clock;
  /* Error report */
  int timi;
  /* Current time in simulation */
  double t, dt;
  /* Time of end of the simulation in s */
  double tend;

  FILE *fp;
  char nom_fichier[STRING_LENGTH_LP];

  /* Creates the simulation structure */
  Simul = AQ_init_simul();

  /* Date */
  timi = gettimeofday(&current_time, NULL);
  clock = (time_t)current_time.tv_sec;
  ctime(&clock);
  clock_time = localtime(&clock);
  date_of_simulation = asctime(clock_time);

  /* Opening of command file */
  if (argc != 3)
    LP_error(stderr, "WRONG COMMAND LINE : libaq%4.2f CommandFileName DebugFileName\n", VERSION_AQ);

  if ((Simul->poutputs = fopen(argv[2], "w")) == 0)
    LP_error(stderr, "Impossible to open the debug file %s\n", argv[2]);

  LP_log_file_header(stderr, "libaq", VERSION_AQ, date_of_simulation, argv[1]);
  LP_log_file_header(Simul->poutputs, "libaq", VERSION_AQ, date_of_simulation, argv[1]);

  /********************************************************/
  /*    Now input files are read  --> see file input.y    */
  /********************************************************/

  CHR_begin_timer();
  lecture(argv[1], Simul->poutputs);
  Simul->pclock->time_spent[LEC_CHR] += CHR_end_timer();

  /********************************************************/
  /*           Starting HYDRO calculations                */
  /********************************************************/

  t = Simul->pchronos->t[BEGINNING_TS];
  tend = Simul->pchronos->t[END_TS];
  dt = Simul->pchronos->dt;
  CHR_calculate_simulation_date(Simul->pchronos, t);

  CHR_begin_timer();
  /*
  MSH_check_redundancy(Simul->pcmsh,Simul->poutputs);
  MSH_referencing_mesh(Simul->pcmsh,Simul->poutputs);


  MSH_order_element_in_ref(Simul->pcmsh,Simul->poutputs);

  MSH_check_element_ordering(Simul->pcmsh,Simul->poutputs);

  //Compatibilite avec GEOSAM mailleur de newsam
  MSH_print_geosam_input(Simul->pcmsh,Simul->poutputs);
  //Correspondance with GIS
  MSH_link_mesh_SIG(Simul->pcmsh,Simul->poutputs);

  MSH_create_ele_faces(Simul->pcmsh,Simul->poutputs);
  // MSH_link_mesh_SIG(Simul->pcmsh,Simul->poutputs);
  MSH_create_vertical_faces(Simul->pcmsh,Simul->poutputs);
  MSH_list_all_ele(Simul->pcmsh,Simul->poutputs);
  */
  // MSH_link_neighbour(Simul->pcmsh,Simul->poutputs);

  // Compatibilite avec GEOSAM mailleur de newsam
  MSH_print_geosam_input(Simul->pcmsh, Simul->poutputs);
  // Correspondance with GIS
  MSH_link_mesh_SIG(Simul->pcmsh, Simul->poutputs);
  AQ_calc_face_transm_All(Simul->pcmsh, Simul->poutputs); // BL fait dans input
  AQ_finalize_count_hydro(Simul->pcmsh, Simul->poutputs);

  //  MSH_list_all_ele_pneigh(Simul->pcmsh,Simul->poutputs);
  // AQ_print_elebound(Simul->pcmsh,Simul->poutputs); //BL to debug
  // AQ_print_elesource(Simul->pcmsh,Simul->poutputs); // BL to debug
  // AQ_print_eleactive(Simul->pcmsh,Simul->poutputs); // BL to debug
  AQ_init_mat_int(Simul->pcmsh, Simul->pgc, Simul->poutputs);
  Simul->pclock->time_spent[INIT_CHR] += CHR_end_timer();
  CHR_begin_timer();
  // AQ_print_mat_int(Simul->pgc,Simul->poutputs); // BL to debug
  // AQ_calc_mat6(Simul->pcmsh,Simul->param,Simul->pgc,1.0,Simul->poutputs); //BL to debug
  // AQ_print_LHS(Simul->pgc,Simul->poutputs); // BL to debug
  // AQ_calc_vector_b(Simul->pcmsh,Simul->param,Simul->pgc,1.0,Simul->poutputs); //BL to debug
  // AQ_print_RHS(Simul->pgc,Simul->poutputs);//BL to debug
  AQ_solve_diffusiv(Simul->pgc, Simul->pcmsh, Simul->param, Simul->pout, Simul->poutputs);
  Simul->pclock->time_spent[MATRIX_SOLVE_CHR] += CHR_end_timer();

  /********************************************************/
  /*        End, summary of the computation length        */
  /********************************************************/

  CHR_calculate_calculation_length(Simul->pclock, Simul->poutputs);
  printf("Time of calculation : %f s\n", Simul->pclock->time_length);

  CHR_print_times(Simul->poutputs, Simul->pclock);

  fclose(Simul->poutputs);
  return 0;
}
