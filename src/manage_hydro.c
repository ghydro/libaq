/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_hydro.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
  #ifdef COUPLED
  #include "reservoir.h"
  #include "FP.h"
  #include "NSAT.h"
  #include "HYD.h"
  #endif
*/
#include "MSH.h"
#include "AQ.h"

/**\fn s_transm_aq *AQ_create_transm()
 *\brief create transmissivity structur
 *\return ptransm
 *
 */

s_transm_aq *AQ_create_transm() {
  s_transm_aq *ptransm;
  ptransm = (s_transm_aq *)malloc(sizeof(s_transm_aq));
  bzero((char *)ptransm, sizeof(s_transm_aq));
  return ptransm;
}

/**\fn s_cond_aq *AQ_create_transm()
 *\brief create conductivity structur
 *\return pcond
 *
 */
s_cond_aq *AQ_create_cond() {
  s_cond_aq *pcond;
  pcond = new_cond_aq();
  bzero((char *)pcond, sizeof(s_cond_aq));
  return pcond;
}

/**\fn s_hydro_aq *AQ_create_ele_hydro(int intern_id)
 *\brief create hydro strucutur
 * return phydro
 */
s_hydro_aq *AQ_create_ele_hydro(int intern_id) {
  s_hydro_aq *phydro;
  int i, j;

  phydro = new_hydro();
  bzero((char *)phydro, sizeof(s_hydro_aq));
  phydro->id = intern_id;
  phydro->pbound = NULL;
  phydro->psource = NULL;
  for (i = 0; i < NSOURCE; i++)
    phydro->nsource_type[i] = 0; // NG : 04/03/2020 : Initialization added for nsource per type
  // phydro->ptransm=AQ_create_transm();
  phydro->ptransm = NULL;
  phydro->pcond = NULL;
  phydro->pmb = NULL;
  // NF 10/7/2017 initialisation with default values for the parameters
  phydro->S[SY] = YIELD_UNCONFINED_AQ;
  phydro->S[SS] = YIELD_CONFINED_AQ;
  phydro->S[SCUR] = phydro->S[SS];
  phydro->status = CONFINED_AQ;       // NF : 29/11/2018
  phydro->pderivation = NULL;         // NG : 20/04/2021
  phydro->emitting_status = NO_TS;    // NG : 21/04/2021
  phydro->zbot = CODE_AQ;             // NG : 13/08/2021 : Turning initialization back on
  phydro->ztop = CODE_AQ;             // NG : 13/08/2021 : Turning initialization back on
  phydro->hyd_cellstate = WETCELL_AQ; // NG : 17/04/2024
  phydro->p_thresh = NULL;            // NG : 29/02/2024

  for (j = 0; j < NSTEP; j++)
    phydro->thresh_val[H_THRESH_AQ][j] = CODE_TS;
  for (j = 0; j < NSTEP; j++)
    phydro->thresh_val[UPTAKE_THRESH_AQ][j] = 0.;

  // Transport-related attributes
  phydro->transp_var = NULL;
  phydro->transp_flux = NULL;

  return phydro;
}
/**\fn s_layer_msh *AQ_create_hydro_tab(s_layer_msh *player, FILE *fpout)
 *\brief create hydro strucutur for each element and faces in layer player
 *
 * return player
 */
void *AQ_create_hydro_tab(s_layer_msh *player, FILE *fpout) {
  int i, j, idir, itype, nele;
  s_ele_msh *pele;
  s_face_msh *pface, *psubf;
  nele = player->nele;

  for (i = 0; i < nele; i++) {
    pele = player->p_ele[i];
    pele->phydro = AQ_create_ele_hydro(i);
    pele->phydro->pmb = AQ_create_mb(NMASS, NTIME_AQ);
    for (idir = 0; idir < ND_MSH; idir++) {

      for (itype = 0; itype < NFACE_MSH; itype++) {
        pface = pele->face[idir][itype];

        if (pface->p_subface != NULL) {
          if (idir != Z_MSH) {
            for (j = 0; j < NFACE_MSH; j++) {
              psubf = pface->p_subface[j];

              if (psubf != NULL && psubf->phydro == NULL) {
                psubf->phydro = AQ_create_ele_hydro(psubf->id);
              }
            }

          } else {
            for (j = 0; j < NCODEG_MSH; j++) {
              psubf = pface->p_subface[j];

              if (psubf != NULL && psubf->phydro == NULL) {
                psubf->phydro = AQ_create_ele_hydro(psubf->id);
              }
            }
          }

        } else {
          if (pface->phydro == NULL) {
            pface->phydro = AQ_create_ele_hydro(pface->id);
          }
        }
      }
    }
  }
}
/**\fn void AQ_calc_faceXY_cond_layer(s_param_aq*,s_layer_msh *player,int mean_type,FILE *fpout)
 *\brief calculate for all horizontal faces, its conductance in the layer player.
 *
 *Warning : player have to be created
 * return void, directly modify the player pointer
 */
void AQ_calc_faceXY_cond_layer(s_param_aq *param_aq, s_layer_msh *player, s_chronos_CHR *chronos, FILE *fpout) {
  int nele;
  s_ele_msh *pele;
  s_face_msh *pface, *psubf;
  int i, j, idir, itype, itype_f, m;
  int type_transm, mean_type, cond_type;

  mean_type = player->mean_type;

  nele = player->nele;
  for (j = 0; j < nele; j++) {
    pele = player->p_ele[j];
    type_transm = pele->phydro->ptransm->type;

    for (idir = 0; idir < Z_MSH; idir++) {
      for (itype = 0; itype < NFACE_MSH; itype++) {
        pface = pele->face[idir][itype];
        AQ_define_conductXY(pface, fpout);
      }
    }
  }
}

/**\fn void AQ_calc_faceZ_cond_layer(s_param_aq*,s_layer_msh *player,int mean_type,FILE *fpout)
 *\brief calculate all vertical faces conductance in the layer player.
 *
 *Warning : player have to be created
 * return void, directly modify the player pointer
 */
void AQ_calc_faceZ_cond_layer(s_param_aq *param_aq, s_layer_msh *player, s_chronos_CHR *chronos, FILE *fpout) {
  int nele;
  s_ele_msh *pele;
  s_face_msh *pface, *psubf;
  int i, j, idir, itype, itype_f, m;
  int type_transm, mean_type, cond_type;

  mean_type = player->mean_type;

  nele = player->nele;
  for (j = 0; j < nele; j++) {
    pele = player->p_ele[j];
    type_transm = pele->phydro->ptransm->type;

    idir = Z_MSH;
    itype = TWO_MSH;
    pface = pele->face[idir][itype];

    if (pele->phydro->pcond == NULL) {
      cond_type = AUTO_COND;
    } else {
      cond_type = player->cond_type;
    }

    AQ_define_conductZ(param_aq, pface, itype, cond_type, mean_type, chronos, fpout);
  }
}

/**\fn void AQ_calc_face_cond_layer(s_param_aq*,s_layer_msh *player,int mean_type,FILE *fpout)
 *\brief calculate all vertical faces conductance in the layer player.
 *
 *Warning : player have to be created
 * return void, directly modify the player pointer
 */
void AQ_calc_face_cond_layer(s_param_aq *param_aq, s_layer_msh *player, s_chronos_CHR *chronos, FILE *fpout) {
  AQ_calc_faceXY_cond_layer(param_aq, player, chronos, fpout);
  AQ_calc_faceZ_cond_layer(param_aq, player, chronos, fpout);
}

/**\fn void AQ_finalize_interlayer_drainance(s_param_aq* param_aq,FILE *fpout)
 *\brief calculate all vertical faces conductance in the full mesh.
 *
 * return void, directly modify the player pointer
 */
void AQ_finalize_interlayer_drainance(s_carac_aq *pcarac_aq, s_chronos_CHR *chronos, FILE *fpout) {
  s_layer_msh *player;
  int icount = 0, nlayers;
  s_cmsh *pcmsh;
  s_param_aq *settings;

  pcmsh = pcarac_aq->pcmsh;
  settings = pcarac_aq->settings;
  nlayers = pcmsh->pcount->nlayer;

  while (icount < nlayers) {
    player = pcmsh->p_layer[icount++];
    AQ_calc_faceZ_cond_layer(settings, player, chronos, fpout);
  }
}

/**\fn void AQ_calc_face_transm_layer(s_layer_msh *player,int mean_type,FILE *fpout)
 *\brief calculate face transmissivity in the layer player. The face transmissivity is the mean (defined by mean_type in [ARITHMETIC,HARMONIC,GEOMETRIC]) of two neighboor cells transmissities values weighted by their length
 *
 *Warning : player have to be created
 * return void, directly modify the player pointer
 */
void AQ_calc_face_transm_layer(s_layer_msh *player, FILE *fpout) {
  /*NF 9/10/2018 Cette fonction est a couper en 2. L'une pour la transmissivite, et l'autre pour la conductance. Tu n'appelleras dans le cas de la charge libre que la fonction relative a la transmissivite*/

  int nele;
  s_ele_msh *pele;
  s_face_msh *pface, *psubf;
  int i, j, idir, itype, itype_f, m;
  int type_transm, mean_type, cond_type;

  mean_type = player->mean_type;

  // if(player->cond_type == AUTO_COND)//NF 13/9/2022 No need for this condition since AUTO_COND is used for the conductance, ie Z direction, which is excluded from this function
  // LP_error(fpout,"libaq%4.2f %s in %s l%d : Problem with calculation of faces transmissivities with auto_cond for layer %d \n",VERSION_AQ,__func__,__FILE__,__LINE__,player->id);
  nele = player->nele;
  for (j = 0; j < nele; j++) {
    pele = player->p_ele[j];
    type_transm = pele->phydro->ptransm->type;
    // LP_printf(fpout,"Calc transm, ele %d : ",pele->id[ABS_MSH]);//NF 6/12/2018 for debugging only
    for (idir = 0; idir < Z_MSH; idir++) // NF 13/9/2022 no need for Z_MSH, since there was a if condition idir!=Z_MSH
    {
      for (itype = 0; itype < NFACE_MSH; itype++) {
        pface = pele->face[idir][itype];
        // MSH_calcul_on_face(pface,idir,fpout); //BL fait ici pour pouvoir avoir l epaisseur definie (moins une boucle sur l ensemble de elements peut se faire en dehors du calcul de la transmissivite aux faces avec la fonction MSH_calcul_on_face_all cf manage_face.c!!
        //  LP_printf(fpout," %d%d --> ",idir,itype);//NF 6/12/2018 for debugging only
        // if(idir!=Z_MSH)//NF 13/9/2022
        //{
        AQ_calc_face_thick(pface, itype, fpout);
        // AR MM 16/10/2018 deplacement dans le input		  AQ_create_face_transm(pface,mean_type,type_transm,fpout);
        AQ_calc_face_transm(pface, mean_type, type_transm, fpout);
        // MM 18/10/18 ajout dans le input		  AQ_create_XY_cond(pface,fpout);
        //}
      }
    }
  }
}

/**\fn double AQ_calc_Tmean(double T_neigh_1,double l_neigh_1,double T_neigh_2,double l_neigh_2,int type_mean,FILE *fpout)
 *\brief calculate the mean (defined by mean_type in [ARITHMETIC,HARMONIC,GEOMETRIC]) of two values (T_neigh_1,T_neigh_2) weighted by their coefficients (l_neigh_1,l_neigh_2)
 *
 * return T_mean
 */
double AQ_calc_Tmean(double T_neigh_1, double l_neigh_1, double T_neigh_2, double l_neigh_2, int type_mean, FILE *fpout) {
  double T_mean;
  if (type_mean == ARITHMETIC) {
    T_mean = (l_neigh_1 * T_neigh_1 + l_neigh_2 * T_neigh_2) / (l_neigh_1 + l_neigh_2);
  } else if (type_mean == HARMONIC) {
    T_mean = (T_neigh_1 * T_neigh_2) * (l_neigh_1 + l_neigh_2) / (l_neigh_1 * T_neigh_2 + l_neigh_2 * T_neigh_1);
  } else if (type_mean == GEOMETRIC) {
    T_mean = exp((log(T_neigh_1) * l_neigh_1 + log(T_neigh_2) * l_neigh_2) / (l_neigh_1 + l_neigh_2));
  } else
    LP_error(fpout, "the mean type %d is not defined", type_mean);
  return T_mean;
}

/**\fn void AQ_create_face_transm(s_face_msh *pface,int type_transm)
 *\brief create the face transmissivity between two cells.
 * Warning : pface have to be created
 * return void directly modify pface
 */

void AQ_create_face_transm(s_layer_msh *player, FILE *fpout) {
  int nele;
  s_ele_msh *pele;
  s_face_msh *pface, *psubf;
  int i, j, idir, itype, itype_f, m;
  int type_transm;

  nele = player->nele;
  for (j = 0; j < nele; j++) {
    pele = player->p_ele[j];
    type_transm = pele->phydro->ptransm->type;

    for (idir = 0; idir < Z_MSH; idir++) {
      for (itype = 0; itype < NFACE_MSH; itype++) {
        pface = pele->face[idir][itype];
        // MSH_calcul_on_face(pface,idir,fpout); //BL fait ici pour pouvoir avoir l epaisseur definie (moins une boucle sur l ensemble de elements peut se faire en dehors du calcul de la transmissivite aux faces avec la fonction MSH_calcul_on_face_all cf manage_face.c!!
        // if(idir!=Z_MSH)
        //{
        if (pface->p_subface != NULL) {
          for (m = 0; m < Z_MSH; m++) {
            psubf = pface->p_subface[m];
            if (psubf->phydro->ptransm == NULL && psubf->type != BOUND) {
              psubf->phydro->ptransm = AQ_create_transm();
              psubf->phydro->ptransm->transm[T_HOMO] = 0;
            }
          }
        } else {
          if (pface->phydro->ptransm == NULL && pface->type != BOUND) {
            pface->phydro->ptransm = AQ_create_transm();
            pface->phydro->ptransm->transm[T_HOMO] = 0;
          }
        }
        //}
      }
    }
  }
}

/**\fn void AQ_calc_face_transm(s_face_msh *pface,int mean_type,int type_transm,FILE *fpout)
 *\brief calculate the face transmissivity between two cells. The face transmissivity is the mean (defined by mean_type in [ARITHMETIC,HARMONIC,GEOMETRIC]) of two neighboor cells transmissities values weighted by their length
 * Warning : pface have to created
 * return void directly modify pface
 */
void AQ_calc_face_transm(s_face_msh *pface, int mean_type, int type_transm, FILE *fpout) {
  int m;
  s_face_msh *psubf;

  if (pface->p_subface != NULL) {
    for (m = 0; m < Z_MSH; m++) {
      psubf = pface->p_subface[m];
      if (psubf != NULL) {
        if (psubf->type != BOUND) {
          AQ_calc_transm(psubf, mean_type, type_transm, fpout);
        }
      }
    }

  } else {
    if (pface->type != BOUND) {
      AQ_calc_transm(pface, mean_type, type_transm, fpout);
    }
  }
}

// NF 06/12/2018
/* NG 31/05/2021 : Changing T display formats from float to scientific in case of very low values
                   Introducing a new EPS_T_AQ parameter to check low values of T.*/
void AQ_eval_T(s_face_msh *pface, double T1, double T2, double l1, double l2, int mean_type, int Ttype, FILE *fpout) {

  if (fabs(T1) > EPS_T_AQ && fabs(T2) > EPS_T_AQ) {
    pface->phydro->ptransm->transm[T_HOMO] = AQ_calc_Tmean(T1, l1, T2, l2, mean_type, fpout);
  } else {
    if (fabs(T1) < EPS_T_AQ)
      LP_error(fpout, "libaq%4.2f in %s l%d: the transmissivity %s of ele %d is %15.6e m2/s !", VERSION_AQ, __FILE__, __LINE__, AQ_name_T(T_HOMO), MSH_get_ele_id(pface->p_ele[ONE], GIS_MSH), T1);
    if (fabs(T2) < EPS_T_AQ)
      LP_error(fpout, "libaq%4.2f in %s l%d:the transmissivity %s of ele %d is %15.6e m2/s !", VERSION_AQ, __FILE__, __LINE__, AQ_name_T(Ttype), MSH_get_ele_id(pface->p_ele[TWO], GIS_MSH), T2);
  }
}

/**\fn void AQ_calc_transm(s_face_msh *pface,int mean_type,int type_transm,FILE *fpout)
 *\brief calculate the face transmissivity between two cells. The face transmissivity is the mean (defined by mean_type in [ARITHMETIC,HARMONIC,GEOMETRIC]) of two neighboor cells transmissities values weighted by their length
 * Warning : pface have to created
 * return void directly modify pface
 */
void AQ_calc_transm(s_face_msh *pface, int mean_type, int type_transm, FILE *fpout) {
  double l_neigh_1, l_neigh_2, T_neigh_1, T_neigh_2;
  int n;
  int calc = YES_TS;

  l_neigh_1 = pface->p_descr[ONE]->l_side;
  l_neigh_2 = pface->p_descr[TWO]->l_side;
  if (type_transm == HOMO) {
    T_neigh_1 = pface->p_ele[ONE]->phydro->ptransm->transm[T_HOMO];
    T_neigh_2 = pface->p_ele[TWO]->phydro->ptransm->transm[T_HOMO];
    AQ_eval_T(pface, T_neigh_1, T_neigh_2, l_neigh_1, l_neigh_2, mean_type, type_transm, fpout); // NF 6/12/2018
  } else {
    for (n = T_HOMO + 1; n < ND_T; n++) {
      T_neigh_1 = pface->p_ele[ONE]->phydro->ptransm->transm[n];
      T_neigh_2 = pface->p_ele[TWO]->phydro->ptransm->transm[n];
      AQ_eval_T(pface, T_neigh_1, T_neigh_2, l_neigh_1, l_neigh_2, mean_type, type_transm, fpout); // NF 6/12/2018
    }
  }
}

void AQ_create_Z_conduct_face(s_layer_msh *player, FILE *fpout) {
  int nele;
  s_ele_msh *pele;
  s_face_msh *pface, *psubf;
  int i, j, idir, itype, itype_f, m;

  nele = player->nele;
  for (j = 0; j < nele; j++) {
    pele = player->p_ele[j];

    // for(idir=0;idir<ND_MSH;idir++)
    //{
    idir = Z_MSH; // NF 13/9/2022 Rather than checking if idir==Z_MSH later, just avoid the for loop that is useless
    for (itype = 0; itype < NFACE_MSH; itype++) {
      pface = pele->face[idir][itype];
      // if(idir==Z_MSH)
      //{
      if (pface != NULL) {
        // AR Cas face avec subface (4 subfaces)
        if (pface->p_subface != NULL) {
          for (i = 0; i < NCODEG_MSH; i++) {
            psubf = pface->p_subface[i];
            // LP_printf(fpout,"%d \n",i);
            if (psubf != NULL) {
              if (pface->loc != BOT_MSH && pface->loc != TOP_MSH) // BL init //MM 29/10/2018 cas de 4 subfaces avec mailles susjacente et sousjacente ?
              {
                if (psubf->phydro->pcond == NULL) // MM 29/10/2018 : creation de lespace memoire pour ces subfaces
                  psubf->phydro->pcond = AQ_create_cond();

              } else // BL pour plus tard gerer les mailles riv ?
              {
                if (pface->loc != BOT_MSH) // NF 8/3/2018 I am not sure this value is allocated properly. To be checked
                {
                  if (psubf->phydro->pcond == NULL)
                    psubf->phydro->pcond = AQ_create_cond();
                }
              }
            }
          }
        }

        // AR Cas face sans subface
        else {
          if (pface->loc != BOT_MSH && pface->loc != TOP_MSH) // BL init
          {
            if (pface->phydro->pcond == NULL)
              pface->phydro->pcond = AQ_create_cond();
          } else // BL pour plus tard gerer les mailles riv ?
          {
            if (pface->loc != BOT_MSH) // NF 8/3/2018 I am not sure this value is allocated properly. To be checked
            {
              if (pface->phydro->pcond == NULL)
                pface->phydro->pcond = AQ_create_cond();
            }
          }
        }
      }
      //}
    }
    //}
  }
}

/**\fn void AQ_calc_conduct_faceZ(s_face_msh *pface,int itype,FILE *fpout)
 *\brief calculate the conductance parameter at Z face
 * Warning : pface have to be created before
 * return void directly modify pface
 */
void AQ_calc_conduct_faceZ(s_face_msh *pface, int itype, FILE *fpout) {
  double s_min;
  if (pface->loc != BOT_MSH && pface->loc != TOP_MSH) // BL init
  {
    s_min = TS_min(pface->p_descr[ONE]->surf, pface->p_descr[TWO]->surf);
    // AR MM 16/10/2018 place memoire deja cree	     if(pface->phydro->pcond==NULL)
    // AR MM 16/10/2018 place memoire deja cree	     pface->phydro->pcond=AQ_create_cond();
    // pface->phydro->pcond->conductance = pface->p_ele[ONE]->phydro->pcond->conductance*pface->p_descr[MSH_switch_direction(itype,fpout)]->surf/ pface->p_ele[ONE]->pdescr->surf; //init1
    pface->phydro->pcond->conductance = pface->p_ele[ONE]->phydro->pcond->conductance * s_min;

  } else // BL pour plus tard gerer les mailles riv ?
  {
    if (pface->loc != BOT_MSH) // NF 8/3/2018 I am not sure this value is allocated properly. To be checked
    {
      // AR MM 16/10/2018 place memoire deja cree	  if(pface->phydro->pcond==NULL)
      //	  pface->phydro->pcond=AQ_create_cond();
      // pface->phydro->pcond->conductance=pface->p_ele[MSH_switch_direction(itype,fpout)]->phydro->pcond->conductance*pface->p_descr[itype]->surf/pface->p_ele[MSH_switch_direction(itype,fpout)]->pdescr->surf;//init1
      pface->phydro->pcond->conductance = pface->p_ele[ONE]->phydro->pcond->conductance * pface->p_descr[ONE]->surf;
    }

    // BL Attention par defaut aux extréminite TOP_MSH et BOT_MSH du modele pface->p_descr[itype] est egal à pface->p_descr[MSH_switch_direction(itype,fpout)]->surf possibilite de definir la surface reelle de la riviere  A FAIRE DANS LE INPUT !!!
  }
}
/**\fn void AQ_calc_TZ_face(s_face_msh *pface,int itype,FILE *fpout)
 *\brief calculate the conductance parameter at Z face
 * Warning : pface have to be created before
 * return void directly modify pface
 */
void AQ_calc_TZ_face(s_face_msh *pface, int itype, int mean_type, FILE *fpout) {
  double l_neigh_1, l_neigh_2, l_min;
  double T_neigh_1, T_neigh_2;
  double TZ_face;
  // AR MM 16/10/2018 place memoire deja cree	  if(pface->phydro->pcond==NULL)
  // AR MM 16/10/2018 place memoire deja cree	    pface->phydro->pcond=AQ_create_cond();
  if (pface->type != BOT_MSH) {
    // l_neigh_1=pface->p_ele[ONE]->phydro->Thick/2;
    l_neigh_1 = pface->p_descr[ONE]->l_side;
    if (l_neigh_1 == 0)
      LP_error(fpout, "thickness is null at ele %d. Check your input files and command file\n", pface->p_ele[ONE]->id[ABS_MSH]); // BL ATTENTION l'epaisseur dans la direction Z[ONE-TWO] ONE doit toujours etre definie si on est pas à la base du modèle sinon le calcul de la conductance via Tz n'est pas realisable.

    T_neigh_1 = pface->p_ele[ONE]->phydro->pcond->conductance;

    if (pface->loc != TOP_MSH) {
      // l_neigh_2=pface->p_ele[TWO]->phydro->Thick/2;
      l_neigh_2 = pface->p_descr[TWO]->l_side;
      if (l_neigh_2 == 0)
        LP_error(fpout, "thickness is null at ele %d. Check your input files and command file\n", pface->p_ele[TWO]->id[ABS_MSH]);
      T_neigh_2 = pface->p_ele[TWO]->phydro->pcond->conductance;
      LP_printf(fpout, "l_neigh_2 : %f l_neigh_1 : %f T_neigh_2 : %f T_neigh_1 : %f\n", l_neigh_2, l_neigh_1, T_neigh_2, T_neigh_1); // BL to debug
    }

    else {

      l_neigh_2 = pface->p_descr[TWO]->l_side;
      T_neigh_2 =
         pface->phydro->pcond->conductance; // BL ATTENTION si on veut calculer la conductance via un TZ à la surface il convient de définir la conductance à la face (A FAIRE DANS LE INPUT) et de définir le lside à la face (A FAIRE DANS LE INPUT) et la surf à la face (A FAIRE DANS LE INPUT ou par defaut pface->p_descr[itype] doit etre egal à pface->p_descr[MSH_switch_direction(itype,fpout)]->surf)
      LP_printf(fpout, "Z_TOP l_neigh_2 : %f l_neigh_1 : %f T_neigh_2 : %f T_neigh_1 : %f\n", l_neigh_2, l_neigh_1, T_neigh_2, T_neigh_1); // BL to debug
    }
    if (T_neigh_1 != 0 && T_neigh_2 != 0) {
      TZ_face = AQ_calc_Tmean(T_neigh_1, l_neigh_1, T_neigh_2, l_neigh_2, mean_type, fpout);
      l_min = TS_min(l_neigh_1, l_neigh_2);

      // pface->phydro->pcond->conductance=(TZ_face*l_min*pface->p_descr[itype]->surf)/((l_neigh_1+l_neigh_2)*pface->p_descr[MSH_switch_direction(itype,fpout)]->surf);
      pface->phydro->pcond->conductance = (TZ_face * pface->p_descr[itype]->surf) / (pface->p_descr[MSH_switch_direction(itype, fpout)]->surf);
      // pface->phydro->pcond->conductance=TZ_face;
    } else {
      pface->phydro->pcond->conductance = 0;
    }
  } else {
    pface->phydro->pcond->conductance = 0;
  }
}

// NG : 11/08/2021 Adding *param_aq in order to use user-defined value of vertical anisotropy factor

/**\fn void AQ_calc_TZ_face(s_face_msh *pface,int itype,FILE *fpout)
 *\brief calculate the conductance parameter at Z face
 * Warning : pface have to be created before
 * return void directly modify pface
 */
void AQ_calc_AUTO_COND_face(s_param_aq *param_aq, s_face_msh *pface, int itype, int mean_type, s_chronos_CHR *chronos, FILE *fpout) {
  s_ele_msh *pele;
  double l_neigh_1, l_neigh_2, l_min;
  double T_neigh_1, T_neigh_2;
  double TZ_face;
  double cond_stor;
  int i, j;
  double alpha_z = param_aq->anisotropy_factor;

  // LP_printf(fpout,"In function %s : Anisotropy factor value = %10.3e.\n", __func__,alpha_z); // NG check

  if (pface->type != BOT_MSH) {

    if (pface->loc != TOP_MSH) {
      for (i = 0; i < NFACE_MSH; i++) {
        j = MSH_switch_direction(i, fpout);
        if (pface->p_ele[i] == NULL)
          LP_error(fpout, "p_ele[%s] is null for face %d loc %s link in direction %s with ele %d", MSH_name_type(i, fpout), pface->id, MSH_name_position_cell(pface->loc, fpout), MSH_name_type(j, fpout), pface->p_ele[j]->id[ABS_MSH]);
      }
      // l_neigh_1=pface->p_descr[ONE]->l_side;
      l_neigh_1 = pface->p_ele[ONE]->phydro->Thick / 2;
      if (l_neigh_1 == 0)
        LP_error(fpout, "the thickness is null at ele %d. Check your input files and command file\n", pface->p_ele[ONE]->id[ABS_MSH]); // BL ATTENTION l'epaisseur dans la direction Z[ONE-TWO] ONE doit toujours etre definie si on est pas à la base du modèle sinon le calcul de la conductance via Tz n'est pas realisable.
      // peut être modifier si on entre le coefficient d'aniso par layer (assez simple à faire)
      T_neigh_1 = pface->p_ele[ONE]->phydro->ptransm->transm[T_HOMO] * alpha_z / pface->p_ele[ONE]->phydro->Thick; // 19/08/2019 AJ,AR,TV, il manquait le coef 2.0//AJ 13/9/2022 l'epaisseur/2 est introduite ici et a la ligne suivant --> supprimer le coeff 2
      l_neigh_2 = pface->p_ele[TWO]->phydro->Thick / 2;
      // l_neigh_2=pface->p_descr[TWO]->l_side;
      if (l_neigh_2 == 0)
        LP_error(fpout, "the thickness is null at ele %d. Check your input files and command file\n", pface->p_ele[TWO]->id[ABS_MSH]);
      // peut être modifier si on entre le coefficient d'aniso par layer (assez simple à faire)
      T_neigh_2 = pface->p_ele[TWO]->phydro->ptransm->transm[T_HOMO] * alpha_z / pface->p_ele[TWO]->phydro->Thick; // 19/08/2019 AJ,AR,TV, il manquait le coef 2.0//AJ 13/9/2022 l'epaisseur/2 est introduite ici et a la ligne suivant --> supprimer le coeff 2

      printf("l1,l2,c1,c2,cond %f %f %f %f %f \n", l_neigh_1, l_neigh_2, pface->p_ele[ONE]->phydro->ptransm->transm[T_HOMO], pface->p_ele[TWO]->phydro->ptransm->transm[T_HOMO], pface->phydro->pcond->conductance); // TV 17/12/2019

      if (T_neigh_1 != 0 && T_neigh_2 != 0) {
        TZ_face = AQ_calc_Tmean(T_neigh_1, l_neigh_1, T_neigh_2, l_neigh_2, mean_type, fpout) / (l_neigh_1 + l_neigh_2);
        l_min = TS_min(pface->p_descr[itype]->surf, pface->p_ele[MSH_switch_direction(itype, fpout)]->pdescr->surf);

        // pface->phydro->pcond->conductance=(TZ_face*2*l_min*pface->p_descr[itype]->surf)/((l_neigh_1+l_neigh_2)*pface->p_descr[MSH_switch_direction(itype,fpout)]->surf);
        pface->phydro->pcond->conductance = TZ_face * l_min;
        // pface->phydro->pcond->conductance=(TZ_face*pface->p_descr[itype]->surf)/(pface->p_descr[MSH_switch_direction(itype,fpout)]->surf);
        // pface->phydro->pcond->conductance=TZ_face;
        printf("l1,l2,T1,T2,cond %f %f %f %f %f \n", l_neigh_1, l_neigh_2, T_neigh_1, T_neigh_2, pface->phydro->pcond->conductance);                                                                       // TV 17/12/2019
        printf("XY ONE: Thick %f , l_side %f , TWO: Thick %f l_side %f \n", pface->p_ele[ONE]->phydro->Thick, pface->p_descr[ONE]->l_side, pface->p_ele[TWO]->phydro->Thick, pface->p_descr[TWO]->l_side); // TV 17/12/2019
      } else
        LP_error(fpout, " The conductance can't be calculated T_neigh_1 : %f T_neigh_2 : %f", T_neigh_1, T_neigh_2);

    } else {
      pele = pface->p_ele[ONE];
      //  cond_stor=pele->phydro->S[Ss]*pele->pdescr->surf/chronos->dt;//NF 19/10/2017 I don't remember why BL did that
      cond_stor = pele->phydro->ptransm->transm[T_HOMO] * alpha_z / pele->phydro->Thick; // NF 19/10/2017 vertical permeability
      cond_stor /= pele->phydro->Thick / 2;                                              // NF 19/10/2017 To have it in s-1, I divide by half thickness of the aquifer
      cond_stor *= pele->pdescr->surf;                                                   // NF 19/10/2017 To have it in m2.s-1
      pface->phydro->pcond->conductance = cond_stor;
    }

  } else {
    pface->phydro->pcond->conductance = 0;
  }
}

/**\fn void AQ_define_conductZ(s_param_aq*, s_ele_mesh*,FILE*)
 *\brief calculate the conductance parameter at Z face
 * Warning : pface have to be created before
 * return void directly modify pface
 */
void AQ_define_conductZ(s_param_aq *param_aq, s_face_msh *pface, int itype, int cond_type, int mean_type, s_chronos_CHR *chronos, FILE *fpout) {
  s_face_msh *psubf;
  int i;
  // pface=pele->face[Z][TWO];
  if (pface != NULL) {
    if (pface->p_subface != NULL) {
      for (i = 0; i < NCODEG_MSH; i++) {
        psubf = pface->p_subface[i];
        // LP_printf(fpout,"%d \n",i);
        if (psubf != NULL) {
          if (cond_type == COND) {
            AQ_calc_conduct_faceZ(psubf, itype, fpout);
          } else if (cond_type == TZ) {
            AQ_calc_TZ_face(psubf, itype, mean_type, fpout);
          } else {
            AQ_calc_AUTO_COND_face(param_aq, psubf, itype, mean_type, chronos, fpout);
          }
        }
      }
    } else {
      if (cond_type == COND) {
        AQ_calc_conduct_faceZ(pface, itype, fpout);
      } else if (cond_type == TZ) {
        AQ_calc_TZ_face(pface, itype, mean_type, fpout);
      } else {
        AQ_calc_AUTO_COND_face(param_aq, pface, itype, mean_type, chronos, fpout);
      }
    }
  } else
    LP_error(fpout, "The selected face in dir Z type %s from ele %d is null", MSH_name_type(itype, fpout), pface->p_ele[MSH_switch_direction(itype, fpout)]->id[ABS_MSH]);
}

/**\fn s_count_hydro_aq *AQ_create_count_hydro()
 *\brief create a hydro counter
 * return  pcount_hydro
 */
s_count_hydro_aq *AQ_create_count_hydro() {
  s_count_hydro_aq *pcount_hydro;
  int i;
  pcount_hydro = new_counter_hydro();
  bzero((char *)pcount_hydro, sizeof(s_count_hydro_aq));

  pcount_hydro->elebound = ((s_id_io **)malloc(NBOUND * sizeof(s_id_io *)));
  for (i = 0; i < NBOUND; i++) {
    // pcount_hydro->elebound[i]=(s_ft *) malloc(sizeof(s_ft));
    pcount_hydro->elebound[i] = NULL;
  }

  pcount_hydro->elesource = ((s_id_io **)malloc(NSOURCE * sizeof(s_id_io *)));
  for (i = 0; i < NSOURCE; i++) {
    // pcount_hydro->elesource[i]=(s_ft *) malloc(sizeof(s_ft));
    pcount_hydro->elesource[i] = NULL;
  }

  // pcount_hydro->eleactive=(s_ft*) malloc(sizeof(s_ft));
  pcount_hydro->eleactive = NULL;

  return pcount_hydro;
}

/**\fn void AQ_finalize_count_hydro(s_cmsh *pcmsh,FILE *fpout)
 *\brief finalize the boundaries counters and set default boundaries
 * Warning pcount_hydro have to be created
 * return  void, directly modifiy pcount_hydro
 */
void AQ_finalize_count_hydro(s_cmsh *pcmsh, FILE *fpout) {
  int nlayer, nele;
  int i, j, default_set;
  s_count_hydro_aq *pcount;
  s_layer_msh *player;
  s_ele_msh *pele;
  s_id_io *id_list;

  nlayer = pcmsh->pcount->nlayer;
  pcount = pcmsh->pcount->pcount_hydro;

  for (i = 0; i < nlayer; i++) {
    player = pcmsh->p_layer[i];
    nele = player->nele;
    for (j = 0; j < nele; j++) {
      pele = player->p_ele[j];
      default_set = 0;
      if (pele->type == BOUND) {
        // BL pour que si on ne définie pas toutes les faces BC dans le fichier d'entrée les BC aux faces non définies soient flux null.
        if (pele->phydro->pbound == NULL || pele->phydro->pbound->type == NEUMANN) {
          default_set = AQ_set_default_BC(pele, fpout);
          if (default_set > 0) {
            // LP_printf(fpout,"Setting pele %d as default BC \n",MSH_get_ele_id(pele,ABS_MSH)); //BL to debug
            id_list = IO_create_id(i, j);
            AQ_define_elebound(id_list, pcount->elebound, NEUMANN, fpout); // BL attention on copie le id_list dans la fonction du coup il faut libérer le poiteur envoyé !!!
            id_list = IO_free_id_serie(id_list, fpout);
          }
        } else // BL doit etre if(pele->phydro->pbound !=NULL &&  pele->phydro->pbound->type!=NEUMANN)
        {

          default_set = AQ_set_default_BC(pele, fpout); // BL pour les autres BC le flux est null aux frontières mais la BC n'est pas changee en NEUMANN
        }
      }
      if (pele->type == ACTIVE) {

        id_list = IO_create_id(i, j);
        if (pcount->eleactive != NULL) {
          // LP_printf(fpout,"adding id_list at elebound[%d]\n",kindof);
          pcount->eleactive = IO_browse_id(pcount->eleactive, END_TS);
          pcount->eleactive = IO_secured_chain_fwd_id(pcount->eleactive, id_list);

        } else {

          // LP_printf(fpout,"elebound[%d] not assigned yet\n",kindof);
          pcount->eleactive = IO_browse_id(id_list, BEGINNING_TS);
        }
      }
    }
  }
}

/**\fn void AQ_define_elecount(s_id_io *id_list,s_id_io *elebound,FILE *fpout)
 *\brief add list of element in elebound
 * Warning elecount heve to be created
 *\return void directly modify elecount
 */
void AQ_define_elecount(s_id_io *id_list, s_id_io *elecount, FILE *fpout) {

  s_id_io *pcpy;

  pcpy = IO_copy_id_serie(id_list, BEGINNING_TS);
  if (elecount == NULL) {
    // LP_printf(fpout,"elebound[%d] not assigned yet\n",kindof);
    elecount = pcpy;

  } else {
    // LP_printf(fpout,"adding id_list at elebound[%d]\n",kindof);
    elecount = IO_browse_id(elecount, END_TS);
    elecount = IO_secured_chain_fwd_id(elecount, pcpy);
  }
}

/**\fn void AQ_print_eleactive(s_cmsh *pcmsh,FILE *fpout)
 *\brief print the elements which are active element
 *\return void
 */
void AQ_print_eleactive(s_cmsh *pcmsh, FILE *fpout) {
  int i;
  s_id_io *ptmp;
  s_id_io *eleactive;
  eleactive = pcmsh->pcount->pcount_hydro->eleactive;
  LP_printf(fpout, "Active elements : \n");
  ptmp = IO_browse_id(eleactive, BEGINNING_TS);
  while (ptmp != NULL) {
    LP_printf(fpout, "Layer : %d cell : %d ABS_MSH : %d\n", ptmp->id_lay, ptmp->id, MSH_get_ele_id(pcmsh->p_layer[ptmp->id_lay]->p_ele[ptmp->id], ABS_MSH));
    ptmp = ptmp->next;
  }
}

/**\fn void AQ_create_XY_cond(s_face_msh *pface,FILE *fpout)
 *\brief create the space for the XY conductivity for flux calculation.
 * Warning : pface have to created
 * return void directly modify pface
 */

void AQ_create_XY_conduct_face(s_layer_msh *player, s_chronos_CHR *chronos, FILE *fpout) {
  double dx;
  int nele;
  s_ele_msh *pele;
  s_face_msh *pface, *psubf;
  int i, j, idir, itype, itype_f, m;
  int type_transm, mean_type, cond_type;

  //======================================

  nele = player->nele;
  for (j = 0; j < nele; j++) {

    pele = player->p_ele[j];
    for (idir = 0; idir < ND_MSH; idir++) {

      for (itype = 0; itype < NFACE_MSH; itype++) {
        pface = pele->face[idir][itype];
        // MSH_calcul_on_face(pface,idir,fpout); //BL fait ici pour pouvoir avoir l epaisseur definie (moins une boucle sur l ensemble de elements peut se faire en dehors du calcul de la transmissivite aux faces avec la fonction MSH_calcul_on_face_all cf manage_face.c!!
        if (idir != Z_MSH) {
          //======================================
          if (pface->p_subface != NULL) {
            for (m = 0; m < Z_MSH; m++) {
              psubf = pface->p_subface[m];
              //	  if(psubf->phydro->ptransm!=NULL && psubf->type!=BOUND)
              if (psubf->type != BOUND) {
                if (psubf->phydro->pcond == NULL) {
                  psubf->phydro->pcond = AQ_create_cond(); // NF 9/10/2018 scinder allocation et calcul etape 3 Mathias
                }
              }
            }
          } else {
            if (pface->type != BOUND) {
              if (pface->phydro->pcond == NULL) {
                pface->phydro->pcond = AQ_create_cond(); // NF 9/10/2018 scinder allocation et calcul etape 3 Mathias
              }
            }
          }
        }
        //===========================
      }
    }
  }
}
//=====================

/**\fn void AQ_calc_conduct_faceXY(s_face_msh *pf,FILE *fpout)
 *\brief calculate the conductance value at one face pf.
 * return void directly modify pface
 */
void AQ_calc_conduct_faceXY(s_face_msh *pf, FILE *fpout) // NF 6/12/2018 new function
{
  double dx;
  if (pf->type != BOUND) {
    dx = TS_min(pf->p_descr[ONE]->l_side, pf->p_descr[TWO]->l_side);
    pf->phydro->pcond->conductance = pf->phydro->ptransm->transm[T_HOMO] * 2 * dx / (pf->p_descr[ONE]->l_side + pf->p_descr[TWO]->l_side);
  }
}

/**\fn void AQ_define_conductXY(s_face_msh *pface,FILE *fpout)
 *\brief calculate the XY conductivity for flux calculation.
 * Warning : pface have to be created
 * return void directly modify pface
 */
void AQ_define_conductXY(s_face_msh *pface, FILE *fpout) { // NF 6/12/2018 rewritten to have a nice, short, and clean piece of code
  int m;
  s_face_msh *pf;

  pf = pface;

  if (pface->p_subface != NULL) {
    for (m = 0; m < Z_MSH; m++) {
      pf = pface->p_subface[m];
      AQ_calc_conduct_faceXY(pf, fpout);
    }
  } else
    AQ_calc_conduct_faceXY(pf, fpout);
}

/**\fn void AQ_recalc_transm_unconfined(s_ele_msh *pele,FILE*fpout)
 *\brief calculate the tranmissivity of unconfined layers
 *
 * return void directly modify phydro->ptransm
 */
void AQ_recalc_transm_unconfined(s_ele_msh *pele, FILE *fpout) {
  s_hydro_aq *phydro;

  phydro = pele->phydro;
  if (phydro->ptransm->type == HOMO) {
    if (phydro->h[DRAWD] <= phydro->Thick)
      phydro->ptransm->transm[HOMO] = phydro->ptransm->transm_init[HOMO] * (1 - phydro->h[DRAWD] / phydro->Thick);
    else
      LP_error(fpout, "element INTERN %d ABS %d GIS %d denoye !!", MSH_get_ele_id(pele, INTERN_MSH), MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, GIS_MSH));

  } else {
    if (phydro->h[DRAWD] <= phydro->Thick) {
      phydro->ptransm->transm[T_X] = phydro->ptransm->transm_init[T_X] * (1 - phydro->h[DRAWD] / phydro->Thick);
      phydro->ptransm->transm[T_Y] = phydro->ptransm->transm_init[T_Y] * (1 - phydro->h[DRAWD] / phydro->Thick);
    } else
      LP_error(fpout, "element INTERN %d ABS %d GIS %d denoye !!", MSH_get_ele_id(pele, INTERN_MSH), MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, GIS_MSH));
  }
}
/**\fn void void AQ_recalc_transm_unconfined_All(s_ele_msh *pele,FILE*fpout)
 *\brief calculate the tranmissivity of unconfined layers for all elements
 *
 * return void directly modify phydro->ptransm
 */
void AQ_recalc_transm_unconfined_All(s_cmsh *pcmsh, FILE *fpout) {

  s_ele_msh *pele;
  s_layer_msh *player;

  int nlayer, nele;
  int k, j;
  nlayer = pcmsh->pcount->nlayer;
  for (k = 0; k < nlayer; k++) {
    player = pcmsh->p_layer[k];
    nele = player->nele;
    for (j = 0; j < nele; j++) {
      pele = player->p_ele[j];
      AQ_recalc_transm_unconfined(pele, fpout);
    }
  }
}

// NF,BL 19/10/2015 defines transmissivitties at element faces, following  X-ONe, X-TWO, Y-ONE,Y-TWO, which means W,E,S,N
void AQ_set_transm_face(s_ele_msh *pele, double v1, double v2, double v3, double v4, FILE *flog) {
  double transm[Z_MSH][ND_MSH];
  int i, j;

  transm[X_MSH][ONE_MSH] = v1;
  transm[X_MSH][TWO_MSH] = v2;
  transm[Y_MSH][ONE_MSH] = v3;
  transm[Y_MSH][TWO_MSH] = v4;

  for (i = 0; i < Z_MSH; i++) {
    for (j = 0; j < ND_MSH; j++) {
      if ((pele->face[i][j]->phydro->ptransm == NULL) && (pele->face[i][j]->type != BOUND))
        pele->face[i][j]->phydro->ptransm = AQ_create_transm();
      if (pele->face[i][j]->type != BOUND)
        pele->face[i][j]->phydro->ptransm->transm[T_HOMO] = transm[i][j];
    }
  }
}

/**
 * \fn void AQ_calc_face_thick(s_face_msh *pface,int itype,FILE *fpout)
 * \brief calculate the face thickness for U darcy calculation
 * \return void directly modify phydro->ptransm
 */
void AQ_calc_face_thick(s_face_msh *pface, int itype, FILE *fpout) {
  s_face_msh *psubf;
  int m;
  if (pface->p_subface != NULL) {
    for (m = 0; m < Z_MSH; m++) {
      psubf = pface->p_subface[m];
      if (psubf->phydro->Thick == 0) {
        if (psubf->type != BOUND)
          psubf->phydro->Thick = psubf->p_ele[MSH_switch_direction(itype, fpout)]->phydro->Thick + (((psubf->p_ele[itype]->phydro->Thick - psubf->p_ele[MSH_switch_direction(itype, fpout)]->phydro->Thick) / (psubf->p_descr[ONE]->l_side + psubf->p_descr[TWO]->l_side)) * psubf->p_descr[MSH_switch_direction(itype, fpout)]->l_side);
        else
          psubf->phydro->Thick = psubf->p_ele[MSH_switch_direction(itype, fpout)]->phydro->Thick;
      }
    }
  } else {
    if (pface->phydro->Thick == 0) {
      if (pface->type != BOUND)
        pface->phydro->Thick = pface->p_ele[MSH_switch_direction(itype, fpout)]->phydro->Thick + (((pface->p_ele[itype]->phydro->Thick - pface->p_ele[MSH_switch_direction(itype, fpout)]->phydro->Thick) / (pface->p_descr[ONE]->l_side + pface->p_descr[TWO]->l_side)) * pface->p_descr[MSH_switch_direction(itype, fpout)]->l_side);
      else
        pface->phydro->Thick = pface->p_ele[MSH_switch_direction(itype, fpout)]->phydro->Thick;
    }
  }
}

void AQ_transport_attributes_allocation(s_hydro_aq *phydro, int nb_species, FILE *flog) {
  phydro->transp_var = (double *)calloc(nb_species, sizeof(double));
  phydro->transp_flux = (double *)calloc(nb_species * NB_MB_FLUX_AQ_TRANSPORT, sizeof(double)); // TODO The number of fluxes should be insterted into this function
}

/**
 * \fn void AQ_update_aq_system_hydraulic_state(s_cmsh*, FILE*)
 * \brief Updates the hydraulic status (DRY/WET) of aquifer cells at the end of a time step (after hydro calculations)
 * \return Directly modifies the phydro->hyd_cellstate
 */
void AQ_update_aq_system_hydraulic_state(s_cmsh *pcmsh, FILE *flog) {

  int i, j, nlayer, nele;
  s_layer_msh *player;
  s_ele_msh *pele;

  nlayer = pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    player = pcmsh->p_layer[i];
    nele = player->nele;
    for (j = 0; j < nele; j++) {
      pele = player->p_ele[j];

      if ((fabs(pele->phydro->zbot - CODE_AQ) > EPS_AQ)) { // if zbot explicitly set. Otherwise, the cell stays in "WET" state, as initialized.
        if (pele->phydro->h[T] > pele->phydro->zbot) {
          pele->phydro->hyd_cellstate = WETCELL_AQ;
        } else {
          pele->phydro->hyd_cellstate = DRYCELL_AQ;
          /* LP_printf(flog,"CELL state %s abs %d - Hydraulic head : %f - Bottom elevation %f\n",AQ_cell_hydraulic_state(pele->phydro->hyd_cellstate),
                     MSH_get_ele_id(pele,ABS_MSH),pele->phydro->h[T],pele->phydro->zbot);  // NG check  */
        }
      }
    }
  }
}