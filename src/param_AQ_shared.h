/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: param_AQ_shared.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

enum timing_ref_aq { INI_AQ, END_AQ, DMASS_AQ, NTIME_AQ };

enum kindof_BC { DIRICHLET, NEUMANN, CAUCHY, NBOUND };

enum boundary_frenquency { CST, VAR, NTYPE };

enum type_emmagasinement { SS, SY, SCUR, N_STOR };

enum sources { RECHARGE_AQ, UPTAKE_AQ, NSOURCE };

enum transm { T_HOMO, T_X, T_Y, ND_T };

enum type_transm { HOMO, ANI, ND_TT };

/*NF 26/9/2017 Quels sont ces types ?. Calcul fait dans manage_hydro.c lance par AQ_define_conduc//NF 9/12/2020 I think TZ is never used,
 at least at the layer scale. The initial idea was to provide it for river cells I think. Then a thickness is required.
 This is done in libhyd with the definition of the HZ. See if it is usefull in libaq for the drainance maybe ? But i think
 the drainance is an AUTO_COND or a COND condition */

enum type_cond { COND, TZ, AUTO_COND, NB_C };

// NF 16/11/2010 to simplify the code and get rid of many useless variable
enum calculation_step { T, ITER, INTERPOL, ITER_INTERPOL, DRAWD, PIC, NSTEP };

enum grad_directop { E_AQ, W_AQ, N_AQ, S_AQ, UP_AQ, DOWN_AQ, NDIR_AQ };

enum mass_type { RECH, UPTAKE, BC, TOT, NMASS };

// NG : 05/03/2020 : New type added for link with HDERM module
enum nb_link_aq { SURF_AQ, NSAT_AQ, HDERM_AQ, NB_LINK_AQ };

// pour boucle de picart nappe-riviere et nappe-surface
enum qlim { MODIF_MB_AQ, MODIF_CALC_AQ, NB_MODIF };

enum layer_type { CONFINED_AQ, UNCONFINED_AQ, NLAY_TYPE };

// NG : 20/04/2021 : List of all possible AQ-derivation type.
enum deriv_type { CASCADE_AQ, BYPASS_AQ, NDERIV_TYPE };

/* NG : 21/04/2021: Set of parameters useful to track the origin of a source pbound in case of multiple source
 * inputs in a same aquifer element.
 * - CASCADE_DISCHARGE_AQ/BYPASS_DISCHARGE_AQ : Cauchy discharge (or fraction of it) coming from a AQ-derivation.
 * (see CaWaQS2.94's (and onward) user guide).
 * - LOCAL_SOURCE_AQ : Injection read in the input file. */
enum source_origin { LOCAL_SOURCE_AQ, ZNS_SOURCE_AQ, HDERM_SOURCE_AQ, CASCADE_DISCHARGE_AQ, BYPASS_DISCHARGE_AQ, NORIGIN_SOURCE_AQ };

/* NG : 29/02/2024
 * - H_THRESH_AQ : points to critical threshold on water table to modify uptakes automatically.
 * - UPTAKE_THRESH_AQ : points to replacement uptake ts (if defined) whenever the hydraulic head gets below a critical water level (reachable through H_THRESH_AQ). */
enum threshold_type { H_THRESH_AQ, UPTAKE_THRESH_AQ, NTHRESH_AQ };

/* NG : 17/04/2024 :
 * 'hydraulic_status' enumerates the possible water cell state :
 * WETCELL_AQ = hydraulic head above cell bottom elevation
 * DRYCELL_AQ = hydraulic head equal or below to bottom elevation.
 * If bottom is left undeclared (for CONFINED layer for instance), the cell is always considered in WETCELL_AQ status. */
enum hydraulic_status { WETCELL_AQ, DRYCELL_AQ, NWATER_STATE_AQ };

#define EPS_AQ_CAUCHY 0.001