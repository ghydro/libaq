/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_threshold.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

/**
 * \fn s_threshold_aq *AQ_fill_threshold (int, s_ft*, double, FILE*)
 * \brief Fills s_threshold_aq* attributes according to kindof.
 * \return Filled s_threshold_aq*
 */
s_threshold_aq *AQ_fill_threshold(int kindof, s_ft *pft, double stot, FILE *fpout) {

  int i;
  s_threshold_aq *pthresh = NULL;
  s_ft *ptmp = NULL;

  pthresh = new_threshold_aq();
  bzero((char *)pthresh, sizeof(s_threshold_aq));

  pthresh->type = kindof;

  if (kindof == UPTAKE_THRESH_AQ) {
    ptmp = TS_division_ft_double(pft, stot, fpout);
    // TS_print_ts(ptmp,fpout);
    // LP_printf(fpout,"surface %f\n",stot);   // check
    pft = TS_free_ts(pft, fpout);
    pthresh->pft = ptmp; // If threshold uptakes --> in m/s. Set as positive values.
  } else {
    pthresh->pft = pft; // Threshold water table in m.
  }

  pthresh->freq = CST; // Set constant by default.
  if (pthresh->pft->next != NULL) {
    pthresh->freq = VAR;
  }

  return pthresh;
}

/**
 * \fn s_threshold_aq** AQ_create_p_threshold(int, s_ft*, double, FILE*)
 * \brief Allocates memory fro **p_thresh table and fills it with s_threshold_aq*
 * \return **p_thresh
 */
s_threshold_aq **AQ_create_p_threshold(int kindof, s_ft *pft, double stot, FILE *fpout) {

  int i;
  s_threshold_aq **p_thresh = NULL;

  p_thresh = (s_threshold_aq **)malloc(NTHRESH_AQ * sizeof(s_threshold_aq *));
  for (i = 0; i < NTHRESH_AQ; i++)
    p_thresh[i] = NULL;

  p_thresh[kindof] = AQ_fill_threshold(kindof, pft, stot, fpout);
  return p_thresh;
}

/**
 * \fn void AQ_define_threshold (s_cmsh*, s_id_io*, s_ft*, double, int, FILE*)
 * \brief Allocates and fills s_threshold_aq* attributes and links to the corresponding aquifer cell
 * \return void
 */
void AQ_define_threshold(s_cmsh *pcmsh, s_id_io *id_list, s_ft *pft, double stot, int kindof, FILE *flog) {

  s_id_io *list;
  s_ele_msh *pele;

  list = IO_browse_id(id_list, BEGINNING_IO);

  while (list != NULL) {

    pele = pcmsh->p_layer[list->id_lay]->p_ele[list->id];

    if (pele->phydro->p_thresh == NULL) {
      pele->phydro->p_thresh = AQ_create_p_threshold(kindof, pft, stot, flog);
    } else {
      if (pele->phydro->p_thresh[kindof] == NULL) { // Either threshold uptake or water table has already been declared.
        pele->phydro->p_thresh[kindof] = AQ_fill_threshold(kindof, pft, stot, flog);
      } else {
        LP_error(flog,
                 "In libaq%4.2f : In file %s, function %s at line %d : Forbidden setting : A %s input already defined to aquifer cell (layer = %d, id_int = %d). "
                 "Check the threshold input file.\n",
                 VERSION_AQ, __FILE__, __func__, __LINE__, AQ_threshold_type(kindof), list->id_lay, list->id);
      }
    }
    list = list->next;
  }
}