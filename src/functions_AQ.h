/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: functions_AQ.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// in manage_simul.c
s_simul_aq *AQ_init_simul();
s_carac_aq *AQ_create_carac();
void AQ_init_carac(s_carac_aq *);
void AQ_init_steady(s_carac_aq *);
void AQ_print_ele_transport_variable(s_carac_aq *, int, FILE *);

// in manage_hydro.c
void *AQ_create_hydro_tab(s_layer_msh *, FILE *);
s_transm_aq *AQ_create_transm();
s_hydro_aq *AQ_create_ele_hydro(int);
void AQ_create_face_transm(s_layer_msh *, FILE *);
void AQ_create_Z_conduct_face(s_layer_msh *, FILE *);
void AQ_create_XY_conduct_face(s_layer_msh *, s_chronos_CHR *, FILE *);
void AQ_calc_face_transm_layer(s_layer_msh *, FILE *);
void AQ_calc_face_cond_layer(s_param_aq *, s_layer_msh *, s_chronos_CHR *, FILE *);
void AQ_calc_faceXY_cond_layer(s_param_aq *, s_layer_msh *, s_chronos_CHR *, FILE *); // NF 13/9/2022
void AQ_calc_faceZ_cond_layer(s_param_aq *, s_layer_msh *, s_chronos_CHR *, FILE *);  // NF 13/9/2022
void AQ_finalize_interlayer_drainance(s_carac_aq *, s_chronos_CHR *, FILE *);         // NF 13/9/2022
double AQ_calc_Tmean(double, double, double, double, int, FILE *);
void AQ_calc_face_transm(s_face_msh *, int, int, FILE *);
void AQ_calc_transm(s_face_msh *, int, int, FILE *);
s_count_hydro_aq *AQ_create_count_hydro();
void AQ_define_elecount(s_id_io *, s_id_io *, FILE *);
void AQ_finalize_count_hydro(s_cmsh *, FILE *);
void AQ_print_eleactive(s_cmsh *, FILE *);
void AQ_define_conductZ(s_param_aq *, s_face_msh *, int, int, int, s_chronos_CHR *, FILE *);
void AQ_calc_conduct_faceZ(s_face_msh *, int, FILE *);
void AQ_define_conductXY(s_face_msh *, FILE *);
void AQ_calc_conduct_faceXY(s_face_msh *pf, FILE *fpout);
void AQ_calc_AUTO_COND_face(s_param_aq *, s_face_msh *, int, int, s_chronos_CHR *, FILE *);
void AQ_calc_TZ_face(s_face_msh *, int, int, FILE *);
s_cond_aq *AQ_create_cond();
void AQ_recalc_transm_unconfined(s_ele_msh *, FILE *);
void AQ_recalc_transm_unconfined_All(s_cmsh *, FILE *);
void AQ_set_transm_face(s_ele_msh *, double, double, double, double, FILE *); // NF,BL 19/10/2015 fonction de lecture des transmissivites aux faces
void AQ_calc_face_thick(s_face_msh *, int, FILE *);
void AQ_transport_attributes_allocation(s_hydro_aq *, int, FILE *);
void AQ_update_aq_system_hydraulic_state(s_cmsh *, FILE *);

// in manage_boundary.c
s_bound_aq *AQ_create_boundary(int);
double AQ_discharge_cauchy(s_face_msh *, s_ele_msh *, double, int, FILE *);
s_ele_msh *AQ_tab_boundaries_cst(s_ele_msh *, int, s_bound_aq *, int, FILE *);
s_ele_msh *AQ_tab_boundaries(s_ele_msh *, int, s_bound_aq *, s_id_io **, FILE *);
void AQ_define_bound(s_cmsh *, s_id_io *, s_ft *, int, int, double, double, double, double, FILE *);
int AQ_set_default_BC(s_ele_msh *, FILE *);
void AQ_print_elebound(s_cmsh *, FILE *);
s_bound_aq *AQ_define_bound_val(int, s_ft *, double, FILE *);
void AQ_define_elebound(s_id_io *, s_id_io **, int, FILE *);
void AQ_define_pdescr_cauchy(s_ele_msh *, double, double, double, FILE *);
void AQ_print_bound_val(s_ele_msh *, FILE *);
double AQ_calc_bound_val_neuman(s_ele_msh *, FILE *);
void AQ_set_qlim(s_cmsh *, s_id_io *, double, int, FILE *);
double AQ_calculate_cauchy_q(double, double, double, double);
void AQ_check_compatibility_DIRICHLET_initial_state(s_carac_aq *, FILE *);
void AQ_init_cauchy_qlim(s_carac_aq *, FILE *);

// in manage_flux.c
s_flux_aq *AQ_define_flux(s_flux_aq *, double);
s_flux_aq *AQ_create_flux();

// in manage_source.c
s_bound_aq ***AQ_create_source(FILE *);                                  // NG 04/03/2020 : Update with new ***psource format
void AQ_add_source_to_element(s_ele_msh *, int, s_bound_aq *, FILE *);   // NG 04/03/2020 : New function which adds a new pbound for a given kindof source type
void AQ_define_source(s_cmsh *, s_id_io *, s_ft *, int, double, FILE *); // NG 04/03/2020 : Update with new ***psource
void AQ_print_elesource(s_cmsh *, FILE *);                               // NG 04/03/2020 : Update with new ***psource
s_bound_aq *AQ_define_source_val(int, s_ft *, double, FILE *);
double *AQ_calc_source_value(int, s_bound_aq **, double, double, FILE *);

// in manage_mat.c
int AQ_calc_lmat5(s_cmsh *);
void AQ_init_mat_int(s_cmsh *, s_gc *, FILE *);
s_id_io *AQ_mat_neigh(s_ele_msh *);
void AQ_fill_mat5_neigh(s_cmsh *, s_gc *, s_id_io *, int, FILE *);

void AQ_calc_vector_b(s_carac_aq *, s_chronos_CHR *, double, FILE *);
double AQ_calc_b_bound(s_ele_msh *, s_cmsh *, int, double, double, double, int, FILE *);
double AQ_calc_b_active(s_ele_msh *, s_cmsh *, int, double, double, double, FILE *);
double AQ_calc_b_oneface(s_ele_msh *, s_cmsh *, double, double, double, int, int, FILE *);
double AQ_calc_b_i_cauchy(s_ele_msh *, double, double, double, int, int, FILE *);
double AQ_calc_b_i_neumann(s_face_msh *, double, double, double, int, FILE *);
double AQ_calc_b_i_dirichlet(s_ele_msh *, double, FILE *);
double AQ_calc_b_i_active(s_face_msh *, s_ele_msh *, double, double, double, int, int, FILE *);
void AQ_calc_b_i_source(s_ele_msh *, double, double, double, int, FILE *);
void AQ_calc_mat6(s_cmsh *, s_chronos_CHR *, s_param_aq *, s_gc *, double, FILE *);

s_ft *AQ_calc_mat6_ele_active(s_ele_msh *, s_cmsh *, int, double, double, double, FILE *);
s_ft *AQ_calc_mat6_dirichlet(s_ele_msh *, double, FILE *);
s_ft *AQ_calc_mat6_neumann(s_ele_msh *, s_cmsh *, double, double, double, int, FILE *);
s_ft *AQ_calc_mat6_cauchy(s_ele_msh *, s_cmsh *, double, double, double, int, FILE *);
s_ft *AQ_calc_mat6_ele_bound(s_ele_msh *, s_cmsh *, int, double, double, double, int, FILE *);
s_ft *AQ_calc_a_neigh_active(s_face_msh *, double, double, double, double, int, FILE *);
s_ft *AQ_calc_a_cauchy(s_face_msh *, double, double, double, int, FILE *);
double AQ_calc_aii_active(s_face_msh *, double, double, double, int, FILE *);

void AQ_def_RHS(double, int, s_gc *, FILE *);
void AQ_def_LHS(s_ft *, int, s_gc *, FILE *);
void AQ_reinit_b_source(s_carac_aq *, FILE *);
double AQ_calc_vector_b_bound(s_ele_msh *, s_cmsh *, int, double, double, double, int, FILE *);
void AQ_init_sol_vec(s_gc *);
void AQ_init_RHS(s_gc *);

void AQ_check_cauchy_top(double dt, double t, s_carac_aq *pchar_aq, FILE *fp);
void AQ_correct_LHS(s_gc *pgc, int id_mat6, double theta, double dt, s_face_msh *pface_aq, int sign);
void AQ_free_mat6(s_gc *); // NF, BL 22/10/2015 free memory allocated to mat6
void AQ_check_qlim_top(s_ele_msh *, s_gc *, double, double, double, FILE *);
void AQ_refresh_mat6_unconfined(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, FILE *fpout); // NF 26/11/2018

double AQ_calc_sizeweight_at_face(int idir, s_ele_msh *pele, s_ele_msh *pneigh, int nneigh, FILE *fpout);

// in manage_param.c
s_param_aq *AQ_create_param();
void AQ_set_uniform_layer_param_values(s_carac_aq *, int, int, double, FILE *); // NG : 10/08/2021
void AQ_print_conduct(s_carac_aq *, FILE *);
double AQ_calc_litho_thickness(s_hydro_aq *, FILE *);
int AQ_check_ztop_definition(s_layer_msh *, FILE *); // NG : 13/08/2021
double AQ_calc_T_unconfined(s_hydro_aq *, FILE *);
double AQ_calc_T_confined(s_hydro_aq *, FILE *);
double AQ_set_Sy_unconfined(s_hydro_aq *, FILE *);
void AQ_calc_param_unconfined(s_param_aq *, s_layer_msh *, FILE *);
void AQ_calc_param_unconfined_for_ele(s_param_aq *, s_ele_msh *, FILE *);

// in itos.c
char *AQ_name_BC(int);
char *AQ_name_source(int);
char *AQ_name_freq(int);
char *AQ_name_bc_mb(int);
char *AQ_name_modif(int);
char *AQ_name_attributs(int);
char *AQ_shortname_param(int);
char *AQ_name_regime(int);
char *AQ_name_out(int);
char *AQ_name_mass(int);
char *AQ_name_eletype(int);
char *AQ_name_T(int);
char *AQ_name_link_aq(int);         // NG : 05/03/2020
char *AQ_name_source_origin(int);   // NG : 07/03/2020
char *AQ_name_outvar(int);          // NG : 05/02/2021
char *AQ_derivation_type_name(int); // NG : 20/04/2021
char *AQ_name_layer_type(int);      // NG : 10/08/2021
char *AQ_threshold_type(int);       // NG : 29/02/2024
char *AQ_cell_hydraulic_state(int);

// in solve_aq.c
double AQ_solve_diffusiv_steady_one_it(s_carac_aq *, s_chronos_CHR *, s_clock_CHR *, s_out_io **, int, int *, FILE *); // NG : 09/06/2021 : counter of steady iterations added
void AQ_solve_diffusiv_steady(s_carac_aq *, s_chronos_CHR *, s_clock_CHR *, s_out_io **, int, FILE *);
void AQ_refresh_state_variable(s_carac_aq *, s_chronos_CHR *, s_clock_CHR *, s_out_io **, FILE *);
double AQ_do_one_pic_it(int *, double, double, s_carac_aq *, s_chronos_CHR *, s_clock_CHR *, int, int *, FILE *); // NG : 09/06/2021 : counter of steady iterations added
double AQ_calc_eps(s_gc *, s_cmsh *, FILE *);
double AQ_solve_diffusiv_pic(s_carac_aq *, s_chronos_CHR *, s_clock_CHR *, int, int *, FILE *); // NG : 09/06/2021 : counter of steady iterations added
double AQ_calc_pic(s_carac_aq *, FILE *);
double AQ_calc_drawd(s_carac_aq *, FILE *);
void AQ_refresh_H_interpol(s_carac_aq *, s_chronos_CHR *, s_out_io *, FILE *);

// in manage_mb.c
s_mb_aq *AQ_create_mb();
void AQ_calc_mb_face(s_cmsh *, s_ele_msh *, double, double, double, int, FILE *);
void AQ_calc_stock_dirichlet(s_ele_msh *, int, FILE *);
double AQ_calc_mb_dirichlet(s_cmsh *, s_ele_msh *, double, double, FILE *);
double AQ_calc_mb_cauchy(s_face_msh *, s_ele_msh *, double, double, double, FILE *);
void AQ_calc_mb(s_ele_msh *, int, FILE *);
void AQ_calc_fluxes(s_ele_msh *, double, FILE *);
void AQ_calc_output_interpol(s_chronos_CHR *, s_carac_aq *, s_out_io *, int, FILE *);
void AQ_reinit_mb_face_All(s_carac_aq *, FILE *);
void AQ_reinit_mb_face(s_mb_aq *, FILE *);
void AQ_calc_mb_face_All(s_carac_aq *, s_chronos_CHR *, s_out_io **, FILE *);
void AQ_calc_mb_in_model(s_id_io *, s_cmsh *, s_ele_msh *, int, int, double, double, double, int, int, FILE *);
int AQ_calc_mb_border_cell(s_cmsh *, s_ele_msh *, int, int, int, double, double, double, FILE *);
void AQ_fill_state(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_out_io **pout_all, int timer, FILE *fpout);
void AQ_calc_mb_end(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_out_io **pout_all, FILE *fpout);

// in manage_output.c
double AQ_get_conductance_at_faceZTWO(s_face_msh *);
void AQ_print_H(s_cmsh *pcmsh, int itype, FILE *fpout);
void AQ_count_nb_out(s_cmsh *, s_out_io *, FILE *);
void AQ_write_H_for(double, double **, s_ele_msh *, FILE *, FILE *);
void AQ_write_MASS_for(double, double **, s_ele_msh *, FILE *, FILE *);
void AQ_write_FLUX_for(double, double **, s_ele_msh *, FILE *, FILE *);
double **AQ_get_values_H(double, s_out_io *, s_carac_aq *, int, int, FILE *);
void AQ_write_H(double, double, s_carac_aq *, s_out_io *, int, FILE *);
void AQ_print_hydhead_only(s_chronos_CHR *, double, s_carac_aq *, s_out_io *, int, FILE *);
double **AQ_get_values_MASS(s_out_io *, s_carac_aq *, int, int, FILE *);
double AQ_get_BC_mb(s_ele_msh *, int);
void AQ_write_MASS(double, s_carac_aq *, s_out_io *, int, FILE *);
void AQ_fill_threshold_output(s_chronos_CHR *, double, s_carac_aq *, s_out_io *, int, FILE *); // NG : 29/02/2024
double **AQ_get_values_FLUXES(s_out_io *, s_carac_aq *, int, int, FILE *);
void AQ_write_FLUXES(double, s_carac_aq *, s_out_io *, int, FILE *);
void AQ_print_for(double, s_cmsh *, double **, s_out_io *, int, FILE *, FILE *);
void AQ_print_output(s_chronos_CHR *, double, s_carac_aq *, s_out_io *, int, FILE *);
void AQ_print_outputs_mod(s_chronos_CHR *, s_carac_aq *, s_out_io *, int, double, FILE *);
void AQ_print_header(FILE *, int, FILE *); // NG : 29/02/2024 : Updated to deal with all three AQ output types
void AQ_print_corresp(s_cmsh *, int, FILE *);
void AQ_print_abstract(s_carac_aq *, FILE *);
void AQ_print_final_state(s_carac_aq *, FILE *);
void AQ_log_print_layer_header(FILE *);                  // NG : 29/01/2021
void AQ_print_param_overview(s_carac_aq *, int, FILE *); // NG : 11/08/2021
void AQ_print_subsurface_lithos(s_cmsh *, FILE *);       // NG : 30/08/2021
double AQ_get_conductance_at_faceZTWO(s_face_msh *);     // NF 23/1/2024 missing prototype added

// in manage_eletype.c
void AQ_count_eletype(s_cmsh *, FILE *);
s_id_io ***AQ_create_eletype(int);
void AQ_tab_eletype_all(s_cmsh *, FILE *);
s_id_io **AQ_tab_eletype(s_id_io *, int, int, FILE *);

// in manage_link.c
void AQ_init_link_ele(s_ele_msh *, int, double, int, FILE *);
void AQ_print_links_ele(s_carac_aq *, int, FILE *); // NG : 05/03/2020 : New function to print AQ links with other modules (Surf, Hderm, Nsat)

// in coupling_surf.c
void AQ_define_recharge_coupled(s_cmsh *, s_id_io *, int, FILE *); // NG : 07/03/2020 : Accounts for multiple source for a given kindof type. Prototype modified to include tracking of the flux origin if coupled with several modules.
void AQ_define_cauchy_coupled(s_carac_aq *, s_ele_msh *, double, double, double, double, FILE *);

// in print_mat.c
void AQ_print_LHS_RHS(s_cmsh *pcmsh, s_gc *pgc, FILE *fpout);
void AQ_print_LHS(s_gc *pgc, FILE *fpout);
void AQ_print_RHS(s_gc *pgc, FILE *fpout);
void AQ_print_mat_int(s_gc *pgc, FILE *fpout);

// in manage_picart.c
s_picart_aq *AQ_init_picart(void);
void AQ_optimise_pic(s_cmsh *pcmsh, s_picart_aq *ppic, int isim_type, FILE *flog);
void AQ_reinit_pic(s_picart_aq *ppic);

// in manage_litho.c
s_litho_aq *AQ_create_litho();
s_litho_aq *AQ_fill_litho(char *name, int id_gis, int id);
s_litho_aq *AQ_browse_litho(s_litho_aq *plitho, int iwhere);
s_litho_aq *AQ_chain_fwd_litho(s_litho_aq *pd1, s_litho_aq *pd2);
s_litho_aq *AQ_copy_litho(s_litho_aq *plitho, FILE *flog);
s_litho_aq *AQ_create_litho_sequence_in_ele(s_litho_aq *pl, FILE *flog);
void *AQ_create_litho_for_each_ele_in_layer(s_layer_msh *player, FILE *flog);
void AQ_find_subsurface_litho(s_layer_msh *, int, FILE *); // NG : 30/08/2021

// in manage_derivation.c  // NG : 20/04/2021
s_derivation_aq *AQ_create_derivation(int);
void AQ_init_derivation_links(s_ele_msh *, s_ele_msh *, double, int, s_ft *, FILE *);
void AQ_print_all_derivation_links(s_carac_aq *, int, FILE *);
void AQ_check_derivation_setup(s_carac_aq *, int, FILE *);
void AQ_define_source_derivation_all(s_carac_aq *, int, int, FILE *);
void AQ_transfer_derivation_discharge(double, double, s_carac_aq *, int, int, FILE *);

// in manage_threshold.c  // NG : 29/02/2024
s_threshold_aq *AQ_fill_threshold(int, s_ft *, double, FILE *);
s_threshold_aq **AQ_create_p_threshold(int, s_ft *, double, FILE *);
void AQ_define_threshold(s_cmsh *, s_id_io *, s_ft *, double, int, FILE *);