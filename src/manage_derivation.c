/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_derivation.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

/**
 * \brief Creates a new derivation structur
 * \return Initialized pointer towards s_derivation_aq structur
 */
s_derivation_aq *AQ_create_derivation(int kindof) {
  int i;
  s_derivation_aq *pderiv = NULL;

  pderiv = new_derivation_aq();
  bzero((char *)pderiv, sizeof(s_derivation_aq));
  pderiv->type = kindof;

  // Default initialization
  pderiv->link = (s_id_spa **)malloc(NDERIV_TYPE * sizeof(s_id_spa *));
  for (i = 0; i < NDERIV_TYPE; i++)
    pderiv->link[i] = NULL;
  pderiv->pft = NULL;

  return pderiv;
}

/**
 * \brief Creates a derivation and also sets the link between an AQ-receiving cell and a AQ-emitting cell
 * \return None
 */
void AQ_init_derivation_links(s_ele_msh *pelefrom, s_ele_msh *peleto, double coef, int deriv_type, s_ft *pft, FILE *flog) {
  s_ft *pftmp;
  s_id_spa *link_tmp;

  pelefrom->phydro->emitting_status = YES_TS;

  /* NG : On est ici forcé d'affecter les infos de dérivation (i.e. link) à la cellule émetrice.
     En effet, on ne peut pas lier ça à la cellule réceptrice car son phydro peut ne pas être ouvert à ce stade
 (ex : si la réceptrice appartient à une couche inférieure) */

  if (pelefrom->phydro->pderivation == NULL) {
    pelefrom->phydro->pderivation = AQ_create_derivation(deriv_type);
    pelefrom->phydro->pderivation->pft = pft;
  }

  link_tmp = SPA_create_ids(peleto->id[ABS_MSH], coef);
  pelefrom->phydro->pderivation->link[deriv_type] = SPA_secured_chain_id_fwd(pelefrom->phydro->pderivation->link[deriv_type], link_tmp);
}

/**
 * \brief Verification function : prints all derivation links for a given type
 * \return None
 */
void AQ_print_all_derivation_links(s_carac_aq *pcarac_aq, int deriv_type, FILE *fp) {
  s_cmsh *pcmsh = pcarac_aq->pcmsh;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele;
  int i, j, nlayer, nele;
  s_id_spa *link;

  nlayer = pcmsh->pcount->nlayer;

  LP_printf(fp, "Derivation type : %s :\n", AQ_derivation_type_name(deriv_type));
  for (i = 0; i < nlayer; i++) {
    player = pcmsh->p_layer[i];
    nele = player->nele;
    p_ele = player->p_ele;
    for (j = 0; j < nele; j++) {
      pele = p_ele[j];
      if (pele->phydro->pderivation != NULL) {
        link = pele->phydro->pderivation->link[deriv_type];
        if (link != NULL) {
          LP_printf(fp, "Emitting AQ-cell Intern IDs (%d,%d) to AQ-cell :\n", pele->id[INTERN_MSH], player->id);
          SPA_print_ids(link, fp);
          if (pele->phydro->pderivation->pft != NULL) {
            LP_printf(fp, "Associated time serie :\n");
            TS_print_ts(pele->phydro->pderivation->pft, fp); // ->pft remains unused in case of a cascade-type link.
            LP_printf(fp, "\n");
          }
        }
      }
    }
  }
}

// NG : 21/04/2021
/**
 * \brief Verification function.
        (i) Looks for discrepencies between derivations and Cauchy BC setups.
                The definition of a emitting derivation cell requires the attributes of a Cauchy BCs (Conductance, Drainage level, etc.)
                Therefore, input consistency check is required.
         (ii) Checks that no AQ emitting derivation cell is both emitter and receiver (dummy configuration).
 * \return void
 */
void AQ_check_derivation_setup(s_carac_aq *pcarac_aq, int deriv_type, FILE *flog) {
  int i, j;
  s_cmsh *pcmsh = pcarac_aq->pcmsh;
  s_derivation_aq *pderiv;
  s_layer_msh *play;
  s_ele_msh *pele, *pele_to;
  s_id_spa *link_tmp;

  for (i = 0; i < pcmsh->pcount->nlayer; i++) {
    play = pcmsh->p_layer[i];
    for (j = 0; j < play->nele; j++) {
      pele = play->p_ele[j];
      pderiv = pele->phydro->pderivation;
      if (pderiv != NULL) {
        if (pderiv->type == deriv_type) {
          link_tmp = SPA_browse_id(pele->phydro->pderivation->link[deriv_type], BEGINNING_SPA, flog);
          while (link_tmp != NULL) {
            if (link_tmp->id == pele->id[ABS_MSH]) {
              LP_error(flog, "In libaq%4.2f : Error in file %s, in function %s : Derivation %s cell INT ID (%d,%d) cant be both emitter and receiver. Check your cascade input file.\n", VERSION_AQ, __FILE__, __func__, AQ_derivation_type_name(deriv_type), pele->id[INTERN_MSH], pele->player->id);
            }
            link_tmp = link_tmp->next;
          }

          if ((pele->type != BOUND) || ((pele->type == BOUND) && (pele->phydro->pbound == NULL)) || ((pele->type == BOUND) && (pele->phydro->pbound->type != CAUCHY))) {
            LP_error(flog, "libaq%4.2f : Error in file %s, at line %d in function %s : missing Cauchy boundary condition on cascade AQ-cell id (%d,%d).\n", VERSION_AQ, __FILE__, __LINE__, __func__, pele->id[INTERN_MSH], pele->player->id);
          }
        }
      }
    }
  }
}

/**
 * \brief Opens sources pointers for Cauchy discharge derivation destination cells
 * \return void
 */
void AQ_define_source_derivation_all(s_carac_aq *pcarac_aq, int link_type, int origin_type, FILE *fp) {
  int i, j, k, nele, nsource, found;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele, *pele_to;
  s_cmsh *pcmsh = pcarac_aq->pcmsh;
  s_id_spa *id_tmp = NULL;
  s_id_io *pid = NULL;

  for (i = 0; i < pcmsh->pcount->nlayer; i++) {
    player = pcmsh->p_layer[i];
    nele = player->nele;
    p_ele = player->p_ele;
    for (j = 0; j < nele; j++) {
      pele = p_ele[j];
      if (pele->phydro->pderivation != NULL) {
        if (pele->phydro->pderivation->link[link_type] != NULL) {
          id_tmp = SPA_browse_id(pele->phydro->pderivation->link[link_type], BEGINNING_SPA, fp);
          while (id_tmp != NULL) {
            pele_to = MSH_get_ele_from_abs(pcmsh, id_tmp->id, fp);
            pid = IO_create_id(pele_to->player->id - 1, pele_to->id[INTERN_MSH]);

            /* In case of multiple cells emitting towards a same one (i.e. in case of BYPASS), we're making sure that
               a dedicated source pointer does not already exist. If so, no need to reopen a new one. */
            nsource = pele_to->phydro->nsource_type[RECHARGE_AQ];
            found = NO_TS;

            for (k = 0; k < nsource; k++) {
              if (pele_to->phydro->psource[RECHARGE_AQ][k]->origin == origin_type) {
                found = YES_TS;
                break;
              }
            }
            if (found == NO_TS)
              AQ_define_recharge_coupled(pcmsh, pid, origin_type, fp);
            free(pid);

            // Check
            // LP_printf(fp,"ele (%d,%d) : nsource: %d\n",pele_to->id[INTERN_MSH],pele_to->player->id,pele_to->phydro->nsource_type[RECHARGE_AQ]);
            // for (k=0;k<nsource;k++)
            //    LP_printf(fp,"compteur %d : origin %s\n",k+1,AQ_name_source_origin(pele_to->phydro->psource[RECHARGE_AQ][k]->origin));

            id_tmp = id_tmp->next;
          }
        }
      }
    }
  }
}

// NG : 20/04/2021
/**
 * \brief At each time step, and for all derivation-AQ links declared within the aquifer system (ie. cascade, bypass type links),
         transfers simulated Cauchy discharges to respective receiving cells,
         as an additional recharge discharge (i.e. source input).
 * \return void
 */
void AQ_transfer_derivation_discharge(double t, double dt, s_carac_aq *pchar_aq, int derivation_type, int origin_type, FILE *fp) {
  int i, j, k, nele, nb_bound, pos_pbound, nb_derivation;
  double qderiv, qderivtot, ft_old, t_old, qcauchy, qexport, prct;
  s_id_io *id_list;
  s_cmsh *pcmsh = pchar_aq->pcmsh;
  s_ele_msh *pele, **p_ele, *pele_from;
  s_layer_msh *player;
  s_id_spa *link;
  s_ft *pft;

  nb_derivation = 0;
  qderivtot = 0.;

  id_list = pchar_aq->pcmsh->pcount->pcount_hydro->elesource[RECHARGE_AQ];
  id_list = IO_browse_id(id_list, BEGINNING_IO);

  while (id_list != NULL) {
    pele = pchar_aq->pcmsh->p_layer[id_list->id_lay]->p_ele[id_list->id];

    qderiv = 0.;
    qexport = 0.;
    prct = 0.;
    nb_bound = pele->phydro->nsource_type[RECHARGE_AQ]; // Nombre total de recharge inputs déclarés sur la maille, tous types confondus.
    pos_pbound = CODE_AQ;                               // Position de la pbound dans le tableau **psource[RECHARGE_AQ]

    // Balayage des pointeurs pbound pour trouver les mailles réceptrices
    for (k = 0; k < nb_bound; k++) {
      if (pele->phydro->psource[RECHARGE_AQ][k]->origin == origin_type) {
        pos_pbound = k;
        break;
      }
    }

    if (pos_pbound != CODE_AQ) {
      //	LP_printf(fp,"Searching discharge for receiving cell (%d,%d) - Derivation type : %s.\n",pele->id[INTERN_MSH],pele->player->id,AQ_derivation_type_name(derivation_type)); // NG check
      for (i = 0; i < pcmsh->pcount->nlayer; i++) {
        player = pcmsh->p_layer[i];
        nele = player->nele;
        p_ele = player->p_ele;
        for (j = 0; j < nele; j++) {
          pele_from = p_ele[j];
          if (pele_from->phydro->pderivation != NULL) {
            if (pele_from->phydro->pderivation->link[derivation_type] != NULL) {
              link = SPA_browse_id(pele_from->phydro->pderivation->link[derivation_type], BEGINNING_SPA, fp);
              while (link != NULL) {
                if (link->id == pele->id[ABS_MSH]) // la maille envoie effectivement vers la maille ou l'on est et selon le type de dérivation qui nous intéresse
                {
                  //   LP_printf(fp,"Ready to send Cauchy discharge from INT ids (%d,%d) to INT ids (%d,%d) on derivation %s type.\n",pele_from->id[INTERN_MSH],pele_from->player->id,pele->id[INTERN_MSH],pele->player->id,AQ_derivation_type_name(derivation_type));  // NG check
                  nb_derivation++;
                  qcauchy = 0.;

                  qcauchy = pele_from->phydro->pmb->pbound[CAUCHY][END_AQ];
                  if (qcauchy < 0) {
                    qcauchy = (-1.) * qcauchy; // Passage en >0 car flux ré-entrant dans le système souterrain.
                    if (qcauchy < 0.) {
                      LP_warning(fp, "libaq%4.2f : In function %s : NEGATIVE recharge rate due to derivation type %s from cell ABS ID %d.\n", VERSION_AQ, __func__, AQ_derivation_type_name(derivation_type), pele->id[ABS_MSH]);
                    }
                  }

                  qderiv += qcauchy;

                  if (derivation_type == BYPASS_AQ) {
                    qexport = TS_interpolate_ts(t, pele_from->phydro->pderivation->pft); // q exporté (rentré positif) du bassin en m3/s             Ca posera problème si jamais on a des ts différentes pour les bypass dans un cas de plusieurs mailles vers une même maille...
                    prct = link->prct;                                                   // Seule les dernières valeurs de qexport et de prct seront considérées.
                  }
                }
                link = link->next;
              }
            }
          }
        }
      }

      if (derivation_type == BYPASS_AQ) {
        qderiv = TS_max(qderiv - qexport, 0.);
        qderiv *= prct;
      }

      qderivtot += qderiv; // For display info (m3/s)

      // Affectation du flux à la maille réceptrice
      qderiv = qderiv / pele->pdescr->surf; // Passage en m/s

      ft_old = pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft->ft;
      pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft = TS_free_ts(pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft, fp);
      t_old = t - dt;
      pft = TS_create_function(t, -qderiv);
      pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft = TS_create_function(t_old, ft_old);
      pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft = TS_secured_chain_fwd_ts(pele->phydro->psource[RECHARGE_AQ][pos_pbound]->pft, pft);
      pele->phydro->psource[RECHARGE_AQ][pos_pbound]->freq = VAR;
    }
    id_list = id_list->next;
  }
  LP_printf(fp, "\t --> Number of aquifer derivation links (%s) : %d. Total return discharge to aquifer : %f m3/s\n", AQ_derivation_type_name(derivation_type), nb_derivation, qderivtot);
}