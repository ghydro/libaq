/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: coupling_surf.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

/* NG : 07/03/2020 : Updated function to account for new ***psource pointer format
and tracking of the recharge fluxes in case of multiple couplings with other modules */
void AQ_define_recharge_coupled(s_cmsh *pcmsh, s_id_io *id_list, int source_origin, FILE *fpout) {
  s_layer_msh *player;
  int id, n = 0;
  int nlayer;
  s_ele_msh *pele;
  s_ft *values;
  s_id_io *ptmp;
  s_count_hydro_aq *pcount;
  s_bound_aq *pbound;
  ptmp = IO_browse_id(id_list, BEGINNING_IO);

  pcount = pcmsh->pcount->pcount_hydro;

  if (ptmp != NULL)
    AQ_define_elebound(ptmp, pcount->elesource, RECHARGE_AQ, fpout);
  else
    LP_error(fpout, "The list of id is NULL !!\n");

  while (ptmp != NULL) {
    id = ptmp->id;
    nlayer = ptmp->id_lay;
    player = pcmsh->p_layer[nlayer];
    pele = player->p_ele[id];

    if (pele->phydro->psource == NULL)
      pele->phydro->psource = AQ_create_source(fpout); // NG : 04/03/2020

    values = TS_create_function(0., 0.);
    pbound = AQ_create_boundary(RECHARGE_AQ);
    pbound->pft = values;
    pbound->freq = CST;
    pbound->origin = source_origin; // NG : 07/03/2020

    AQ_add_source_to_element(pele, RECHARGE_AQ, pbound, fpout); // NG : 04/03/2020

    ptmp = ptmp->next;
  }
}

void AQ_define_cauchy_coupled(s_carac_aq *pchar_aq, s_ele_msh *pele, double Z_max, double surf_riv, double ep_riv_bed, double TZ_riv_bed, FILE *fpout) {
  s_layer_msh *player;
  s_cmsh *pcmsh;
  int id, n = 0;
  int nlayer;
  double h_ini;
  s_id_io *id_list;
  s_count_hydro_aq *pcount;
  s_bound_aq *pbound;
  s_face_msh *pface;

  if (pele->phydro->pbound == NULL) {
    pcmsh = pchar_aq->pcmsh;
    id_list = IO_create_id((pele->player->id - 1), pele->id[INTERN_MSH]);
    pbound = AQ_create_boundary(CAUCHY);
    pbound->pft = TS_create_function(0., Z_max);
    pbound->freq = CST;
    pcount = pcmsh->pcount->pcount_hydro;
    AQ_define_elebound(id_list, pcount->elebound, pbound->type, fpout);
    IO_free_id_serie(id_list, fpout);
    pele = AQ_tab_boundaries(pele, CODE_TS, pbound, pcount->elebound, fpout);
    if (pele->phydro->pcond == NULL && fabs(TZ_riv_bed - CODE_TS) < EPS) {
      LP_error(fpout, "libaq%4.2f %s in %s l%d : No conductance is defined  for surface or river cell INTERN_ID : %d ABS_ID : %d GIS_ID : %d check your input files AQ_SURF or ELE_MUSK !! \n", VERSION_AQ, __func__, __FILE__, __LINE__, pele->id[INTERN_MSH], pele->id[ABS_MSH], pele->id[GIS_MSH]);

    } else if (fabs(TZ_riv_bed - CODE_TS) > EPS) {
      AQ_define_pdescr_cauchy(pele, surf_riv, ep_riv_bed, TZ_riv_bed, fpout);
    }
  }
}
