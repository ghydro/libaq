/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_mb.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"
#ifdef OMP
#include "omp.h"
#endif

/**\fn s_mb_aq *AQ_create_mb(int nmass_type,int ntime)
 *\brief create mass balance structur
 *\return s_mb_aq*
 *
 */
s_mb_aq *AQ_create_mb() {
  s_mb_aq *pmb;
  int i, ntime, idir, itype;

  pmb = new_mb_aq();
  bzero((char *)pmb, sizeof(s_mb_aq));

  for (ntime = 0; ntime < NTIME_AQ; ntime++)
    pmb->mass[ntime] = 0.;

  pmb->flux = (double ***)malloc(ND_MSH * sizeof(double **));
  bzero((char *)pmb->flux, ND_MSH * sizeof(double **));
  for (idir = 0; idir < ND_MSH; idir++) {
    pmb->flux[idir] = (double **)malloc(NFACE_MSH * sizeof(double *));
    bzero((char *)pmb->flux[idir], NFACE_MSH * sizeof(double *));
    for (itype = 0; itype < NFACE_MSH; itype++) {
      pmb->flux[idir][itype] = (double *)malloc(NTIME_AQ * sizeof(double));
      bzero((char *)pmb->flux[idir][itype], NTIME_AQ * sizeof(double));
    }
  }

  for (i = 0; i < NBOUND; i++)
    for (ntime = 0; ntime < NTIME_AQ; ntime++)
      pmb->pbound[i][ntime] = 0.;

  for (i = 0; i < NSOURCE; i++)
    for (ntime = 0; ntime < NTIME_AQ; ntime++)
      pmb->source[i][ntime] = 0.;

  pmb->error = 0;

  return pmb;
}

/**\fn void AQ_fill_mb_ini_end(s_carac_aq *pchar_aq,s_chronos_CHR *chronos,s_out_io **pout_all,FILE *fpout)
 *\brief function to fill mass balance structure at the beginning and at the end of a time step
 *\return void
 *
 */
/*void AQ_fill_mb_ini_end(s_carac_aq *pchar_aq,s_chronos_CHR *chronos,s_out_io **pout_all,FILE *fpout)
{
  int nlayer,nele,k,j,i;
  s_layer_msh *player;
  s_ele_msh *pele,**p_ele;
  s_mb_aq *pmb;
  s_bound_aq *psource;
  s_ft *pft;
  s_out_io *pout;
  double S_1,S_0,flux;
  double deltat,a0,theta,sum_flux_ini,sum_flux_end;
  double t_out,dt,t;
  int n_neigh,idir,itype;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface;
  s_cmsh *pcmsh;

  pcmsh=pchar_aq->pcmsh;
  a0=pchar_aq->settings->a0;
  theta=pchar_aq->settings->general_param[THETA_AQ];

  if(pout_all[MASS_IO]!=NULL)
    {
      pout=pout_all[MASS_IO];
      t=chronos->t[CUR_CHR];
      dt=chronos->dt;
      t_out=pout->t_out[CUR_IO];

      nlayer=pcmsh->pcount->nlayer;

      for(k=0;k<nlayer;k++)
        {
          player=pcmsh->p_layer[k];
          nele=player->nele;
          p_ele=player->p_ele;

          for(j=0;j<nele;j++)
            {
              pele=p_ele[j];

              pele->phydro->pmb->error=0.;
              sum_flux_ini=0.;
              sum_flux_end=0.;

              pele->phydro->pmb->mass[INI_AQ]=pele->phydro->h[ITER];
              pele->phydro->pmb->mass[END_AQ]=pele->phydro->h[T];
              pele->phydro->pmb->mass[DMASS_AQ]=a0*pele->pdescr->surf*pele->phydro->S[SCUR]*(pele->phydro->pmb->mass[END_AQ]-pele->phydro->pmb->mass[INI_AQ])/dt;//NF 26/11/2018 There will be a problem for changes between CONFINED/UNCONFINED DURING A TIME STEP WE SHOULD SPLIT THIS FUNCTION WITH A CALL AT THE BEGINNING OF THE CALCULATION TO HAVE THE PROPER SCUR AND ANOTHER AT THE END. SPLIT IN
mb_calculate_state, and mb_calculate_mb

              for (i=0;i<NSOURCE;i++){
                if (pele->phydro->psource != NULL){
                  psource=pele->phydro->psource[i];
                  if (psource != NULL){
                    if(psource->freq==CST)
                      {
                        pele->phydro->pmb->source[i][INI_AQ]=psource->pft->ft*pele->pdescr->surf;//Remind : psource->pft->ft is in m/s
                        pele->phydro->pmb->source[i][END_AQ]=pele->phydro->pmb->source[i][INI_AQ];
                        pele->phydro->pmb->source[i][DMASS_AQ]=pele->phydro->pmb->source[i][INI_AQ];
                      }
                    else
                      {
                        pft=TS_browse_ft(psource->pft,BEGINNING_TS);
                        S_0=TS_interpolate_ts(t-dt,pft);
                        pft=TS_browse_ft(psource->pft,BEGINNING_TS);
                        S_1=TS_interpolate_ts(t,pft);

                        pele->phydro->pmb->source[i][INI_AQ]=S_0*pele->pdescr->surf;
                        pele->phydro->pmb->source[i][END_AQ]=S_1*pele->pdescr->surf;
                        pele->phydro->pmb->source[i][DMASS_AQ]=theta*pele->phydro->pmb->source[i][END_AQ]+(1-theta)*pele->phydro->pmb->source[i][INI_AQ];
                      }
                    pele->phydro->pmb->error-=pele->phydro->pmb->source[i][DMASS_AQ];//Sign - because uptake is positive and recharge negative
                    sum_flux_ini-=pele->phydro->pmb->source[i][INI_AQ];
                    sum_flux_end-=pele->phydro->pmb->source[i][END_AQ];
                  }
                }
                //else ca reste a 0 comme initialise
              }

              //Filling discharge at faces (m3/s) along x, y and z.
              for(idir=0;idir<ND_MSH;idir++)
                {
                  for(itype=0;itype<NFACE_MSH;itype++)
                    {
                      pele->phydro->pmb->flux[idir][itype][INI_AQ]=0.;
                      pele->phydro->pmb->flux[idir][itype][END_AQ]=0.;
                      neigh=pele->p_neigh[idir][itype];
                      if(neigh!=NULL)//si il y a des voisins
                        {
                          neigh=IO_browse_id(neigh,BEGINNING_TS);
                          n_neigh= IO_length_id(neigh);
                          while(neigh!=NULL)
                            {
                              pneigh=MSH_get_ele(pcmsh,neigh->id_lay,neigh->id,fpout);
                              pface=MSH_return_face(pele,pneigh,idir,itype,fpout);

                              flux=pface->phydro->pcond->conductance*(pneigh->phydro->h[ITER]-pele->phydro->h[ITER]);
                              pele->phydro->pmb->flux[idir][itype][INI_AQ]+=flux;
                              sum_flux_ini+=flux;

                              flux=pface->phydro->pcond->conductance*(pneigh->phydro->h[T]-pele->phydro->h[T]);
                              pele->phydro->pmb->flux[idir][itype][END_AQ]+=flux;
                              sum_flux_end+=flux;

                              neigh=neigh->next;
                            }
                        }
                      else//si pas de voisins
                        {
                          if (pele->type==BOUND)//si la maille est BOUND
                            {
                              if (idir==Z_MSH && itype==TWO && pele->phydro->pbound->type==CAUCHY)
                                {
                                  pface=pele->face[idir][itype];
                                  if (pface->loc==TOP_MSH){//FB 12/04/2018 RIV_MSH cells are managed by cawaqs for convergence issue with Picard iterations (the value of the Cauchy discharge is calculated in CAW_cAQ_check_qlim_riv for RIV_MSH faces). Libaq knows TOP_MSH cells only.
                                    pele->phydro->pmb->pbound[CAUCHY][INI_AQ]=AQ_discharge_cauchy(pface,pele,t-dt,ITER,fpout);
                                    pele->phydro->pmb->pbound[CAUCHY][END_AQ]=AQ_discharge_cauchy(pface,pele,t,T,fpout);
                                  }
                                }
                              else if (pele->phydro->pbound->type==NEUMANN)
                                {
                                  if (pele->phydro->pbound->pft!=0)
                                    printf("Neumann pas encore traite dans MB. Idabs=%d dir=%d type=%d\n",pele->id[ABS_MSH],idir,itype);
                                }
                              else
                                {
                                  pele->phydro->pmb->flux[idir][itype][INI_AQ]=0.;
                                  pele->phydro->pmb->flux[idir][itype][END_AQ]=0.;
                                }
                            }
                          else
                            {
                              pele->phydro->pmb->flux[idir][itype][INI_AQ]=0.;
                              pele->phydro->pmb->flux[idir][itype][END_AQ]=0.;
                            }

                        }
                      pele->phydro->pmb->flux[idir][itype][DMASS_AQ]=theta*pele->phydro->pmb->flux[idir][itype][END_AQ]+(1-theta)*pele->phydro->pmb->flux[idir][itype][INI_AQ];
                      pele->phydro->pmb->error+=pele->phydro->pmb->flux[idir][itype][DMASS_AQ];
                    }
                }

                if (pele->type==BOUND)
                  if (pele->phydro->pbound->type==DIRICHLET)
                    {
                      pele->phydro->pmb->pbound[DIRICHLET][INI_AQ]=-sum_flux_ini;
                      pele->phydro->pmb->pbound[DIRICHLET][END_AQ]=-sum_flux_end;
                    }

                for (i=0;i<NBOUND;i++)
                  {
                    if (i==CAUCHY){
                      pele->phydro->pmb->pbound[i][DMASS_AQ]=pele->phydro->pmb->pbound[i][END_AQ];
        }
                    else
                      pele->phydro->pmb->pbound[i][DMASS_AQ]=theta*pele->phydro->pmb->pbound[i][END_AQ]+(1-theta)*pele->phydro->pmb->pbound[i][INI_AQ];
                    pele->phydro->pmb->error+=pele->phydro->pmb->pbound[i][DMASS_AQ];
                  }

              pele->phydro->pmb->error-=pele->phydro->pmb->mass[DMASS_AQ];
            }
        }
    }
    }*/

/**\fn void AQ_fill_state(s_carac_aq *pchar_aq,s_chronos_CHR *chronos,s_out_io **pout_all,int timer,FILE *fpout)
 *\brief function to fill the state of the mass balance structures at the timer (INI_AQ or END_AQ) of a time step
 *\return void
 *
 */
// NG : 04/03/2020 : Updated to integrate new ***psource with potential multiple source time series for one element
void AQ_fill_state(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_out_io **pout_all, int timer, FILE *fpout) {
  int nlayer, nele, k, j, i, l;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele;
  s_mb_aq *pmb;
  //  s_bound_aq *psource;
  s_bound_aq **psource; // NG : 04/03/2020
  s_ft *pft;
  s_out_io *pout;
  double S_1, S_0, flux;
  double ft, l_side;
  double deltat, a0, theta, sum;
  double t_out, dt, t;
  int n_neigh, idir, itype;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface;
  s_cmsh *pcmsh;
  int state;
  int nb_pbound_source; // NG : 04/03/2020
  double som_source;    // NG : 04/03/2020

  pcmsh = pchar_aq->pcmsh;
  theta = pchar_aq->settings->general_param[THETA_AQ];

  state = T; // NF 29/11/2018 it was necessary to distinguish between the two when mass balance was calculated only for CONFINED after the call to AQ_refresh_state_variable. Now that we have a call to AQ_fill_state before and after a time step, it can generate miscalculation, even though at the beginning of a time step h[ITER]=h[T] (consequence of refresh)
  if (timer == INI_AQ) {
    t -= dt;
    // state=ITER;
  }
  // else//ie timer == END_AQ
  //  state=T;

  if (pout_all[MASS_IO] != NULL) {
    pout = pout_all[MASS_IO];
    t = chronos->t[CUR_CHR];
    dt = chronos->dt;
    t_out = pout->t_out[CUR_IO];

    nlayer = pcmsh->pcount->nlayer;

    for (k = 0; k < nlayer; k++) {
      player = pcmsh->p_layer[k];
      nele = player->nele;
      p_ele = player->p_ele;

      for (j = 0; j < nele; j++) {
        pele = p_ele[j];
        sum = 0;

        pele->phydro->pmb->mass[timer] = pele->phydro->h[state];   // NF 7/12/2018
        pele->phydro->pmb->storage[timer] = pele->phydro->S[SCUR]; // NF 7/12/2018

        // NG : 04/03/2020 : Rewritten sub-part to account for multiple sources for each kindof source-type in mass balance calculations
        // NG : 04/03/2024 : Additional case to account for modified uptakes if threshold option is ON.
        if (pele->phydro->psource != NULL) {

          for (i = 0; i < NSOURCE; i++) {

            if (pele->phydro->psource[i] != NULL) {

              if (i == UPTAKE_AQ && pele->phydro->p_thresh != NULL) { // Threshold values have been defined
                som_source = 0.;
                som_source = pele->phydro->thresh_val[UPTAKE_THRESH_AQ][state]; // For uptakes, values in m/s. thresh_val is the effective uptake ts.
              } else {                                                          // Common case
                psource = pele->phydro->psource[i];
                nb_pbound_source = pele->phydro->nsource_type[i]; // Number of pbound sources for current source type checked
                som_source = 0.;

                for (l = 0; l < nb_pbound_source; l++) {

                  if (psource[l]->freq == CST)
                    som_source += psource[l]->pft->ft; // m/s
                  else
                    som_source += TS_interpolate_ts(t, psource[l]->pft);
                }
              }
              pele->phydro->pmb->source[i][timer] = som_source * pele->pdescr->surf; // conversion en m3/s
              sum -= pele->phydro->pmb->source[i][timer];
            }
          }
        }

        // Filling discharge at faces (m3/s) along x, y and z.
        for (idir = 0; idir < ND_MSH; idir++) {
          for (itype = 0; itype < NFACE_MSH; itype++) {
            pele->phydro->pmb->flux[idir][itype][timer] = 0.;
            neigh = pele->p_neigh[idir][itype];
            if (neigh != NULL) // si il y a des voisins
            {
              neigh = IO_browse_id(neigh, BEGINNING_TS);
              n_neigh = IO_length_id(neigh);
              while (neigh != NULL) {
                pneigh = MSH_get_ele(pcmsh, neigh->id_lay, neigh->id, fpout);
                pface = MSH_return_face(pele, pneigh, idir, itype, fpout);
                flux = pface->phydro->pcond->conductance * (pneigh->phydro->h[state] - pele->phydro->h[state]);
                pele->phydro->pmb->flux[idir][itype][timer] += flux;
                sum += flux;
                neigh = neigh->next;
              }
            } else // si pas de voisins
            {
              if (pele->type == BOUND) // si la maille est BOUND
              {
                if (idir == Z_MSH && itype == TWO && pele->phydro->pbound->type == CAUCHY) {
                  pface = pele->face[idir][itype];
                  if (pface->loc == TOP_MSH) { // FB 12/04/2018 RIV_MSH cells are managed by cawaqs for convergence issue with Picard iterations (the value of the Cauchy discharge is calculated in CAW_cAQ_check_qlim_riv for RIV_MSH faces). Libaq knows TOP_MSH cells only.
                    pele->phydro->pmb->pbound[CAUCHY][timer] = AQ_discharge_cauchy(pface, pele, t, state, fpout);
                  }
                } else if (pele->phydro->pbound->type == NEUMANN) { // TV2019 start fix
                  pface = pele->face[idir][itype];
                  if (pface->type == BOUND) {
                    if (pface->phydro->pbound != NULL) {
                      pft = pface->phydro->pbound->pft;
                      while (pft != NULL) {
                        // LP_printf(fpout," value of boundary in element %d at face [%s][%s] : %f at time %f\n",pele->id[ABS_MSH],MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout),pft->ft,pft->t);
                        l_side = pface->p_ele[MSH_switch_direction(itype, fpout)]->pdescr->l_side; // Fix TV2019 09/08/2019
                        pele->phydro->pmb->pbound[NEUMANN][timer] = pft->ft * l_side;              // Fix TV2019 09/08/2019
                        pft = pft->next;
                      }
                    } else {
                      // LP_printf(fpout," face [%s][%s] is a NULL flux boundary\n",MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout));
                    }
                  }
                } // TV2019 end fix

                /* else
                  {
                    pele->phydro->pmb->flux[idir][itype][timer]=0.;
                    }*/
              }
              /*else
                {
                  pele->phydro->pmb->flux[idir][itype][timer]=0.;
                  }*/
            }
          }
        }

        if (pele->type == BOUND) {
          if (pele->phydro->pbound->type == DIRICHLET) {
            pele->phydro->pmb->pbound[DIRICHLET][timer] = -sum;
          }
        }
      }
    }
  }
}

// NF 7/12/2018 Fast coding to account for storage variation in UNCONFINED condition, when the head swithches between two lithologies
double AQ_calc_vol(s_ele_msh *pele, int timer, FILE *fpout) {
  s_litho_aq *plitho;
  s_hydro_aq *phydro;
  double zbot;
  double wd = 0;
  double vol = 0;
  double thick = 0, Sy = 0;
  thick = 0;

  phydro = pele->phydro;
  if (phydro->status == CONFINED_AQ)
    vol = pele->phydro->pmb->mass[timer] * pele->phydro->pmb->storage[timer];
  else {
    zbot = phydro->zbot;
    plitho = phydro->plitho;
    wd = pele->phydro->pmb->mass[timer] - phydro->zbot;
    if (wd > EPS_TS) {
      while (thick < wd && plitho->next != NULL) {
        thick += plitho->thick;
        Sy = plitho->Sy;
        vol += plitho->thick * Sy;
        plitho = plitho->next;
      }
      vol += (wd - thick) * Sy;
    } else {
      LP_warning(fpout, "libaq%4.2f %s in %s l%d: Element %d Error in mb because head below the layer bottom : %f\n", VERSION_AQ, __func__, __FILE__, __LINE__, pele->id[ABS_MSH], wd); // NF 17/12/2018 The volume is set to 0 because. Let see if we have to calculate a negative volume with Sy of the first litho
    }
  }
  return vol;
}

/**\fn void AQ_calc_mb_end(s_carac_aq *pchar_aq,s_chronos_CHR *chronos,s_out_io **pout_all,FILE *fpout)
 *\brief function to calculate mass balance at the end of a time step
 *\return void
 *
 */
void AQ_calc_mb_end(s_carac_aq *pchar_aq, s_chronos_CHR *chronos, s_out_io **pout_all, FILE *fpout) {
  int nlayer, nele, k, j, i, ii;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele;
  s_mb_aq *pmb;
  s_bound_aq **psource; // NG : 04/03/2020
  s_ft *pft;
  s_out_io *pout;
  double S_1, S_0, flux;
  double deltat, a0, theta, sum_flux_ini, sum_flux_end;
  double t_out, dt, t;
  int n_neigh, idir, itype;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  s_face_msh *pface;
  s_cmsh *pcmsh;
  double m[DMASS_AQ]; // NF 7/12/2018

  pcmsh = pchar_aq->pcmsh;
  a0 = pchar_aq->settings->a0;
  theta = pchar_aq->settings->general_param[THETA_AQ];

  if (pout_all[MASS_IO] != NULL) {

    pout = pout_all[MASS_IO];
    t = chronos->t[CUR_CHR];
    dt = chronos->dt;
    t_out = pout->t_out[CUR_IO];
    nlayer = pcmsh->pcount->nlayer;

    for (k = 0; k < nlayer; k++) {
      player = pcmsh->p_layer[k];
      nele = player->nele;
      p_ele = player->p_ele;

      for (j = 0; j < nele; j++) {
        pele = p_ele[j];
        pele->phydro->pmb->error = 0.;

        for (ii = 0; ii < DMASS_AQ; ii++) {
          if (player->type == CONFINED_AQ)
            m[ii] = pele->phydro->pmb->mass[ii] * pele->phydro->pmb->storage[ii]; // NF 7/12/2018 For mb calculation, we need to calculate the volume of water accounting for SY or SS
          else
            m[ii] = AQ_calc_vol(pele, ii, fpout); // NF 7/12/2018 Fast coding to account for storage variation in UNCONFINED condition, when the head swithches between two lithologies
        }

        pele->phydro->pmb->mass[DMASS_AQ] = a0 * pele->pdescr->surf * (m[END_AQ] - m[INI_AQ]) / dt; // NF 7/12/2018

        // NG : 04/03/2020 : Updated subpart to integrate newer format of ***psource pointer
        for (i = 0; i < NSOURCE; i++) {
          if (pele->phydro->psource != NULL) {
            psource = pele->phydro->psource[i];
            if (psource != NULL) {
              pele->phydro->pmb->source[i][DMASS_AQ] = theta * pele->phydro->pmb->source[i][END_AQ] + (1 - theta) * pele->phydro->pmb->source[i][INI_AQ];
              pele->phydro->pmb->error -= pele->phydro->pmb->source[i][DMASS_AQ]; // Sign - because uptake is positive and recharge negative
            }
          }
          // else ca reste a 0 comme initialise
        }

        // Filling discharge at faces (m3/s) along x, y and z.
        for (idir = 0; idir < ND_MSH; idir++) {
          for (itype = 0; itype < NFACE_MSH; itype++) {
            pele->phydro->pmb->flux[idir][itype][DMASS_AQ] = theta * pele->phydro->pmb->flux[idir][itype][END_AQ] + (1 - theta) * pele->phydro->pmb->flux[idir][itype][INI_AQ];
            pele->phydro->pmb->error += pele->phydro->pmb->flux[idir][itype][DMASS_AQ];
          }
        }

        for (i = 0; i < NBOUND; i++) {
          if (i == CAUCHY) {
            pele->phydro->pmb->pbound[i][DMASS_AQ] = pele->phydro->pmb->pbound[i][END_AQ];
          } else
            pele->phydro->pmb->pbound[i][DMASS_AQ] = theta * pele->phydro->pmb->pbound[i][END_AQ] + (1 - theta) * pele->phydro->pmb->pbound[i][INI_AQ];

          pele->phydro->pmb->error += pele->phydro->pmb->pbound[i][DMASS_AQ];
        }

        pele->phydro->pmb->error -= pele->phydro->pmb->mass[DMASS_AQ];
      }
    }
  }
}

double AQ_discharge_cauchy(s_face_msh *pface, s_ele_msh *pele, double t, int time, FILE *fpout) {
  double mb = 0.;
  double h_riv;
  s_ft *pft;

  if (pface->phydro->pcond == NULL)
    LP_error(fpout, "Attempt to calculate Cauchy discharge but the conductance is not defined at the face for element ID_ABS %d.\n", pele->id[ABS_MSH]);

  if (pele->modif[MODIF_MB_AQ] == NO_LIM_AQ) {
    pft = TS_browse_ft(pele->phydro->pbound->pft, BEGINNING_TS);
    // printf("h_riv=%f h=%f\n",h_riv,pele->phydro->h[time]);
    h_riv = TS_interpolate_ts(t, pft);
    mb = pface->phydro->pcond->conductance * (h_riv - pele->phydro->h[time]);
    // printf("h_riv=%f h=%f\n",h_riv,pele->phydro->h[time]);
    // getchar();
  } else {
    mb = -pele->phydro->Qlim; // Rappel Qlim est negatif. Donc le signe -.
    // printf("h_riv=%f h=%f\n",h_riv,pele->phydro->h[time]);
    // getchar();
  }
  return mb;
}
