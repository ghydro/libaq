/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_picart.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdarg.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
#include "MSH.h"
#include "AQ.h"

s_picart_aq *AQ_init_picart(void) {
  s_picart_aq *ppic;

  ppic = new_picart_aq();
  bzero((char *)ppic, sizeof(s_picart_aq));
  ppic->eps_pic = EPS_TS;
  ppic->nitmin = PICNITMIN_AQ;
  ppic->nitmax = PICNITMAX_AQ;

  return ppic;
}

void AQ_optimise_pic(s_cmsh *pcmsh, s_picart_aq *ppic, int isim_type, FILE *flog) {
  int *n_eletype;
  int neletype;

  n_eletype = pcmsh->pcount->pcount_hydro->nele_type;
  neletype = n_eletype[CAUCHY];

  LP_printf(flog, "Optimising surface-subsurface modelling :\n", ppic->nitmin);
  if ((neletype == 0) && (isim_type != STEADY_AQ)) {
    ppic->nitmin = 1;
    LP_printf(flog, "\tNo need for Picart coupling\n\tMinimum number of Picart iterations set to %d\n", ppic->nitmin);
  } else {
    LP_printf(flog, "\tMinimum number of Picart iterations per time step : %d\n\tMaximum number of Picart iterations per time step : %d\n\tConvergence criterion %e\n", ppic->nitmin, ppic->nitmax, ppic->eps_pic);
  }
}

void AQ_reinit_pic(s_picart_aq *ppic) { ppic->iit = 0; }
