/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_output.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"
#ifdef OMP
#include "omp.h"
#endif

void AQ_print_output(s_chronos_CHR *chronos, double t_day, s_carac_aq *pchar_aq, s_out_io *pout, int out_type, FILE *fpout) {
  double t, t_out, dt, sum_abs;
  int nlayer, nele, i, j, k, m, id_abs, idir, itype, nsource, nbound;
  int nele_tot, type_out, output, output_i, n_neigh, logprint;
  double **val;
  double *mb_layer;
  s_cmsh *pcmsh;
  s_layer_msh *player;
  s_ele_msh *pele;
  s_id_io *window;
  s_face_msh *pface;
  s_id_io *neigh;
  s_ele_msh *pneigh;
  FILE *fp;

  t = chronos->t[CUR_CHR];
  dt = chronos->dt;
  t_out = pout->t_out[CUR_IO];
  output = NB_OUT_AQ;
  logprint = pchar_aq->settings->log_balance;

  // NG : 29/01/2021 : Log-print of layer mass balance option added.
  if (logprint == YES_TS)
    AQ_log_print_layer_header(fpout);

  pcmsh = pchar_aq->pcmsh;
  fp = pout->fout;
  type_out = pout->type_out;

  if (type_out == ALL_IO)
    nele_tot = pcmsh->pcount->nele;
  else
    nele_tot = IO_length_id(pout->id_output);

  val = (double **)malloc(output * sizeof(double *));
  mb_layer = (double *)calloc(output, sizeof(double)); // NG 29/01/2021 : To store all terms of the mass balance at the layer scale

  for (i = 0; i < output; i++)
    val[i] = (double *)malloc(nele_tot * sizeof(double));

  if (type_out == ALL_IO) {
    nlayer = pcmsh->pcount->nlayer;
    for (k = 0; k < nlayer; k++) {
      player = pcmsh->p_layer[k];
      nele = player->nele;

      for (m = 0; m < output; m++)
        mb_layer[m] = 0.;

      for (j = 0; j < nele; j++) {
        pele = player->p_ele[j];
        id_abs = pele->id[ABS_MSH] - 1;
        sum_abs = 0;
        output_i = 0;

        // To debug, if you want to print H at the beginning of the time step
        // val[output_i][id_abs]=pele->phydro->pmb->mass[INI_AQ];
        // output_i++;

        // 2 columns Hend, DeltaV/Deltat
        val[output_i][id_abs] = pele->phydro->pmb->mass[END_AQ]; // output_i=H_INI in enum out_aq_mb
        output_i++;
        val[output_i][id_abs] = pele->phydro->pmb->mass[DMASS_AQ]; // output_i=DV_DT in enum out_aq_mb
        output_i++;
        // sum_abs+=fabs(pele->phydro->pmb->mass[DMASS_AQ]);

        // 6 colonnes (fluxes x,y,z)
        for (idir = 0; idir < ND_MSH; idir++) {
          for (itype = 0; itype < NFACE_MSH; itype++) {
            val[output_i][id_abs] = pele->phydro->pmb->flux[idir][itype][DMASS_AQ]; // FB 27/03/2018 In the output file, positive fluxes are fluxes entering the cell, negative fluxes are fluxes that exit the cell
            // output_i=FLUX_X_ONE,FLUX_X_TWO,FLUX_Y_ONE,FLUX_Y_TWO,FLUX_Z_ONE,FLUX_Z_TWO in enum out_aq_mb
            sum_abs += fabs(pele->phydro->pmb->flux[idir][itype][DMASS_AQ]);
            //                        LP_printf(fpout,"In %s Velocity of id_abs: %d idir: %d, itype: %d, value: %e\n",__func__ , id_abs,idir, itype,pele->phydro->pmb->flux[idir][itype][DMASS_AQ]);  // NG check

            output_i++;
          }
        }

        // To debug, if you want to print the conductances at faces (6 columns)
        /*for(idir=0;idir<ND_MSH;idir++)
        {
                for(itype=0;itype<NFACE_MSH;itype++)
                {
                        neigh=pele->p_neigh[idir][itype];
                        if(neigh!=NULL)//si il y a des voisins
                        {
                                neigh=IO_browse_id(neigh,BEGINNING_TS);
                                n_neigh= IO_length_id(neigh);
                                if (n_neigh>1)
                                        val[output_i][id_abs]=CODE_TS;
                                else
                                {
                                        pneigh=MSH_get_ele(pcmsh,neigh->id_lay,neigh->id,fpout);
                                        pface=MSH_return_face(pele,pneigh,idir,itype,fpout);
                                        val[output_i][id_abs]=pface->phydro->pcond->conductance;
                                }
                        }
                        else if (idir==Z_MSH && itype==TWO_MSH)
                                val[output_i][id_abs]=pface->phydro->pcond->conductance;
                        else
                                val[output_i][id_abs]=CODE_TS;
                        output_i++;
                }
        }*/

        // 2 columns (recharge, uptake)
        for (nsource = 0; nsource < NSOURCE; nsource++) {
          val[output_i][id_abs] = pele->phydro->pmb->source[nsource][DMASS_AQ] * (-1.); // FB 27/03/2018 In the output file, recharge is positive (entering) and uptake is negative (outgoing), whereas in structure pmb the recharge is negative and the uptake is positive.
          // output_i=REC,UPT in enum out_aq_mb
          sum_abs += fabs(pele->phydro->pmb->source[nsource][DMASS_AQ]);
          output_i++;
        }

        // 4 columns Dirichlet, Neumann, Overflow, Stream-aquifer exchange
        for (nbound = 0; nbound < NBOUND; nbound++) {
          if (nbound == CAUCHY) {
            if (pele->loc == TOP_MSH) {
              val[output_i][id_abs] = pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // output_i=SOL in enum out_aq_mb
              output_i++;
              val[output_i][id_abs] = 0.; // output_i=ENR in enum out_aq_mb
              output_i++;
            } else if (pele->loc == RIV_MSH) {
              val[output_i][id_abs] = 0.; // output_i=SOL in enum out_aq_mb
              output_i++;
              val[output_i][id_abs] = pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // output_i=ENR in enum out_aq_mb
              output_i++;
            } else {
              val[output_i][id_abs] = 0.; // output_i=SOL in enum out_aq_mb
              output_i++;
              val[output_i][id_abs] = 0.; // output_i=ENR in enum out_aq_mb
              output_i++;
            }
          } else {
            val[output_i][id_abs] = pele->phydro->pmb->pbound[nbound][DMASS_AQ]; // output_i=FLUX_DIRICHLET,FLUX_NEUMANN in enum out_aq_mb
            output_i++;
          }
          sum_abs += fabs(pele->phydro->pmb->pbound[nbound][DMASS_AQ]);
        }

        // column Error
        val[output_i][id_abs] = pele->phydro->pmb->error; // output_i=ERR in enum out_aq_mb
        output_i++;

        // column Relative Error
        if (sum_abs != 0)
          val[output_i][id_abs] = fabs(pele->phydro->pmb->error) / sum_abs; // output_i=ERR_REL in enum out_aq_mb
        else
          val[output_i][id_abs] = CODE_TS;

        for (m = 0; m < output; m++)
          mb_layer[m] += val[m][id_abs];
      } // End cell loop

      if (logprint == YES_TS) {
        LP_printf(fpout, "%2d ", player->id);
        LP_printf(fpout, "%10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e", mb_layer[REC], mb_layer[UPT], mb_layer[SOL], mb_layer[FLUX_Z_ONE], mb_layer[FLUX_Z_TWO], mb_layer[ENR], mb_layer[FLUX_DIRICHLET], mb_layer[FLUX_NEUMANN], mb_layer[DV_DT]);
        LP_printf(fpout, "\n");
      }
    } // End layer loop
  } else
    LP_error(fpout, "Only the case ALL_IO has been considered in AQ_print_output.\n");

  if (pout->format == UNFORMATTED_IO)
    IO_print_bloc_bin(val, nele_tot, output, fp);
  else
    AQ_print_for(t_day, pcmsh, val, pout, out_type, fp, fpout);

  for (i = 0; i < output; i++) // FB 05/04/2018 Free memory
    free(val[i]);
  free(val);
  free(mb_layer);
}

void AQ_print_hydhead_only(s_chronos_CHR *chronos, double t_day, s_carac_aq *pchar_aq, s_out_io *pout, int out_type, FILE *fpout) {
  int nlayer, nele, i, j, k, id_abs, nele_tot, type_out;
  int pos = 0;
  double **val;
  double t, t_out, dt;
  s_cmsh *pcmsh;
  s_layer_msh *player;
  s_ele_msh *pele;
  FILE *fp;

  t = chronos->t[CUR_CHR];
  dt = chronos->dt;
  t_out = pout->t_out[CUR_IO];
  pcmsh = pchar_aq->pcmsh;
  fp = pout->fout;
  type_out = pout->type_out;

  if (type_out == ALL_IO)
    nele_tot = pcmsh->pcount->nele;
  else
    nele_tot = IO_length_id(pout->id_output);

  val = (double **)malloc(NB_VAL_OUTPUT_HYDHEAD_AQ * sizeof(double *));
  for (i = 0; i < NB_VAL_OUTPUT_HYDHEAD_AQ; i++)          // NG : Here, **val is actually a vector. I've kept the **val definition so the
    val[i] = (double *)malloc(nele_tot * sizeof(double)); // AQ_print_for() can be reused directly. Can also be useful if NB_VAL_OUTPUT_HYDHEAD_AQ is set
                                                          // to a higher value (ie. > 1) if needed in the future.

  if (type_out == ALL_IO) {
    nlayer = pcmsh->pcount->nlayer;
    for (k = 0; k < nlayer; k++) {
      player = pcmsh->p_layer[k];
      nele = player->nele;
      for (j = 0; j < nele; j++) {
        pele = player->p_ele[j];
        id_abs = pele->id[ABS_MSH] - 1;
        val[pos][id_abs] = pele->phydro->pmb->mass[END_AQ]; // Hydraulic head only
      }
    }
  } else
    LP_error(fpout, "libaq%4.2f : Error in file %s, function %s at line %d : Mask-type selection option unavailable.\n", VERSION_AQ, __FILE__, __func__, __LINE__);

  if (pout->format == UNFORMATTED_IO)
    IO_print_bloc_bin(val, nele_tot, 1, fp);
  else
    AQ_print_for(t_day, pcmsh, val, pout, out_type, fp, fpout);

  for (i = 0; i < NB_VAL_OUTPUT_HYDHEAD_AQ; i++)
    free(val[i]);

  free(val);
}

void AQ_fill_threshold_output(s_chronos_CHR *chronos, double t_day, s_carac_aq *pchar_aq, s_out_io *pout, int out_type, FILE *fpout) {

  int nlayer, nele, i, j, k, id_abs, nele_tot, type_out, nb_pbound;
  double **val;
  double *u_values;
  double t, t_out, dt, theta;
  s_cmsh *pcmsh;
  s_layer_msh *player;
  s_ele_msh *pele;
  FILE *fp;
  s_bound_aq **psource;

  theta = pchar_aq->settings->general_param[THETA_AQ];
  t = chronos->t[CUR_CHR];
  dt = chronos->dt;
  t_out = pout->t_out[CUR_IO];
  pcmsh = pchar_aq->pcmsh;
  fp = pout->fout;
  type_out = pout->type_out;

  if (type_out == ALL_IO) {
    nele_tot = pcmsh->pcount->nele;
  } else {
    nele_tot = IO_length_id(pout->id_output);
  }

  val = (double **)malloc(NB_OUT_THRESH_AQ * sizeof(double *));
  for (i = 0; i < NB_OUT_THRESH_AQ; i++)
    val[i] = (double *)calloc(nele_tot, sizeof(double)); // NG : 16-04-2024 - Bug fix. calloc not malloc. val was filled with junk !

  if (type_out == ALL_IO) {
    nlayer = pcmsh->pcount->nlayer;
    for (k = 0; k < nlayer; k++) {
      player = pcmsh->p_layer[k];
      nele = player->nele;
      for (j = 0; j < nele; j++) {
        pele = player->p_ele[j];
        id_abs = pele->id[ABS_MSH] - 1;

        // Simulated hydraulic head.
        val[H_END_AQ][id_abs] = pele->phydro->pmb->mass[END_AQ];

        // Threshold hydraulic head (-9999 if undefined).
        val[H_THR_AQ][id_abs] = pele->phydro->thresh_val[H_THRESH_AQ][DMASS_AQ];

        // Initial input uptake (non modified due to the potential impact of thresholds).
        nb_pbound = pele->phydro->nsource_type[UPTAKE_AQ]; // Number of uptakes

        if (pele->phydro->psource != NULL) {
          psource = pele->phydro->psource[UPTAKE_AQ]; // psource in m/s
          if (psource != NULL) {
            u_values = AQ_calc_source_value(nb_pbound, psource, t, dt, fpout);
            val[UPT_AQ][id_abs] = (-1.) * ((theta * u_values[1] + (1 - theta) * u_values[0])) * pele->pdescr->surf;
            free(u_values);
          }
        }

        // Effective uptake due to threshold inputs.
        val[UPT_THR_AQ][id_abs] = pele->phydro->pmb->source[UPTAKE_AQ][DMASS_AQ] * (-1.);

        // DRY/WET status of the cell
        val[DRYWET_AQ][id_abs] = (double)pele->phydro->hyd_cellstate;
      }
    }
  } else {
    LP_error(fpout, "libaq%4.2f : Error in file %s, function %s at line %d : Mask-type selection option unavailable.\n", VERSION_AQ, __FILE__, __func__, __LINE__);
  }

  if (pout->format == UNFORMATTED_IO) {
    IO_print_bloc_bin(val, nele_tot, NB_OUT_THRESH_AQ, fp);
  } else {
    AQ_print_for(t_day, pcmsh, val, pout, out_type, fp, fpout);
  }

  for (i = 0; i < NB_OUT_THRESH_AQ; i++)
    free(val[i]);

  free(val);
}

void AQ_print_for(double t_day, s_cmsh *pcmsh, double **val, s_out_io *pout, int out_type, FILE *fp, FILE *flog) {
  int nlayer, nele, id_abs, i, k, j;
  s_ele_msh *pele;
  s_layer_msh *player;
  s_id_io *window;

  if (pout->type_out == ALL_IO) {
    nlayer = pcmsh->pcount->nlayer;

    for (k = 0; k < nlayer; k++) {
      player = pcmsh->p_layer[k];
      nele = player->nele;
      for (j = 0; j < nele; j++) {
        pele = player->p_ele[j];
        id_abs = MSH_get_ele_id(pele, ABS_MSH) - 1;
        // fprintf(fp, "%f %d %d %d ", t_day, MSH_get_ele_id(pele, ABS_MSH), MSH_get_ele_id(pele, GIS_MSH), MSH_get_ele_id(pele, INTERN_MSH)); // NG: Does not really make sense for AQ cells.
        fprintf(fp, "%f %d %d %d ", t_day, MSH_get_ele_id(pele, INTERN_MSH), player->id - 1, MSH_get_ele_id(pele, ABS_MSH));

        switch (out_type) {
        case MASS_IO: {
          for (i = 0; i < NB_OUT_AQ; i++)
            fprintf(fp, "%e ", val[i][id_abs]);
          break;
        }
        case AQ_H_IO: {
          for (i = 0; i < NB_VAL_OUTPUT_HYDHEAD_AQ; i++)
            fprintf(fp, "%e ", val[i][id_abs]);
          break;
        }
        case AQ_THRESH_IO: {
          for (i = 0; i < NB_OUT_THRESH_AQ; i++)
            fprintf(fp, "%e ", val[i][id_abs]);
          break;
        }
        default: {
          LP_error(flog, "In libaq%4.2f in file %s : Inappropriate or unknown aquifer output type.\n", VERSION_AQ, __FILE__);
          break;
        }
        }
        fprintf(fp, "\n");
      }
    }
  } else {
    LP_error(flog, "libaq%4.2f : Error in file %s, function %s at line %d : Mask-type selection option unavailable.\n", VERSION_AQ, __FILE__, __func__, __LINE__);
  }
}

// NG : 09/02/2024 : Restructured function to handle all three output type for the aquifer module
void AQ_print_header(FILE *fpout, int out_type, FILE *flog) {
  int nsource, idir, itype, nbound;

  // Common part
  fprintf(fpout, "DAY ID_INT ID_LAYER ID_ABS ");

  switch (out_type) {
  case MASS_IO: {
    fprintf(fpout, "H[END_AQ](m) ");
    fprintf(fpout, "DV/Dt(m3/s)");

    for (idir = 0; idir < ND_MSH; idir++) {
      for (itype = 0; itype < NFACE_MSH; itype++) {
        fprintf(fpout, " AQ_FLUX[%s][%s](m3/s)", MSH_name_direction(idir, fpout), MSH_name_type(itype, fpout));
      }
    }
    // FB To debug
    /* for(idir=0;idir<ND_MSH;idir++)
    {
      for(itype=0;itype<NFACE_MSH;itype++)
      {
        fprintf(fpout," Conduc[%s][%s]",MSH_name_direction(idir,fpout),MSH_name_type(itype,fpout));
      }
    }*/

    for (nsource = 0; nsource < NSOURCE; nsource++)
      fprintf(fpout, " AQ_%s(m3/s)", AQ_name_source(nsource));

    for (nbound = 0; nbound < N_BC_MB - 2; nbound++)
      fprintf(fpout, " AQ_%s(m3/s)", AQ_name_bc_mb(nbound));

    fprintf(fpout, " AQ_DISCHARGE_AT_SOIL(m3/s)");
    fprintf(fpout, " AQ_iNR(m3/s)");
    fprintf(fpout, " %s_%s(m3/s)", IO_name_mod(out_type), "ERROR");
    fprintf(fpout, " %s_%s(-)", IO_name_mod(out_type), "RELATIVE_ERROR");
    break;
  }
  case AQ_H_IO: {
    fprintf(fpout, "H[END_AQ](m) ");
    break;
  }
  case AQ_THRESH_IO: {
    fprintf(fpout, "H[END_AQ](m) ");
    fprintf(fpout, "H_threshold(m) ");
    fprintf(fpout, "AQ_potential_%s(m3/s) ", AQ_name_source(UPTAKE_AQ));
    fprintf(fpout, "AQ_effective_%s(m3/s) ", AQ_name_source(UPTAKE_AQ));
    fprintf(fpout, "AQ_wetdry_status(-)");
    break;
  }
  default: {
    LP_error(flog, "libaq%4.2f : Error in file %s, function %s at line %d : Unknown aquifer output type.\n", VERSION_AQ, __FILE__, __func__, __LINE__);
    break;
  }
  }
  fprintf(fpout, "\n");
}

// NG : 31/05/2021 : Absolute mass balance error added
void AQ_log_print_layer_header(FILE *fpout) {
  LP_printf(fpout, "\n                                              AQUIFER FLUXES                                         \n");
  LP_printf(fpout, "-----------------------------------------------------------------------------------------------------\n");
  LP_printf(fpout, "ID   %s    %s  %s  %s  %s  %s  %s    %s   %s       \n", AQ_name_outvar(REC), AQ_name_outvar(UPT), AQ_name_outvar(SOL), AQ_name_outvar(FLUX_Z_ONE), AQ_name_outvar(FLUX_Z_TWO), AQ_name_outvar(ENR), AQ_name_outvar(FLUX_DIRICHLET), AQ_name_outvar(FLUX_NEUMANN), AQ_name_outvar(DV_DT));
}

/**
 * \fn void AQ_print_corresp(s_cmsh*, int, FILE*)
 * \brief Prints the AQ_print_corresp.txt output file
 * \return
 *
 * NG : 05/05/2023 : Adding surface and libmesh POSITION cell attribute (to easily
 *                   find TOP and RIV cells in external programs for instance).
 */
void AQ_print_corresp(s_cmsh *pcmsh, int mod, FILE *fp) {
  int nlayer, nele, r, e;
  FILE *fpout;
  s_layer_msh *player;
  s_ele_msh *pele;
  char *name_mod, *name;

  name_mod = IO_name_mod(mod);
  name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/%s_corresp_file.txt", getenv("RESULT"), name_mod);
  fpout = fopen(name, "w");
  free(name_mod);

  if (fpout == NULL) {
    LP_error(fp, "Cannot open file %s", name);
  }

  fprintf(fpout, "ID_INTERN ID_LAYER ID_ABS ID_GIS AREA POSITION_MSH X_CENTROID Y_CENTROID\n");
  nlayer = pcmsh->pcount->nlayer;
  for (r = 0; r < nlayer; r++) {
    player = pcmsh->p_layer[r];
    nele = player->nele;
    for (e = 0; e < nele; e++) {
      pele = player->p_ele[e];
      fprintf(fpout, "%d %d %d %d %.2f %s %.2f %.2f\n", pele->id[INTERN_MSH], player->id - 1, pele->id[ABS_MSH], pele->id[GIS_MSH], pele->pdescr->surf, MSH_name_position_cell(pele->loc, fp), pele->pdescr->center[X_MSH], pele->pdescr->center[Y_MSH]);
    }
  }
  fclose(fpout);
  free(name);
}

void AQ_print_abstract(s_carac_aq *pchar_aq, FILE *flog) {

  int i, j;
  int nlayer, nele, nsize;
  s_layer_msh *player;
  s_ele_msh *pele;
  char *name;
  nlayer = pchar_aq->pcmsh->pcount->nlayer;
  nele = pchar_aq->pcmsh->pcount->nele;
  nsize = pchar_aq->pcmsh->nsize;

  LP_printf(flog, "\t --> Layer number : %d\n", nlayer);
  for (i = 0; i < nlayer; i++) {
    player = pchar_aq->pcmsh->p_layer[i];
    LP_printf(flog, " Layer %d named %s element number in layer %d \n", i + 1, player->name, player->nele);
  }
  LP_printf(flog, "\t --> Total Element Number : %d \n", nele);
  LP_printf(flog, "number of different cell size : %d\n", nsize);
  for (i = 0; i < nsize; i++) {
    if (i > 0)
      LP_printf(flog, "Size %d : %f m \n", i + 1, pchar_aq->pcmsh->l_max / (pow(2, i)));
    else
      LP_printf(flog, "Size %d : %f m \n", i + 1, pchar_aq->pcmsh->l_max);
  }
}

double AQ_get_conductance_at_faceZTWO(s_face_msh *pface) {
  double val = CODE_TS;
  if (pface->p_subface == NULL)
    val = pface->phydro->pcond->conductance;
  else
    val = (double)NOCONDUCT_AQ;
}

// NG : 11/08/2021 : Prints a full overview of AQ system input initial parameters
void AQ_print_param_overview(s_carac_aq *pchar_aq, int modname, FILE *flog) {
  int l, e, nlayer, nele;
  char *name_mod, *name, *header;
  double eptot, trans, stor;
  s_layer_msh *player;
  s_ele_msh *pele;
  FILE *fpout;
  s_face_msh *pface;
  double val = CODE_TS; // NF 14/9/2022

  name_mod = IO_name_mod(modname);
  name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
  header = (char *)malloc(STRING_LENGTH_LP * sizeof(char));

  sprintf(name, "%s/%s_param_overview.txt", getenv("RESULT"), name_mod);
  free(name_mod);

  fpout = fopen(name, "w");

  if (fpout == NULL)
    LP_error(flog, "In libaq%4.2f : Error in file %s, function %s at line %d : Cannot open file %s", VERSION_AQ, __FILE__, __func__, __LINE__, name);

  sprintf(header, "%s %s %s %s %s %s %s %s", "ID_INTERN ID_LAYER ID_ABS ID_GIS AREA XCENTR YCENTR", AQ_shortname_param(THICK), AQ_shortname_param(TRANSM_HOMO), AQ_shortname_param(STORAGE), AQ_shortname_param(H_INI), AQ_shortname_param(CONDUCT), AQ_shortname_param(ZBOT), AQ_shortname_param(ZTOP));

  fprintf(fpout, "%s\n", header);
  nlayer = pchar_aq->pcmsh->pcount->nlayer;

  for (l = 0; l < nlayer; l++) {
    player = pchar_aq->pcmsh->p_layer[l];
    nele = player->nele;

    for (e = 0; e < nele; e++) {
      // Infos de géométrie
      pele = player->p_ele[e];
      eptot = 0., trans = 0.;
      stor = 0.;
      fprintf(fpout, "%d %d %d %d %.2f %.2f %.2f ", pele->id[INTERN_MSH], player->id, pele->id[ABS_MSH], pele->id[GIS_MSH], pele->pdescr->surf, pele->pdescr->center[X_MSH], pele->pdescr->center[Y_MSH]);

      // Epaisseurs, Transmissivités
      if (player->type == UNCONFINED_AQ) {
        eptot = AQ_calc_litho_thickness(pele->phydro, flog);
        trans = AQ_calc_T_confined(pele->phydro, flog);
      } else {
        eptot = pele->phydro->Thick;
        trans = pele->phydro->ptransm->transm[T_HOMO];
      }
      fprintf(fpout, "%.2f %.3e ", eptot, trans);

      // Emmagasinement
      if (player->type == UNCONFINED_AQ)
        stor = AQ_set_Sy_unconfined(pele->phydro, flog);
      else
        stor = pele->phydro->S[SCUR];
      fprintf(fpout, "%.3e ", stor);

      pface = pele->face[Z_MSH][TWO_MSH];
      if (player->cond_type == AUTO_COND) {
        val = AQ_get_conductance_at_faceZTWO(pface);
        fprintf(fpout, "%.3f %.3e ", pele->phydro->h[T], val); // NF 14/9/2022 The conductance is defined for Z TWO faces only ! The one at the element scale may only be used for river defined with a streambed--> changed from element to faces
      } else {
        if (player->cond_type == COND) {
          if (pface->loc == TOP_MSH)
            fprintf(fpout, "%.3f %.3e ", pele->phydro->h[T], pele->phydro->pcond->conductance);
          else {
            val = AQ_get_conductance_at_faceZTWO(pface);
            fprintf(fpout, "%.3f %.3e ", pele->phydro->h[T], val);
          }
        } // NF 14/9/2022 case TZ not treated yet
      }
      fprintf(fpout, "%.2f %.2f ", pele->phydro->zbot, pele->phydro->ztop);
      fprintf(fpout, "\n");
    }
  }
  fclose(fpout);
  free(header);
  free(name);
}

void AQ_print_final_state(s_carac_aq *pchar_aq, FILE *flog) {
  int nlayer, nele;
  int i, j;
  s_layer_msh *player;
  s_ele_msh *pele;
  FILE *fout;
  char *name;
  nlayer = pchar_aq->pcmsh->pcount->nlayer;

  for (i = 0; i < nlayer; i++) {
    player = pchar_aq->pcmsh->p_layer[i];
    nele = player->nele;
    name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
    sprintf(name, "%s/Hend_%s.txt", getenv("RESULT"), player->name);
    LP_printf(flog, " opening file %s\n", name);
    fout = fopen(name, "w");
    if (fout == NULL) {
      LP_error(flog, "cannot open the file %s\n", name);
    }
    for (j = 0; j < nele; j++) {
      pele = player->p_ele[j];
      fprintf(fout, "%d %f\n", pele->id[INTERN_MSH], pele->phydro->h[INTERPOL]);
    }
    fclose(fout);
    free(name);
  }
}

/**\fn void AQ_print_H(s_gc *pgc,FILE *fpout)
 *\brief function to print the H vector
 *\return void
 *
 */
void AQ_print_H(s_cmsh *pcmsh, int itype, FILE *fpout) {
  int i, neletot;
  s_ele_msh *pele;
  neletot = pcmsh->pcount->nele;
  fprintf(fpout, "H : \n");

  for (i = 0; i < neletot; i++) {
    pele = MSH_get_ele_from_abs(pcmsh, i + 1, fpout); // NF 15/12/2017 id ABS_MSH starts at 1 and not 0
  }
}

// NG : 30/08/2021
/**\fn void AQ_print_subsurface_litho(s_cmsh *,FILE *)
 *\brief Prints out a file with characteristics of subsurface lithologies
 *\return void
 */
void AQ_print_subsurface_lithos(s_cmsh *pcmsh, FILE *flog) {
  int e, l;
  char *name;
  FILE *fpout;
  s_layer_msh *player;
  s_ele_msh *pele;
  s_litho_aq *ptmp;

  name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/Subsurface_lithologies.txt", getenv("RESULT"));

  fpout = fopen(name, "w");

  if (fpout == NULL)
    LP_error(flog, "In libaq%4.2f : Error in file %s, function %s : Cannot open file %s", VERSION_AQ, __FILE__, __func__, name);

  fprintf(fpout, "ID_INTERN_CELL ID_LAYER ID_ABS_CELL ID_GIS_CELL ID_GIS_LITHO NAME_LITHO\n");

  for (l = 0; l < pcmsh->pcount->nlayer; l++) {
    player = pcmsh->p_layer[l];
    if (player->type == UNCONFINED_AQ) {
      for (e = 0; e < player->nele; e++) {
        pele = player->p_ele[e];
        ptmp = AQ_browse_litho(pele->phydro->plitho, END_TS); // Starting from the top
        while (ptmp != NULL) {
          if (ptmp->litho_top == YES_AQ) {
            fprintf(fpout, "%d %d %d %d %d %s\n", pele->id[INTERN_MSH], player->id, pele->id[ABS_MSH], pele->id[GIS_MSH], ptmp->id[GIS_MSH], ptmp->name);
            break;
          }
          ptmp = ptmp->prev;
        }
      }
    }
  }
  fclose(fpout);
  free(name);
}
