/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_param.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

s_param_aq *AQ_create_param() {
  s_param_aq *param;

  param = new_param_aq();
  bzero((char *)param, sizeof(s_param_aq));

  // Default initialization
  param->Surf = NO_MSH;
  param->cascade = NO_MSH;                       // NG : 21/01/2021
  param->bypass = NO_MSH;                        // NG : 19/04/2021
  param->Sy_factor = SY_DEFAULT_FACTOR_AQ;       // NG : 11/08/2021
  param->anisotropy_factor = ALPHA_Z_DEFAULT_AQ; // NG : 11/08/2021
  param->log_balance = NO_AQ;                    // NG : 29/01/2021
  param->print_lithos = NO_AQ;                   // NG : 31/08/2021

  return param;
}

// NG : 10/08/2021
/**\fn void AQ_set_uniform_layer_param_values(s_carac_aq*, int, int, double, FILE*)
 *\brief Sets a uniform field value for one given AQ layer and the kindof param type.
 *\return void
 */
void AQ_set_uniform_layer_param_values(s_carac_aq *pcarac_aq, int id_layer, int kindof, double value, FILE *flog) {
  int i;
  s_layer_msh *player;
  s_ele_msh *pele, **p_ele;
  s_cmsh *pcmsh = pcarac_aq->pcmsh;

  player = pcmsh->p_layer[id_layer];
  p_ele = player->p_ele;

  for (i = 0; i < player->nele; i++) {
    switch (kindof) {
    case TRANSM_HOMO: {
      p_ele[i]->phydro->ptransm = AQ_create_transm();
      if (value > EPS_T_AQ) {
        p_ele[i]->phydro->ptransm->transm[T_HOMO] = value;
        p_ele[i]->phydro->ptransm->type = HOMO;
      } else
        LP_error(flog, "Error in file %s, function %s at line %d : Incoherent input value for param %s.\n", __FILE__, __func__, __LINE__, AQ_name_attributs(kindof));
      break;
    }

    case THICK: {
      p_ele[i]->phydro->Thick = value;
      break;
    }

    case STORAGE: {
      p_ele[i]->phydro->S[SS] = value;
      p_ele[i]->phydro->S[SCUR] = value;
      break;
    }

    case H_INI: {
      p_ele[i]->phydro->h[ITER] = value;
      p_ele[i]->phydro->h[T] = value;
      p_ele[i]->phydro->h[INTERPOL] = value;
      p_ele[i]->phydro->h[ITER_INTERPOL] = value;
      p_ele[i]->phydro->h[PIC] = value;
      break;
    }

    case CONDUCT: {
      p_ele[i]->phydro->pcond = AQ_create_cond();
      p_ele[i]->phydro->pcond->conductance = value;
      break;
    }

    case ZTOP: {
      p_ele[i]->phydro->ztop = value;
      break;
    }

    case ZBOT: {
      p_ele[i]->phydro->zbot = value;
      break;
    }

    default: {
      LP_error(flog, "libaq%4.2f : Error in file %s, in function %s, at line %d : Unknown param type.\n", VERSION_AQ, __FILE__, __func__, __LINE__);
      break;
    }
    }
  }
}

void AQ_print_conduct(s_carac_aq *pchar_aq, FILE *flog) {

  s_cmsh *pcmsh;
  s_ele_msh *pele;
  s_layer_msh *player;
  s_face_msh *pface, *psubf;
  int nele, nlayer, e, l, i;
  FILE *fcond;
  char *name, *name_mod;
  double cond, surf;
  // name_mod=IO_name_mod(PIEZ_IO);//FB 27/02/18
  name_mod = IO_name_mod(MASS_IO); // FB 27/02/18 Keyword PIEZ_IO does not exist anymore
  name = (char *)malloc((strlen(getenv("RESULT")) + ALLOCSCD_LP) * sizeof(char));
  sprintf(name, "%s/%s_conduc.txt", getenv("RESULT"), name_mod);
  fcond = fopen(name, "w+");
  if (fcond == NULL)
    LP_error(flog, "cannot open the file %s\n", name);
  pcmsh = pchar_aq->pcmsh;
  nlayer = pcmsh->pcount->nlayer;
  fprintf(fcond, "ID_Abs Cond\n");
  for (l = 0; l < nlayer; l++) {
    player = pcmsh->p_layer[l];
    nele = player->nele;
    for (e = 0; e < nele; e++) {
      pele = player->p_ele[e];
      cond = 0;
      if (pele->face[Z_MSH][TWO] != NULL) {
        pface = pele->face[Z_MSH][TWO];
        if (pface->p_subface != NULL) {
          surf = 0;
          for (i = 0; i < NCODEG_MSH; i++) {
            psubf = pface->p_subface[i];
            if (psubf != NULL) {
              if (psubf->p_descr[TWO] != NULL && psubf->p_descr[ONE] != NULL)
                surf += TS_min(psubf->p_descr[TWO]->surf, psubf->p_descr[ONE]->surf);
              else {
                if (psubf->p_descr[TWO] != NULL)
                  surf += psubf->p_descr[TWO]->surf;
                else
                  surf += psubf->p_descr[ONE]->surf;
              }
              cond += psubf->phydro->pcond->conductance;
            }
          }
          if (surf > 0)
            cond /= surf;
        } else {
          if (pface->p_descr[TWO] != NULL && pface->p_descr[ONE] != NULL)
            surf = TS_min(pface->p_descr[TWO]->surf, pface->p_descr[ONE]->surf);
          else {
            if (pface->p_descr[TWO] != NULL)
              surf = pface->p_descr[TWO]->surf;
            else
              surf = pface->p_descr[ONE]->surf;
          }
          cond = pface->phydro->pcond->conductance / surf;
        }
      } else {
        LP_error(flog, "face is null at ele %d", pele->id[ABS_MSH]);
      }
      fprintf(fcond, "%d %e\n", pele->id[ABS_MSH], cond);
    }
  }
  fclose(fcond);
  free(name);
  free(name_mod);
}

double AQ_calc_litho_thickness(s_hydro_aq *phydro, FILE *flog) {

  s_litho_aq *pl;
  double e = 0;

  pl = phydro->plitho;
  while (pl != NULL) {
    e += pl->thick;
    pl = pl->next;
  }

  return e;
}

/* NG : 13/08/2021 : In case ztop is not explicitly defined, it is automatically
calculated based on zbot and thicknesses. */
int AQ_check_ztop_definition(s_layer_msh *play, FILE *flog) {
  int i, nele = play->nele;
  int answer = NO_AQ;
  s_ele_msh *pele;

  for (i = 0; i < nele; i++) {
    pele = play->p_ele[i];
    if (play->type == UNCONFINED_AQ) {
      if (fabs(pele->phydro->ztop - CODE_AQ) < EPS_AQ) // Only if ztop has not been already defined
      {
        pele->phydro->ztop = pele->phydro->zbot;
        pele->phydro->ztop += AQ_calc_litho_thickness(pele->phydro, flog);
        answer = YES_AQ;
      }
    } else {
      if ((fabs(pele->phydro->ztop - CODE_AQ) < EPS_AQ) && (fabs(pele->phydro->zbot - CODE_AQ) > EPS_AQ)) // Ignoring calculation if no zbot defined
      {
        pele->phydro->ztop = pele->phydro->zbot;
        pele->phydro->ztop += pele->phydro->Thick;
        answer = YES_AQ;
      }
    }
  }
  return answer;
};

double AQ_calc_T_confined(s_hydro_aq *phydro, FILE *flog) {

  s_litho_aq *pl;
  double T = 0;

  pl = phydro->plitho;
  while (pl != NULL) {
    T += pl->thick * pl->k;
    pl = pl->next;
  }

  return T;
}

double AQ_calc_T_unconfined(s_hydro_aq *phydro, FILE *flog) {

  s_litho_aq *pl, *plprev, *plbot;
  double Transm = 0, e = 0, d = 0;
  double wd = 0.;

  wd = phydro->h[T] - phydro->zbot;
  pl = phydro->plitho;
  plbot = phydro->plitho;
  while ((pl != NULL) && (e < wd)) { // warning : the input conductivity file must be written with the value of the bottom lithology at the beginning of the line
    while (plbot->thick == 0) {
      plbot = plbot->next;
    }
    e += pl->thick;
    Transm += pl->thick * pl->k;
    //        printf("H_t : %e  \n",phydro->h[T]);
    //  printf("    E : %e  Transm_cumul : %e  \n",e,T);
    //   printf("litho %s mouillee %f m \n",pl->name,e);
    plprev = pl;
    pl = pl->next;
  }

  // Ajustement en fonction du toit de la nappe et non pas du toit de la formation aquifer
  if (e > wd) {
    //                printf("wd: %e \n",wd);
    if (wd > AQ_DEFAULT_WET_THICKNESS) {
      d = e - wd;
      Transm -= d * plprev->k;
    } else {
      // printf("plbot : %s \n",pl->name);
      Transm = plbot->k * AQ_DEFAULT_WET_THICKNESS;
    }
  }

  return Transm;
}

double AQ_set_Sy_unconfined(s_hydro_aq *phydro, FILE *flog) {

  s_litho_aq *pl, *plprev;
  double yield = 0, e = 0, d = 0;
  double wd = 0.;

  wd = phydro->h[T] - phydro->zbot;
  pl = phydro->plitho;
  while ((pl != NULL) && (e < wd)) {
    e += pl->thick;
    yield = pl->Sy; // MM 27/11/18 the considerated specific yield value for unconfined calculation is the value for the uppermost wet lithology, further calculation for the integration of values of underlying lithologies must be implemented here
    plprev = pl;
    pl = pl->next;
  }
  if (e > wd) {
    if (wd > EPS_TS) {
      d = e - wd;
      yield = plprev->Sy;
    } else {
      yield = pl->Sy;
    }
    // LP_printf(flog,"Sy: %e \n",yield);
  }

  return yield;
}

void AQ_calc_param_unconfined(s_param_aq *param_aq, s_layer_msh *play, FILE *flog) {
  int i, nele;
  double d; // Difference between h and layer thickness
  s_ele_msh *pele;
  s_hydro_aq *phydro;

  nele = play->nele;
  for (i = 0; i < nele; i++) {
    pele = play->p_ele[i];
    AQ_calc_param_unconfined_for_ele(param_aq, pele, flog);
  }
}

/* NG : 13/08/2021 : Modified to integrate multification coefficient
 on S when switching from UNCONFINED to CONFINED.
*/
void AQ_calc_param_unconfined_for_ele(s_param_aq *param_aq, s_ele_msh *pele, FILE *flog) {
  int i, nele;
  double d, Sy; // Difference between h and layer thickness
  s_hydro_aq *phydro;
  s_litho_aq *plitho, *ptmp;

  /*The comparison between litho thickness and H is currently managed using absolute thickness, this will require to set a comparison of H with Thickness of litho + Zbottom of litho */

  phydro = pele->phydro;
  phydro->ptransm->litho_thickness = AQ_calc_litho_thickness(phydro, flog);
  d = phydro->h[T] - phydro->ptransm->litho_thickness - phydro->zbot;

  if (d > EPS_TS) {
    phydro->status = CONFINED_AQ; // NF 29/11/2018
    plitho = pele->phydro->plitho;
    phydro->Thick = phydro->ptransm->litho_thickness;
    phydro->ptransm->transm[T_HOMO] = AQ_calc_T_confined(phydro, flog);
    // LP_printf(flog,"%e confined\n",phydro->ptransm->transm[T_HOMO]);     // NG check

    // phydro->S[SCUR]=phydro->S[SS];    // NG : 13/08/2021 : As is, when it switches from UNCONFINED to CONFINED, S goes to the default value
    //  set when phydro is initialized (YIELD_CONFINED_AQ). No calculation is made whatsoever.

    /* Modification : When the cell switches from UNCONFINED to CONFINED config S(confined) is equal to the product between the
                      Sy value associated with the first litho with non-null thickness (starting from the top) and the Sy_factor
                      (which value can be user defined). Indeed, this litho level in particular is considered as the effective top
                      litho can differ from cell to cell ! */

    ptmp = AQ_browse_litho(plitho, END_TS);

    // Fetching proper value of Sy
    while (ptmp != NULL) {
      if (ptmp->litho_top == YES_AQ) {
        Sy = ptmp->Sy;
        break;
      }
      ptmp = ptmp->prev;
    }
    phydro->S[SCUR] = (Sy * param_aq->Sy_factor);
  } else {
    phydro->status = UNCONFINED_AQ; // NF 29/11/2018
    phydro->Thick = phydro->h[T] - phydro->zbot;
    phydro->ptransm->transm[T_HOMO] = AQ_calc_T_unconfined(phydro, flog);
    // LP_printf(flog,"%e_unconfined\n",phydro->ptransm->transm[T_HOMO]);
    phydro->S[SCUR] = AQ_set_Sy_unconfined(phydro, flog); // MM 28/11/18  the use of this function requires a very important maximum number of steady iteration to solve hydrosystem
                                                          // phydro->S[SCUR]=phydro->S[SY];
    // LP_printf(flog,"%e\n",phydro->S[SCUR]);
  }
}
