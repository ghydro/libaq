/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_simul.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"
/**\fn s_simul_aq *AQ_init_simul()
 *\brief initialize the simulation structur
 *\return psimul
 */
s_simul_aq *AQ_init_simul() {
  /* Structure containing all global variables */
  s_simul_aq *psimul;
  int i;

  psimul = new_simul_aq();
  bzero((char *)psimul, sizeof(s_simul_aq));

  psimul->pchronos = CHR_init_chronos();
  psimul->pclock = CHR_init_clock();
  psimul->pcarac_aq = AQ_create_carac();
  psimul->pcarac_aq->settings = AQ_create_param();
  AQ_init_carac(psimul->pcarac_aq);

  psimul->pout = (s_out_io **)calloc(NOUT_AQ, sizeof(s_out_io *)); // NF,BL 19/10/2015 needed in libaq. This is an exotic feature of BL

  /* for(i=0;i<NOUT_AQ;i++){
    psimul->pout[i]=IO_init_out_struct(pchronos->t[BEGINNING_TS],pchronos->t[END_TS],pchronos->dt);
    IO_init_out_file(psimul->pout[i],FORMATTED_IO,CODE_TS,CODE_TS,daSimul->poutputs);
    } *///NF,BL 19/10/2015 Uncomment to write outputs

  return psimul;
}

/**\fn s_carac_aq *AQ_init_carac()
 *\brief initialize the carac_aq structur
 *\return pcarac_aq
 */
s_carac_aq *AQ_create_carac() {

  s_carac_aq *pcarac_aq;

  pcarac_aq = new_carac_aq();
  bzero((char *)pcarac_aq, sizeof(s_carac_aq));
  pcarac_aq->iappli_gcc = AQ_GC;
  pcarac_aq->ppic = AQ_init_picart();

  return pcarac_aq;
}

void AQ_init_carac(s_carac_aq *pcarac_aq) {

  pcarac_aq->pcmsh = MSH_create_carac_glo();
  pcarac_aq->pcmsh->pcount->pcount_hydro = AQ_create_count_hydro();
  pcarac_aq->calc_aq = GC_create_gc(AQ_GC);
#ifdef OMP
  if (pcarac_aq->settings != NULL) {
    pcarac_aq->settings->psmp = new_smp();
    pcarac_aq->settings->psmp->nthreads = 1;
  }
#endif
  pcarac_aq->steadynitmax = STEADYNITMAX_AQ;
}

// NF, BL 22/10/2015 Creation d'une routine d'initialisation du schema numeric en steady state
void AQ_init_steady(s_carac_aq *pcarac_aq) {
  pcarac_aq->settings->general_param[THETA_AQ] = 1.;
  pcarac_aq->settings->a0 = 1.; // NF 26/11/2018 If it is 0, there is no dampening effect of the storage. As steady state is solved by convergence, the storage terms needs to be calculated even in steady state. To set it up to 0, provide no storage in the COMM file
}

/**\fn void AQ_print_ele_transport_variable(s_carac_aq*, int, FILE*)
 *\brief Debug function : Prints transport variable associated with one given specie
 *\return -
 */
void AQ_print_ele_transport_variable(s_carac_aq *pcarac_aq, int id_species, FILE *flog) {
  int i, j, ne;
  s_layer_msh *player;
  s_ele_msh *pele;
  s_hydro_aq *phydro;
  s_cmsh *pcmsh = pcarac_aq->pcmsh;

  for (i = 0; i < pcmsh->pcount->nlayer; i++) {
    player = pcmsh->p_layer[i];

    for (j = 0; j < player->nele; j++) {
      pele = player->p_ele[j];
      phydro = pele->phydro;
      LP_printf(flog, "Element (Int : %d, Layer : %d) Transport variable (Specie %d) = %f\n", pele->id[INTERN_MSH], pele->player->id, id_species, phydro->transp_var[id_species]);
    }
  }
}
