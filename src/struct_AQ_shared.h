/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: struct_AQ_shared.h
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/*! \file struct_AQ_MSH_coupled.h
Here are the strucuturs used when libaq and libmesh are coupled
*/

typedef struct hydraulics_aq s_hydro_aq;
typedef struct boundary_condition s_bound_aq;
typedef struct flux s_flux_aq;
typedef struct counter_hydro s_count_hydro_aq;
typedef struct transm_hydro s_transm_aq;
typedef struct conduct_hydro s_cond_aq;
typedef struct mass_balance_aq s_mb_aq;
typedef struct litho s_litho_aq;
typedef struct derivation_aq s_derivation_aq;
typedef struct threshold_aq s_threshold_aq;

/**
 * \struct  boundary_condition of type s_bound_aq
 * \brief structure to define boundaries conditions
 */
struct boundary_condition {
  /*!< Type of boundary in [dirichlet,neumann,cauchy]*/
  int type;
  /*!< boundary frequency in [constant,variable]*/
  int freq;
  /*!< pointer to the values attributed to bondaries conditions. This value is either the head(dirichlet) or the discharge(neuman) **/
  s_ft *pft;
  /*!< definition of qlim **/
  double qlim; /*------------------- WARNING : -------------------- il faudra multiplier par la surface de l'élément pour avoir les bon flux !!!  */
  /*!< Parameter tracking the origin of the source flux imposed in case of multiple sources inputs on an aquifer element.
       Values in NORIGIN_SOURCE_AQ. LOCAL_SOURCE_AQ refers to isolated recharges or uptakes. */
  int origin;
};

/**
 * \struct  litho of type  s_litho_aq
 * \brief defines the lithology of a layer in the case UNCONFINED_AQ
 */
struct litho {
  int id[3];                   /*!<GIS_MSH -> identifiant du fichier de commande, INTERN_MSH-> identifiant interne a une layer varie de 0 a nlitho-1, ABS_MSH -> not used for lithologies*/
                               // NG : Shouldn't it be NID_MSH then, instead of "3" ?
  char name[STRING_LENGTH_LP]; /*!< name of the nested area*/
  double thick;                /*!< thickness of the lithology*/
  double k;                    /*!< permeability of the lithology*/
  double Sy;                   /* Specific yield of the lithology */
  int litho_top; /* Declares if the litho layer is at the top. Values in [YES,NO]. NO by default. 
                    Beware, it's the effective layer (first from top with non-null thickness).*/ // NG : 30/08/2021
  s_litho_aq *next;
  s_litho_aq *prev;
};

/**
 * \struct  flux of type s_flux_aq
 * \brief Is the structur of influxes and outfluxes
 */
struct flux {
  /*!< influx */
  double influx;
  /*!< outflux*/
  double outflux;
};

/**
 * \struct derivation_aq structur of type s_derivation_aq
 * \brief Manages the attributes of an AQ-derivation (on Cauchy discharge).
 */
struct derivation_aq {
  /*!< Derivation type. Values in NDERIV_TYPE.*/
  int type;
  /*!< Time series associated with a derivation link. Useful for bypasses-type links. Remains unused for cascade-type links. Set to NULL by default anyway. */
  s_ft *pft;
  /*!< Vector sized over NDERIV_TYPE of s_id_spa* chain pointers. These chains point toward a serie of destination cells for each derivation type. Set to NULL by default. */
  s_id_spa **link;
};

/**
 * \struct hydraulics of type s_hydro_aq
 * \brief This structur groups all variables and structurs needed for flow calculation. This structur have been defined from invpiez
 */
struct hydraulics_aq {

  int id;          /*!<id of hydro element the same than element*/
  int calc;        /*!<Used to know if calculation were already made (invipiez)*/
  double h[NSTEP]; /*!< tab of hydraulic heads at time t and at previous iter NSTEP in [T,ITER,INTERPOL,ITER_INTERPOL,DRAWD,PIC,NSTEP] ITER is the initial h at the beginning of a time step PIC is the initial h at the beginning of a PICART ITERATION*/
  double Thick;    /*!< Aquifer thickness (confined) or wet thickness (unconfined) used to calculated the transmissivity if hydraulic conductivity is used*/
  s_litho_aq *plitho;
  double zbot;                                                                         // MM 24/10/2018 < altitude of layer bottom
  double ztop;                                                                         // MM 24/10/2018 < altitude of layer top
  double Qlim;                                                                         /*!< the maximal infiltration rate from river to aquifer*/
  s_transm_aq *ptransm;                                                                /*!< pointer to strucutur of transmissivities values*/
  s_cond_aq *pcond; /*!< pointer to strucutur of conductivities values*/               // NF 27/7/2021 this pointers shouldn't be here. It will be accessible towards the interface structure that is connected to a face.See libmesh0.16
  double S[N_STOR];                                                                    /*!< Storativity values N_STOR in [Ss(used for confined aquifer),Sy(used for unconfined aquifer)]*/
  double **grad_h;                                                                     /*!< values of hydraulic gradient in each directions X Y Z ONE TWO*/
  s_bound_aq *pbound;                                                                  /*!< Pointer to the boundary structur */
  int nsource_type[NSOURCE]; /*!<number of each source type RECHARGE_AQ or UPTAKE_AQ*/ // NF-NG : 04/03/2020
  s_bound_aq ***psource; /*!< Pointer to the source (boundary) structur (multiple pointers 
                              for each source type) *psource[NSOURCE] NSOURCE in [RECHARGE_AQ,UPTAKE_AQ]*/ // NF-NG : 04/03/2020
  double b_source;                                                                          /*!< coef of B matrix for sources */
  s_mb_aq *pmb;                                                                             /*!< pointer to mass balance structure */
  int status;                                                                               /*!< defines if the physical problem is CONFINED or UNCONFINED*/
  s_derivation_aq *pderivation; /*!< pointer to derivation_aq structur */                   // NG : 20/04/2021
  int emitting_status;                                                                      /*!< flag identifying if the cell is an derivation emitting cell. Values in YES, NO. Set to NO by default. */
  s_threshold_aq **p_thresh; /*!< Vector of s_threshold_aq*. Sized over NTHRESH_AQ param.*/ // NG : 29/02/2024
  double thresh_val[NTHRESH_AQ][NSTEP];                                                     /*!< Effective values of threshold attributes (water table and/or uptakes values) at each time step considered to compute the b_source term. */
  int hyd_cellstate; /*!< Labels the hydraulic state of a cell : WET (i.e. h > zbot) or DRY (i.e. h <= zbot). 
                          Initialized to WETCELL_AQ by default. */ // NG : 17/04/2024

  // Transport-related attributes
  double *transp_var;  /* Array of transport values (ie. C or T variable depending on the specie type). Table dimensioned over the total number of species.!< */
  double *transp_flux; /* Array of transport fluxes (ie. C or T variable depending on the specie type). Dimensions: nb_species*nb_flux. */
};

/**
 * \struct  mass_balance  of type s_mb_aq
 * \brief This structur groups all variables and matrix needed for mass balance calculation. This structur have been taken from HycGeos
 */
struct mass_balance_aq {
  /*!< mass[INI_AQ] is the hydraulic head [m] at the beginning of the time step. mass[END_AQ] is the hydraulic head [m] at the end of the time step.
   * mass[DMASS_AQ] [m3/s] is the difference between the volume of water at the end of the time step and the volume of water at the beginning of
   * the time step, divided by the time step. */
  double mass[NTIME_AQ];
  /*!< storage[t] is either SY or SS at t. It multiplies mass[t] when calculating mass[DMASS_AQ]. This is needed to print a proper MB*/ // NF 7/12/2018
  double storage[DMASS_AQ];
  /*!< flux[idir][itype][INI_AQ] (m3/s) is the discharge entering through the face (idir, itype) at the beginning of the time step. flux[idir][itype][END_AQ] (m3/s)
   * is the discharge entering through the face (idir, itype) at the end of the time step. flux[idir][itype][DMASS_AQ] (m3/s) is the average discharge during the
   * time step, i.e., theta*flux[idir][itype][END_AQ]+(1-theta)*flux[idir][itype][INI_AQ] */
  double ***flux;
  /*!< For each boundary condition type kk in [DIRICHLET,NEUMANN,CAUCHY]: pbound[kk][INI_AQ] (m3/s) and pbound[kk][END_AQ] (m3/s) are the discharges
   * entering in the cell because of boundary condition kk, at the beginning and at the end of the time step, respectively. pbound[kk][DMASS_AQ] (m3/s)
   * is the average discharge linked to the boundary condition, i.e., pbound[DIRICHLET][DMASS_AQ]=theta*pbound[DIRICHLET][END_AQ]+(1-theta)*pbound[DIRICHLET][INI_AQ].
   * Similarly for NEUMANN. Instead pbound[CAUCHY][DMASS_AQ]=pbound[CAUCHY][END_AQ] because the Cauchy term is treated implicitly in the equation.  */
  double pbound[NBOUND][NTIME_AQ];
  /*!< For NSOURCE in [RECHARGE_AQ,UPTAKE_AQ], source[NSOURCE][NTIME_AQ] (m3/s) is the discharge injected (RECHARGE_AQ) or extracted (UPTAKE_AQ) in
   * the cell at the beginning of the time step (INI_AQ), at the end of the time step (END_AQ). source[NSOURCE][DMASS_AQ] (m3/s) is the average discharge
   * injected/extracted during the time step, i.e.: source[NSOURCE][DMASS_AQ]=theta*source[NSOURCE][END_AQ]+(1-theta)*source[NSOURCE][INI_AQ] */
  double source[NSOURCE][NTIME_AQ];
  /*!< error (m3/s) is the mass balance error, calculated as: mass[DMASS_AQ]-flux[][][DMASS_AQ]-pbound[][DMASS_AQ]-source[][DMASS_AQ] */
  double error;
};

/**
 * \struct threshold structur of type s_threshold_aq
 * \brief Stores the attributes to be considered when modifying uptakes via user-defined thresholds on critical minimal water table.
 */
struct threshold_aq {
  int type;  /*!< Threshold type. Values in NTHRESH_AQ enumeration. Can store either critical water table or threshold uptake time serie. */
  int freq;  /*!< Threshold time serie type = Either constant or variable. Values in NTYPE. */
  s_ft *pft; /*!< Threshold time serie */
};

/**
 * \struct counter_hydro of type s_count_hydro_aq
 * \brief This structur groups the counters of hydraulics properties
 */
struct counter_hydro {

  /*!< Number of boundaries*/
  int *nele_type;
  /*!< Layer and Elements id which are boundaries*/
  s_id_io **elebound;
  /*!< Layer and Elements id which get imposed */
  s_id_io **elesource;
  /*!< Layer and Elements id which are active */
  s_id_io *eleactive;
  /*!< Number of BORDER elements. It is only used for the OPEN_BOUNDARY option */
  int nborder;
  s_id_io ***p_eletype;
};

/**
 * \struct conduct_hydro of type s_cond_aq
 * \brief This structur defines the conductances
 */
struct conduct_hydro {
  /*!< Conductance value in m2.s-1. For a Z-TWO face with Cauchy boundary condition it represents the conductance coefficient. For a face in X or Y
   * direction it represents the internode transmissivity. For a gravel pit it is a conductance value given as input.*/
  double conductance;
  double q;                                                                                              /*!< Discharge associated to the conductance condition (m3/s) */
  int type; /*!< Type of conductance given by the user or recalculated automatically at each time step*/ // NF 9/12/2020
};

/**
 * \struct transm_hydro of type s_transm_aq
 * \brief This structur defines the transmissivities
 */
struct transm_hydro {
  /*!< kind of transmissivity HOMO ANI*/
  int type;
  int anisotropy_z;
  /*!< transmissivity value ND_T in [T_HOMO,T_X,T_Y] */
  double transm[ND_T];
  double transm_init[ND_T];
  double litho_thickness;
};

#define new_transm() ((s_transm_aq *)malloc(sizeof(s_transm_aq))) // NF : 2/9/2010
#define new_flux() ((s_flux_aq *)malloc(sizeof(s_flux_aq)))       // NF : 2/9/2010
#define new_hydro() ((s_hydro_aq *)malloc(sizeof(s_hydro_aq)))
#define new_bound() ((s_bound_aq *)malloc(sizeof(s_bound_aq)))
#define new_counter_hydro() ((s_count_hydro_aq *)malloc(sizeof(s_count_hydro_aq)))
#define new_mb_aq() ((s_mb_aq *)malloc(sizeof(s_mb_aq)))
#define new_cond_aq() ((s_cond_aq *)malloc(sizeof(s_cond_aq)))
#define new_litho_aq() ((s_litho_aq *)malloc(sizeof(s_litho_aq)))
#define new_derivation_aq() ((s_derivation_aq *)malloc(sizeof(s_derivation_aq))) // NG : 20/04/2021
#define new_threshold_aq() ((s_threshold_aq *)malloc(sizeof(s_threshold_aq)))    // NG : 19/02/2024