/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_setup_layer.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

/**\fn s_layer_aq *AQ_chain_layer(s_layer_aq *pd1, s_layer_aq *pd2)
 *\brief chain layers
 *
 *
 */
s_layer_aq *AQ_chain_layer(s_layer_aq *pd1, s_layer_aq *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd1;
}

/**\fn s_layer_aq **AQ_tab_layer(s_layer_aq *play,int nlay,FILE *fp)
 *\brief generate the layer_hydro tabular in input.y
 *
 */
s_layer_aq **AQ_tab_layer(s_layer_aq *play, int nlay, FILE *fp) {
  s_layer_aq **p_lay;
  int i = 0;
  p_lay = (s_layer_aq **)malloc(nlay * sizeof(s_layer_aq *));
  for (i = nlay - 1; i >= 0; i--) {
    p_lay[i] = play;
    play = play->prev;
  }
  return p_lay;
}

/**\fn s_layer_aq *AQ_create_layer(char *name,int id)
 *\brief create layer_hydro
 *
 *
 */
s_layer_aq *AQ_create_layer(char *name, int id) {

  s_layer_aq *player;

  player = new_layer_aq();
  bzero((char *)player, sizeof(s_layer_aq));
  // NF 9/12/2020 by default player->cond_type is initialized at COND
  sprintf(player->name, "%s", name);
  player->id = id;
  player->mean_type = HARMONIC;

  return player;
}
