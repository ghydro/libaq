/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_source.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnes RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "libpc.h"
#include "IO.h"
#include "CHR.h"
#include "spa.h"
#include "GC.h"
/*
#ifdef COUPLED
#include "reservoir.h"
#include "FP.h"
#include "NSAT.h"
#include "HYD.h"
#endif
*/
#include "MSH.h"
#include "AQ.h"

// NG : 05/03/2020
/**
 * \fn s_bound_aq ***AQ_create_source(FILE *fpout)
 * \brief Create pointer towards source structure for one element. Manage memory allocation.
 * \return psource pointer
 */
s_bound_aq ***AQ_create_source(FILE *fpout) {

  s_bound_aq ***psource = NULL;
  int i;

  psource = (s_bound_aq ***)malloc(NSOURCE * sizeof(s_bound_aq **));
  if (psource == NULL)
    LP_error(fpout, "libaq%4.2f in %s of %s at line %d: Memory allocation failure for psource pointer.\n", VERSION_AQ, __func__, __FILE__, __LINE__);
  bzero((char *)psource, NSOURCE * sizeof(s_bound_aq **));

  for (i = 0; i < NSOURCE; i++) {
    psource[i] = (s_bound_aq **)malloc(sizeof(s_bound_aq *));
    if (psource[i] == NULL)
      LP_error(fpout, "libaq%4.2f in %s of %s at line %d: Memory allocation failure for psource[kindof] pointer.\n", VERSION_AQ, __func__, __FILE__, __LINE__);
    bzero((char *)psource[i], sizeof(s_bound_aq *)); // Initialisation avec un tableau d'un seul element. Chaque tableau de kindof sera realloc a la declaration d'une nouvelle source via AQ_add_source_to_element()
  }

  return psource;
}

// NG : 05/03/2020
/**
 * \fn AQ_add_source_to_element(s_ele_msh *pele, int kindof, s_bound_aq *pbound, FILE *fpout)
 * \brief for a given kindof, add a pbound to an element
 * \return void. Directly reallocates tables in ***psource and assign the newer pbound in psource
 */
void AQ_add_source_to_element(s_ele_msh *pele, int kindof, s_bound_aq *pbound, FILE *fpout) {
  int nb_source;

  pele->phydro->nsource_type[kindof]++; // Incrementation du nombre de source pour l'element et le kindof considere
  nb_source = pele->phydro->nsource_type[kindof];

  pele->phydro->psource[kindof] = (s_bound_aq **)realloc(pele->phydro->psource[kindof], nb_source * sizeof(s_bound_aq *));
  if (pele->phydro->psource[kindof] == NULL) {
    LP_error(fpout, "libaq%4.2f in %s of %s at line %d: Memory reallocation failure for psource pointer.\n", VERSION_AQ, __func__, __FILE__, __LINE__);
  }

  pele->phydro->psource[kindof][nb_source - 1] = pbound;

  // LP_printf(fpout,"Fonction %s : Valeur de nsource %d Kindof = %s, element %d, origin %s.\n",__func__,nb_source,AQ_name_source(kindof),
  //  pele->id[ABS_MSH],AQ_name_source_origin(pele->phydro->psource[kindof][nb_source-1]->origin));  // NG check
}

/**
 * \fn s_bound_aq *AQ_create_boundary(int kindof)
 * \brief create BC structur
 * \return pboud
 */
/* NG : 05/03/2020 : Updated with new declaration of multiple source bound for a given kindof source type
and tracking of the origin of the pbound */
void AQ_define_source(s_cmsh *pcmsh, s_id_io *id_list, s_ft *values, int kindof, double stot, FILE *fpout) {

  s_layer_msh *player;
  int id, n = 0;
  int nlayer;
  s_ele_msh *pele;
  s_id_io *ptmp;
  s_count_hydro_aq *pcount;
  s_bound_aq *pbound;

  ptmp = id_list;
  pcount = pcmsh->pcount->pcount_hydro;
  pbound = AQ_define_source_val(kindof, values, stot, fpout);
  // LP_printf(Simul->poutputs,"dimension of pft : %d\n",TS_length_ts(pbound->pft)); // BL to debug

  if (ptmp != NULL) {
    nlayer = ptmp->id_lay;
    player = pcmsh->p_layer[nlayer];
    AQ_define_elebound(ptmp, pcount->elesource, kindof, fpout);
  } else {
    LP_error(fpout, "The list of id is NULL !!\n");
  }

  while (ptmp != NULL) {
    id = ptmp->id;
    // LP_printf(fpout," id cell : %d layer : %d\n ",id,nlayer);
    pele = player->p_ele[id];

    if (pele->phydro->psource == NULL) {
      pele->phydro->psource = AQ_create_source(fpout);
    }

    AQ_add_source_to_element(pele, kindof, pbound, fpout); // NG : Modifie directement le psource de l'element et y assigne la pbound

    ptmp = ptmp->next;
  }
}

/**
 * \fn void AQ_print_elesource(s_cmsh *pcmsh,FILE *fpout)
 * \brief print the elements which are sources_inputs (meme fonction que AQ_print_elebound sauf la boucle sans doute qu'il y a plus intelligent à faire que deux fonctions...)
 * \return void
 */
// NG : 05/03/2020 : Updated version to integrate multiple sources per source type and tracking of the origin of the source fluxes
void AQ_print_elesource(s_cmsh *pcmsh, FILE *fpout) {

  int i, j, div, nb_ele;
  double Qtot, qtmp;
  s_id_io *ptmp;
  s_id_io **elesource;
  s_ele_msh *pele;
  s_ft *pft;
  int nb_sources; // nombre de sources de type kindof pour l'element

  elesource = pcmsh->pcount->pcount_hydro->elesource;

  for (i = 0; i < NSOURCE; i++) {
    LP_printf(fpout, "\nSource_input : %s.\n", AQ_name_source(i));
    nb_ele = 0;
    Qtot = 0;

    if (elesource[i] != NULL) {
      ptmp = IO_browse_id(elesource[i], BEGINNING_TS);

      while (ptmp != NULL) {
        nb_ele++;
        pele = MSH_get_ele(pcmsh, ptmp->id_lay, ptmp->id, fpout);

        nb_sources = pele->phydro->nsource_type[i];

        LP_printf(fpout, "Layer : %d cell : %d ABS_MSH : %d input type : %s. Total number of input sources : %d\n", ptmp->id_lay, ptmp->id, MSH_get_ele_id(pele, ABS_MSH), AQ_name_source(i), nb_sources); // NG : 04/03/2020

        for (j = 0; j < nb_sources; j++) {
          LP_printf(fpout, "Input frequency %d : %s. Origin : %s\n", j, AQ_name_freq(pele->phydro->psource[i][j]->freq), AQ_name_source_origin(pele->phydro->psource[i][j]->origin)); // NG : 07/03/2020

          pft = TS_browse_ft(pele->phydro->psource[i][j]->pft, BEGINNING_TS);
          div = 0;
          qtmp = 0;
          while (pft != NULL) {
            div++;
            LP_printf(fpout, " value of source : %e at time %f\n", pft->ft, pft->t);
            qtmp += pft->ft * pele->pdescr->surf;
            pft = pft->next;
          }
          if (div > 0) {
            Qtot += qtmp / div;
          } else {
            LP_error(fpout, "No value stored if source %s at ele %d ABS_MSH %d\n", AQ_name_source(i), ptmp->id, MSH_get_ele_id(pele, ABS_MSH));
          }
        }
        ptmp = ptmp->next;
      }
    }
    LP_printf(fpout, "Number of aquifer cells submitted to %s : %d. Total discharge %f\n", AQ_name_source(i), nb_ele, Qtot);
  }
}

/**
 * \fn s_bound_aq *AQ_define_sources_val(int kindof,s_ft *pft,double ltot)
 * \brief define sources values (tjs pareil quasiment la meme fonction que AQ_define_bound_val vraiment pertinent?)
 * \return *s_bound_aq
 */
s_bound_aq *AQ_define_source_val(int kindof, s_ft *pft, double stot, FILE *fpout) {

  s_bound_aq *pbound;
  s_ft *ptmp;
  int sign = 1;

  pbound = AQ_create_boundary(kindof);

  // NF 23/10/2017 Sign convention in diffusivity equation : recharge are negative and uptake positive
  if (kindof == RECHARGE_AQ) {
    sign *= -1;
  }
  stot *= sign;

  ptmp = TS_division_ft_double(pft, stot, fpout);

  pft = TS_free_ts(pft, fpout);
  pbound->pft = ptmp;
  if (pbound->pft->next != NULL) {
    pbound->freq = VAR;
  }

  pbound->origin = LOCAL_SOURCE_AQ; // NG 07/03/2020

  return pbound;
}

/**
 * \fn double* AQ_calc_source_value(int, s_bound_aq**, double, double, FILE*)
 * \brief Computes total source values (either recharge or uptake).
 * \return Array which contains total source values at time t-dt (position 0) and at time t (position 1) to include in b_source.
 */
double *AQ_calc_source_value(int nb_pbound, s_bound_aq **psource, double t, double deltat, FILE *fpout) {

  int i, j;
  double *src_values;

  src_values = (double *)calloc(2, sizeof(double)); // pos 0 is value at time t-dt and pos 1 is value at time t.

  for (j = 0; j < nb_pbound; j++) {

    if (psource[j]->freq == CST) {
      src_values[1] += psource[j]->pft->ft;
      src_values[0] += psource[j]->pft->ft;
    } else {
      /* NG : 02/06/2021 : Pointer chains have been rewound when created in input.y. So here,
      browsing can progress along time-step only, with no additional full rewind operations required. */
      // pft = TS_browse_ft(psource[j]->pft,BEGINNING_TS);

      psource[j]->pft = TS_function_t_pointed(t - deltat, psource[j]->pft, fpout);
      src_values[0] += TS_interpolate_ts(t - deltat, psource[j]->pft);
      src_values[1] += TS_interpolate_ts(t, psource[j]->pft);
    }
  }
  return src_values;
}