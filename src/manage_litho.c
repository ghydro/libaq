/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libaq
 * FILE NAME: manage_litho.c
 *
 * CONTRIBUTORS: Baptiste LABARTHE, Nicolas FLIPO, Nicolas GALLOIS,
 *               Fulvia BARATELLI, Agnès RIVIERE, Mathias MAILLOT,
 *               Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Aquifer systems (saturated) with finite volume
 * using a semi-implicit temporal scheme: confined-unconfined aquifer units,
 * 2D diffusivity equation in each layer, 1D vertical exchanges between layers
 * with a linear drainance model, 1D vertical exchanges at the soil surface
 * or for river with a conductance model, subcell lithology.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libaq Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "IO.h"
#include "spa.h"
#include "CHR.h"
#include "MSH.h"
#include "libpc.h"
#include "GC.h"
#include "AQ.h"

/**\fn s_litho_aq *AQ_create_litho(char *name,int id_gis,int id)
 *\brief create litho
 *eof(int)
 *
 */
s_litho_aq *AQ_create_litho() {

  s_litho_aq *plitho;

  plitho = new_litho_aq();
  bzero((char *)plitho, sizeof(s_litho_aq));
  plitho->litho_top = NO_AQ; // NG : 30/08/2021
  return plitho;
}

/**\fn s_litho_aq *AQ_create_litho(char *name,int id_gis,int id)
 *\brief create litho
 */
s_litho_aq *AQ_fill_litho(char *name, int id_gis, int id) {

  s_litho_aq *plitho;
  sprintf(plitho->name, "%s", name);
  plitho->id[GIS_MSH] = id_gis;
  plitho->id[INTERN_MSH] = id;
  return plitho;
}

/**\fn s_litho_aq *AQ_browse_litho(s_litho_aq *plitho,int iwhere)
 *\brief rewind or forward chained litho
 *
 * iwhere can be BEGINNING_TS or END_TS
 */
s_litho_aq *AQ_browse_litho(s_litho_aq *plitho, int iwhere) {
  s_litho_aq *ptmp, *preturn;
  ptmp = plitho;

  while (ptmp != NULL) {
    preturn = ptmp;
    switch (iwhere) {
    case BEGINNING_TS:
      ptmp = ptmp->prev;
      break;
    case END_TS:
      ptmp = ptmp->next;
      break;
    }
  }
  return preturn;
}

/** \fn s_litho_aq *AQ_chain_fwd_litho(s_litho_aq *pd1,s_litho_aq *pd2)
/* \brief Links two s_litho together */
s_litho_aq *AQ_chain_fwd_litho(s_litho_aq *pd1, s_litho_aq *pd2) {
  if (pd2 != NULL)
    pd1->next = pd2;
  if (pd1 != NULL)
    pd2->prev = pd1;
  return pd1;
}

s_litho_aq *AQ_copy_litho(s_litho_aq *plitho, FILE *flog) {
  s_litho_aq *pl_cpy;
  pl_cpy = AQ_create_litho();
  pl_cpy = AQ_fill_litho(plitho->name, plitho->id[GIS_MSH], plitho->id[INTERN_MSH]);
  return pl_cpy;
}

s_litho_aq *AQ_create_litho_sequence_in_ele(s_litho_aq *pl, FILE *flog) {
  s_litho_aq *pltmp, *plnew;
  pltmp = pl;
  plnew = AQ_copy_litho(pltmp, flog);
  pltmp = pltmp->next;
  while (pltmp != NULL) {
    plnew = AQ_chain_fwd_litho(plnew, AQ_copy_litho(pltmp, flog));
    plnew = plnew->next;
    pltmp = pltmp->next;
  }
  plnew = AQ_browse_litho(plnew, BEGINNING_TS);
  return plnew;
};

void *AQ_create_litho_for_each_ele_in_layer(s_layer_msh *player, FILE *flog) {
  int i, j, nlitho;
  s_ele_msh *pele;
  s_litho_aq *plitho;

  plitho = AQ_browse_litho(player->plitho, BEGINNING_TS);
  for (i = 0; i < player->nele; i++) {
    pele = player->p_ele[i];
    pele->phydro->plitho = AQ_create_litho_sequence_in_ele(plitho, flog);
  }
}

// NG : 30/08/2021
/**\fn void AQ_find_subsurface_litho(s_layer_msh*, int, FILE*)
 *\brief Determines for each ele, the first found subsurface lithology (first from the top with non-null thickness)
 *\return void
 */
void AQ_find_subsurface_litho(s_layer_msh *player, int type_id, FILE *flog) {
  int i, found = NO_AQ;
  s_ele_msh *pele;
  s_litho_aq *plitho;

  for (i = 0; i < player->nele; i++) {
    pele = player->p_ele[i];
    plitho = pele->phydro->plitho;
    plitho = AQ_browse_litho(plitho, END_TS); // Starting form the top
    while (plitho != NULL) {
      if (plitho->thick > EPS_AQ) {
        found = YES_AQ;
        plitho->litho_top = YES_AQ;
        // LP_printf(flog,"lithoaff ele %d layer %d litho %d\n",pele->id[INTERN_MSH],pele->player->id,plitho->id[INTERN_MSH]);  // NG check
        break;
      }
      plitho = plitho->prev;
    }
    if (found == NO_AQ)
      LP_error(flog, "In libaq%4.2f : Error in file %s, function %s at line %d : All lithologies thicknesses for element INTERN ID %d layer %d are null. Check your data.\n", VERSION_AQ, __FILE__, __func__, __LINE__, pele->id[INTERN_MSH], player->id);
  }
}
